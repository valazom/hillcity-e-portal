namespace Common.Resoures
{
    public class EmailTemplateStrings
    {
        public const string EmailTemplateDirectory = "Template";
        public const string EmailTemplateRegistrationFilename = "RegisterEmail.txt";
        public const string EmailTemplateForgetPasswordFilename = "ForgetPasswordEmail.txt";
        public const string EmailTemplateArgumentUsername = "Username";
        public const string EmailTemplateArgumentEmail = "Email";
        public const string EmailTemplateArgumentRoleName = "RoleName";
        public const string EmailTemplateArgumentAdminEmail = "adminEmail";
        public const string EmailTemplateArgumentLink = "link";
        public const string EmailToAddress ="ToAddress";
        public const string EmailFromAddress ="FromAddress";
        public const string EmailSubject ="Subject";
        public const string EmailName ="EmailName";
        public const string EmailAddress ="EmailAddress";
        public const string EmailArgumentPasswordResetSubject = "Password reset";
        public const string EmailArgumentRegistrationSubject = "User registration";
        public const string EmailArgumentFullName = "FullName";
        public const string EmailArgumentAssignedFullName = "AssignedFullName";
        public const string EmailArgumentOccupation = "Occupation";
        public const string EmailArgumentState = "State";
        public const string EmailArgumentCountry = "Country";
        public const string EmailArgumentGender = "Gender";
        public const string EmailArgumentUniversity = "University";
        public const string EmailArgumentCourse = "Course";
        public static string EmailArgumentName = "Name";
        public static string EmailArgumentDate = "Date";
        public static string EmailArgumentMode = "Mode";
        public static string EmailArgumentAddress = "Address";
        public static string EmailArgumentStartTime = "StartTime";
        public static string EmailArgumentCreatedByUsername = "CreatedByUsername";
        public static string EmailArgumentEngagementDateCreated = "EngagementDateCreated";
        public static string EmailArgumentMentorFullName = "MentorFullName";
        public static string EmailArgumentDetails = "Details";
        public static string EmailArgumentCategory = "Category";
        public static string EmailArgumentTaskEndDate = "TaskEndDate";
    }
}
namespace Common.Resoures
{
    public static class MessageStrings
    {
        public const string  MessageFailureEngagementReport = "Engagement report has not been uploaded";
        public const string  MessageFailureEngagementTask = "EngagementTask has not been completed";
        public const string  MessageFailureUserRegInfo = "Registration information is empty";
        public const string  MessageFailureUsernameExist = "Username already exist";
        public const string  MessageFailureEmailExist = "Email already exist";
        public const string MessageFailurePasswordValidation = "Your password must have aleast lower and upper case character with a special character of minimum length 8 and maximum length 15";
        public const string MessageFailureUserLogin = "Username or password incorrect";
        public const string MessageFailureEmailMessageTypeNotSupported = "Email message type not supported";
        public const string MessageFailureForgetPasswordUsernameDoesNotExist = "Username does not exist";
        public const string MessageFailureUsernameHash = "Username hash is empty";
        public const string MessageSuccessEmailPasswordReset = "Email for password reset successfully sent";
        public const string MessageSuccessPasswordReset = "Password reset succesful";
        public const string MessageFailureRoleDoesNotExist = "Role does not exist";
        public const string MessageFailureMenteeDoesNotExist = "Mentee information deos not exist";
        public const string MessageFailureMenteeAlreadyExist = "Mentee already exist";
        public const string MessageFailureMentorDoesNotExist = "Mentor deos not exist";
        public const string MessageFailureMentorAlreadyExist = "Mentor already exist";
        public const string MessageFailureEngagementDoesNotExist = "Engagement deos not exist";
        public const string MessageFailureEngagementAlreadyExist = "Engagement already exist";
        public const string MessageFailureEnagagementCannotComplete = "Engagement cannot be completed";
        public const string MessageFailureEngagmentTaskDoesNotExist = "Engagement Task does not exist";
        public const string MessageFailureEngagmentTaskAlreadyExist = "Engagement Task already exist";
        public const string MessageFailureEngagementScheduleDate = "The Engagement schedule date cannot be less than the created date";
        public const string MessageFailureEngagementLocation = "Engagement status says face-to-face but no location is set";
    }
}
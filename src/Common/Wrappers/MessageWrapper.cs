namespace Common.Wrappers
{
    public class MessageWrapper
    {
        public string Message { get; set; }
        public bool isValid { get; set; }
    }
}
namespace Common.Email.Configuration
{
    public interface IEmailConfiguration
    {
         string SmtpServer {get;}
         int SmtpPort {get;}
         string SmtpUsername {get;}
         string SmtpPassword {get;}
    }
}
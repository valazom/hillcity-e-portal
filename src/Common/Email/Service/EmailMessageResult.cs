using System;

namespace Common.Email.Service
{
    public class EmailMessageResult
    {
        public bool Succeeded { get; set; }
        public string ErrorMessage { get; set; }
        public Exception Exception { get; set; }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Email.Configuration;
using Common.Email.MessageInfo;
using Common.Resoures;
using MimeKit;
using MimeKit.Text;

namespace Common.Email.Service
{
    public class EmailService : IEmailService
    {
        private IEmailConfiguration _emailConfiguration;

        private EmailMessageResult _emailSentMessage;
        

        public EmailService(IEmailConfiguration emailConfiguration)
        {
            _emailConfiguration = emailConfiguration;
            _emailSentMessage = new EmailMessageResult();
        }
        
        private MimeMessage GetInternetAddress(EmailMessage emailMessage)
        {
            var message = new MimeMessage();
            message.To.AddRange(emailMessage.ToAddress.Select(x => new MailboxAddress(x.Name, x.Address)));
            message.From.Add(new MailboxAddress(emailMessage.FromAddress.Name, emailMessage.FromAddress.Address));
            message.Bcc.AddRange(emailMessage.BccAddress.Select(x => new MailboxAddress(x.Name, x.Address)));
            message.Subject = emailMessage.Subject;

            message.Body = new TextPart(TextFormat.Html)
            {
                Text = emailMessage.Content
            };
            return message;
        }

        public async Task<EmailMessageResult> SendAsync(EmailMessage emailmessage)
        {
            try
            {
                MimeMessage message = GetInternetAddress(emailmessage);

                using (var emailClient = new MailKit.Net.Smtp.SmtpClient())
                {
                    await emailClient.ConnectAsync(_emailConfiguration.SmtpServer, _emailConfiguration.SmtpPort, true);

                    emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                    await emailClient.AuthenticateAsync(_emailConfiguration.SmtpUsername, _emailConfiguration.SmtpPassword);

                    await emailClient.SendAsync(message);

                    _emailSentMessage.Succeeded = true;

                    await emailClient.DisconnectAsync(true);

                    return _emailSentMessage;
                }
            }
            catch (Exception ex)
            {

                _emailSentMessage.Succeeded = false;
                _emailSentMessage.ErrorMessage = ex.Message;
                _emailSentMessage.Exception = ex;
                return _emailSentMessage;
            }

        }

       
    }
}
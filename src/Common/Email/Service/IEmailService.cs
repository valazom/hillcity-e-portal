using System.Collections.Generic;
using System.Threading.Tasks;
using Common.Email.MessageInfo;

namespace Common.Email.Service
{
    public interface IEmailService
    {
        Task<EmailMessageResult> SendAsync(EmailMessage message);
    }
}
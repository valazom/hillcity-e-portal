namespace Common.Email.MessageInfo
{
    public enum EmailMessageType
    {
        Registration = 1,
        ForgetPassword,
        MentorAssignment,
        MenteeAssgnment,
        Engagement,
        EngagementAccepted,
        EngagementTask
    }
}
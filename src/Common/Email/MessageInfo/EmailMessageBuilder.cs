using System.Collections.Generic;

namespace Common.Email.MessageInfo
{
    public class EmailMessageBuilder
    {
        private List<EmailAddress> ToAddresses;
        private EmailAddress FromAddress;
        private List<EmailAddress> BccAddresses;
        private string Subject;
        private string Content;
        public EmailMessageBuilder()
        {
            ToAddresses = new List<EmailAddress>();
            BccAddresses = new List<EmailAddress>();
        }
        public EmailMessageBuilder AddToAddresses(EmailAddress emailAddress)
        {
            ToAddresses.Add(emailAddress);
            return this;
        }
        public EmailMessageBuilder AddToAddresses(string Name, string Address)
        {
            var emailAddress = new EmailAddress
            {
                Name = Name,
                Address = Address
            };
            return AddToAddresses(emailAddress);
        }
        public EmailMessageBuilder AddToAddresses(List<EmailAddress> emailAddresses)
        {
            ToAddresses.AddRange(emailAddresses);
            return this;
        }
        public EmailMessageBuilder AddFromAddress(string Name,string Address)
        {
            var emailAddress = new EmailAddress
            {
                Name = Name,
                Address = Address
            };
            return AddFromAddress(emailAddress);
        }
        public EmailMessageBuilder AddFromAddress(EmailAddress emailAddress)
        {
            FromAddress = emailAddress;
            return this;
        }
        public EmailMessageBuilder AddBccAddress(EmailAddress emailAddress)
        {
            BccAddresses.Add(emailAddress);
            return this;
        }
        public EmailMessageBuilder AddBccAddress(List<EmailAddress> emailAddresses)
        {
            BccAddresses.AddRange(emailAddresses);
            return this;
        }
        public EmailMessageBuilder AddSubject(string subject)
        {
            Subject = subject;
            return this;
        }
        public EmailMessageBuilder AddBody(string content)
        {
            Content = content;
            return this;
        }
        public EmailMessage Build()
        {
            var message = new EmailMessage
            {
                ToAddress = ToAddresses,
                FromAddress = FromAddress,
                BccAddress = BccAddresses,
                Subject = Subject,
                Content = Content
            };
            return message;
        }

    }
}
using System.Collections.Generic;

namespace Common.Email.MessageInfo
{
    public class EmailMessage
    {
        public EmailMessage()
        {
            ToAddress = new List<EmailAddress>();
            BccAddress = new List<EmailAddress>();
        }
        public List<EmailAddress> ToAddress { get; set; }
        public List<EmailAddress> BccAddress { get; set; }
        public EmailAddress FromAddress { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
    }
}
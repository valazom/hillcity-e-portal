using System.ComponentModel.DataAnnotations;
using Domain.Enums;

namespace Domain.Models
{
    public class EmailQueue
    {
        [Key]
        public int QueueId { get; set; }
        public EmailSentStatus Status { get; set; }
        [Required]
        public string  Subject { get; set; }
        [Required]
        public string ToAddress { get; set; }
        [Required]
        public string FromAddress { get; set; }
        public string CC {get; set;}
        public string BCC {get; set;}
        [Required]
        public string Body { get; set; }
        public int SentAttempts { get; set; }
    }
}
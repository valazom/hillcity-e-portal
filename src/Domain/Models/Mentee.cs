using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models
{
    public class Mentee
    {
        public Mentee(int MenteeId, string University,int MentorId,double score = 0)
        {
            this.UserId = MenteeId;
            this.University = University;
            this.MentorId = MentorId;
            this.Score = CalculateScore(score);
        }
        public Mentee()
        {
            
        }
        // Calulate the overall score of the Mentee
        private double CalculateScore(double score)
        {
            return (this.Score += score)/2;
        }

        
        [Key]
        public int UserId { get; set; }
        public string University { get; set; }
        public string  Course { get; set; }
        [Required]
        public DateTime DateOfAdmission { get; set; }
        [Required]
        public DateTime DateOfGraduation { get; set; }
        public int?  MentorId { get; set; }
        public double Score { get; set; }
        public virtual Mentor Mentor { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<Engagement> Engagements {get; set;}
    }
}
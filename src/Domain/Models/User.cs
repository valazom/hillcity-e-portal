using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Enums;
using Microsoft.AspNetCore.Identity;

namespace Domain.Models
{
    public class User
    {
        public User()
        {
            this.UserRoles = new List<RoleUser>();
        }
        [Key]
        public int UserId { get; set; }
        [StringLength(500)]
        public string  FirstName { get; set; }
        [StringLength(500)]
        public string LastName {get; set;}
        public string FullName { get{ return FirstName + " " + LastName;} }
        [Required]
        public string Username { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        public Gender Gender { get; set; }
        public bool IsDeleted { get; set; }
        public int LocationId { get; set; }
        public string Photo { get; set; }
        public string PublicId { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt {get; set;}
        public virtual Location Location { get; set; }
        public virtual ICollection<RoleUser> UserRoles { get; set; }
    }
}
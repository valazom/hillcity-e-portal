using System;
using System.ComponentModel.DataAnnotations;
using Common.Resoures;
using Common.Wrappers;
using Domain.Enums;

namespace Domain.Models
{
    public class Engagement
    {
        [Key]
        public int EngagementId { get; set; }
        [Required]
        [StringLength(1000)]
        public string Subject { get; set; }
        [Required]
        public TimeSpan Duration { get; set; }
        public TimeSpan StartTime {get; set;}
        public DateTime DateCreatedUtc { get; set; }
        [Required]
        public DateTime ScheduledDateUtc { get; set; }
        public DateTime? DateCompleted {get; set;}
        public int? LocationId { get; set; }
        public string EngagementReport { get; set; }
        public bool HasEngagementTask {get {return Task != null;}}
        [Required]
        public CommunicationMode CommunicationMode { get; set; }
        public EngagementStatus Status { get; set; }
        public virtual Location Location { get; set; }
        public int? EngagementTaskId { get; set; }
        public virtual EngagementTask Task { get; set; }
        public virtual Mentee Mentee { get; set; }
        public virtual Mentor Mentor { get; set; }

        
  
    }
}
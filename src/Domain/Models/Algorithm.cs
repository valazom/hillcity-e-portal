using Domain.Enums;

namespace Domain.Models
{
    public class Algorithm
    {
        public int AlgorithmId { get; set; }
        public AlgorithmType Type { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }
}
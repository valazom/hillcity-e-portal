using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Domain.Models
{
    public class Role
    {
        [Key]
        public int RoleId { get; set; }
        [Required]
        [StringLength(100)]
        public string RoleName { get; set; }
        public virtual ICollection<RoleUser> RoleUsers { get; set; }
    }
}
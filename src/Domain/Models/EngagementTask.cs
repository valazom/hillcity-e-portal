using System;
using System.ComponentModel.DataAnnotations;
using Domain.Enums;

namespace Domain.Models
{
    public class EngagementTask
    {
        public int EngagementTaskId { get; set; }
        [Required]
        public TaskCategory TaskCategory { get; set; }
        [Required]
        public string Details { get; set; }
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        public EngagementTaskStatus Status { get; set; }
        public int EngagementId { get; set; }
        public virtual Engagement Engagement { get; set; }
    }
}
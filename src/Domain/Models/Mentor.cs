using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Models
{
    public class Mentor
    {
        public Mentor(int MentorId,string Occupation)
        {
            this.UserId = MentorId;
            this.Occupation = Occupation;
        }
        public Mentor()
        {
            
        }
        
        [Key]
        public int UserId { get; set; }
        public string Occupation { get; set; }
        public string State { get; set; }
        public string  Country { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<Mentee> Mentees { get; set; }
        public virtual ICollection<Engagement> Engagements { get; set; }
    }
}
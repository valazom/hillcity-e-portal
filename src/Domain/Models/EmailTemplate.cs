using System.ComponentModel.DataAnnotations;
using Common.Email.MessageInfo;

namespace Domain.Models
{
    public class EmailTemplate
    {
        [Key]
        public int TemplateId { get; set; }
        [Required]
        public EmailMessageType MessageType { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string TemplateHtml {get; set;}
    }
}
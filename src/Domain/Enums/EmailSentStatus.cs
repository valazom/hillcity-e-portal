namespace Domain.Enums
{
    public enum EmailSentStatus
    {
        Pending = 1,
        Sent = 2,
        Error = 3
    }
}
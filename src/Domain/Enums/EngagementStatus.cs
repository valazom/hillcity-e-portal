namespace Domain.Enums
{
    public enum EngagementStatus
    {
        Pending = 1,
        Accept,
        Completed
    }
}
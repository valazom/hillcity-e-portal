namespace Domain.Enums
{
    public enum EngagementTaskStatus
    {
        Pending = 1,
        Overdue,
        Completed
    }
}
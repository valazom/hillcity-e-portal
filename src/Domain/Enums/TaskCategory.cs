namespace Domain.Enums
{
    public enum TaskCategory
    {
        Principles_And_Values = 1,
        Academic_Review,
        General_Well_Being_Of_Mentee,
        Mentee_Life_Goals_And_Complete,
        Any_Other_Business
    }
}
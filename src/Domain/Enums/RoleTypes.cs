namespace Domain.Enums
{
    public enum RoleTypes
    {
        Recipent = 1,
        Mentor,
        Administrator
    }
}
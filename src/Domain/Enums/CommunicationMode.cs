using System.ComponentModel;

namespace Domain.Enums
{
    public enum CommunicationMode
    {
        [Description("Face to Face")]
        FaceToFace = 1,
        [Description("WhatsApp Video Call")]
        WhatsAppVideoCall,
        [Description("WhatApp Voice Call")]
        WhatsAppVoiceCall,
        [Description("Skype Video Call")]
        SkypeVideoCall,
        [Description("Skype Voice Call")]
        SkypeVoiceCall,
        [Description("Phone Call")]
        PhoneCall

    }
}
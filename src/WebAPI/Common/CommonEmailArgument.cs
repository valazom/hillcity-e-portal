using System.Collections.Generic;
using Domain.Models;
using Microsoft.Extensions.Configuration;

namespace WebAPI.Common
{
    public class CommonEmailArgument
    {
        public IConfiguration config;
        public CommonEmailArgument(IConfiguration config)
        {
            this.config = config;
        }
        public Dictionary<string, string> GetMenteeEmailArguments(Mentor mentor, Mentee mentee)
        {
            var menteeEmailBuilder = new EmailArgumentsBuilder();
            var menteeEmailArguments = menteeEmailBuilder.AddFullName(mentee.User.FullName)
                                                                  .AddAssignedFullName(mentor.User.FullName)
                                                                  .AddUserArguments(mentor.User)
                                                                  .AddToAddressArgument(mentee.User.Email)
                                                                  .AddFromAddressArgument(config.GetSection("EmailSettings:FromAddress").Value)
                                                                  .AddOccupation(mentor.Occupation)
                                                                  .AddState(mentor.State)
                                                                  .AddCountry(mentor.Country)
                                                                  .AddGender(mentor.User.Gender.ToString())
                                                                  .AddSubjectArgument("Mentor assignment")
                                                                  .Build();
            return menteeEmailArguments;
        }

        public Dictionary<string, string> GetMentorEmailArguments(Mentor mentor, Mentee mentee)
        {
            var mentorEmailBuilder = new EmailArgumentsBuilder();
            var mentorEmailArguments = mentorEmailBuilder.AddFullName(mentor.User.FullName)
                                                                  .AddAssignedFullName(mentee.User.FullName)
                                                                  .AddUserArguments(mentee.User)
                                                                  .AddToAddressArgument(mentor.User.Email)
                                                                  .AddFromAddressArgument(config.GetSection("EmailSettings:FromAddress").Value)
                                                                  .AddUniversity(mentee.University)
                                                                  .AddCourse(mentee.Course)
                                                                  .AddGender(mentee.User.Gender.ToString())
                                                                  .AddSubjectArgument("Mentee assignement")
                                                                  .Build();
            return mentorEmailArguments;
        }

    }
}
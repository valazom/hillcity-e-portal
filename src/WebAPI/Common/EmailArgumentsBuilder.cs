using System;
using System.Collections.Generic;
using Common.Resoures;
using Domain.Models;

namespace WebAPI.Common
{
    public class EmailArgumentsBuilder
    {
        private  Dictionary<string,string> EmailArguments  { get; set; }
        public EmailArgumentsBuilder()
        {
            EmailArguments = new Dictionary<string, string>();
        }

        public EmailArgumentsBuilder AddUserArguments(User user)
        {
            if(!DoesKeyExist(EmailTemplateStrings.EmailTemplateArgumentUsername))
               EmailArguments.Add(EmailTemplateStrings.EmailTemplateArgumentUsername,user.Username);
            if(!DoesKeyExist(EmailTemplateStrings.EmailTemplateArgumentEmail))
               EmailArguments.Add(EmailTemplateStrings.EmailTemplateArgumentEmail,user.Email);
            return this;
        }
        public EmailArgumentsBuilder AddRoleArgument(string roleName)
        {
           if(!DoesKeyExist(EmailTemplateStrings.EmailTemplateArgumentRoleName))
              EmailArguments.Add(EmailTemplateStrings.EmailTemplateArgumentRoleName,roleName);
           return this;
        }
        public EmailArgumentsBuilder AddSubjectArgument(string Subject)
        {
           if(!DoesKeyExist(EmailTemplateStrings.EmailSubject))
             EmailArguments.Add(EmailTemplateStrings.EmailSubject,Subject);
           return this;
        }
        public EmailArgumentsBuilder AddLinkArgument(string link)
        {
            if(!DoesKeyExist(EmailTemplateStrings.EmailTemplateArgumentLink))
              EmailArguments.Add(EmailTemplateStrings.EmailTemplateArgumentLink,link);
            return this;
        }
        public EmailArgumentsBuilder AddToAddressArgument(string ToAddress){
            if(!DoesKeyExist(EmailTemplateStrings.EmailToAddress))
               EmailArguments.Add(EmailTemplateStrings.EmailToAddress,ToAddress);
            return this;
        }
        public EmailArgumentsBuilder AddFromAddressArgument(string fromAddress){
            if(!DoesKeyExist(EmailTemplateStrings.EmailFromAddress))
               EmailArguments.Add(EmailTemplateStrings.EmailFromAddress,fromAddress);
            return this;
        }
        public EmailArgumentsBuilder AddAdminEmail(string adminEmail){
            if(!DoesKeyExist(EmailTemplateStrings.EmailTemplateArgumentAdminEmail))
              EmailArguments.Add(EmailTemplateStrings.EmailTemplateArgumentAdminEmail,adminEmail);
            return this;
        }
        public EmailArgumentsBuilder AddFullName(string FullName)
        {
            if(!DoesKeyExist(EmailTemplateStrings.EmailArgumentFullName))
              EmailArguments.Add(EmailTemplateStrings.EmailArgumentFullName,FullName);
            return this;
        }
        public EmailArgumentsBuilder AddAssignedFullName(string AssignedFullName)
        {
            if(!DoesKeyExist(EmailTemplateStrings.EmailArgumentAssignedFullName))
              EmailArguments.Add(EmailTemplateStrings.EmailArgumentAssignedFullName,AssignedFullName);
            return this;
        }
        public EmailArgumentsBuilder AddOccupation(string Occupation)
        {
            if(!DoesKeyExist(EmailTemplateStrings.EmailArgumentOccupation))
              EmailArguments.Add(EmailTemplateStrings.EmailArgumentOccupation,Occupation);
            return this;
        }
        public EmailArgumentsBuilder AddState(string State)
        {
            if(!DoesKeyExist(EmailTemplateStrings.EmailArgumentState))
              EmailArguments.Add(EmailTemplateStrings.EmailArgumentState,State);
            return this;
        }
        public EmailArgumentsBuilder AddCountry(string Country)
        {
            if(!DoesKeyExist(EmailTemplateStrings.EmailArgumentCountry))
              EmailArguments.Add(EmailTemplateStrings.EmailArgumentCountry,Country);
            return this;
        }
        public EmailArgumentsBuilder AddGender(string Gender)
        {
            if(!DoesKeyExist(EmailTemplateStrings.EmailArgumentGender))
              EmailArguments.Add(EmailTemplateStrings.EmailArgumentGender,Gender);
            return this;
        }
        public EmailArgumentsBuilder AddUniversity(string University)
        {
            if(!DoesKeyExist(EmailTemplateStrings.EmailArgumentUniversity))
              EmailArguments.Add(EmailTemplateStrings.EmailArgumentUniversity,University);
            return this;
        }
        public EmailArgumentsBuilder AddCourse(string Course)
        {
            if(!DoesKeyExist(EmailTemplateStrings.EmailArgumentCourse))
              EmailArguments.Add(EmailTemplateStrings.EmailArgumentCourse,Course);
            return this;
        }
        public EmailArgumentsBuilder AddName(string Name)
        {
            if(!DoesKeyExist(EmailTemplateStrings.EmailArgumentName))
              EmailArguments.Add(EmailTemplateStrings.EmailArgumentName,Name);
            return this;
        }
        public EmailArgumentsBuilder AddDate(DateTime date)
        {
            if(!DoesKeyExist(EmailTemplateStrings.EmailArgumentDate))
              EmailArguments.Add(EmailTemplateStrings.EmailArgumentDate,date.ToString("dd/MM/yyyy"));
            return this;
        }
        public EmailArgumentsBuilder AddMode(string mode)
        {
            if(!DoesKeyExist(EmailTemplateStrings.EmailArgumentMode))
              EmailArguments.Add(EmailTemplateStrings.EmailArgumentMode,mode);
            return this;
        }
        public EmailArgumentsBuilder AddAddress(string Address)
        {
            if(!DoesKeyExist(EmailTemplateStrings.EmailArgumentAddress))
              EmailArguments.Add(EmailTemplateStrings.EmailArgumentAddress,Address);
            return this;
        }
        public EmailArgumentsBuilder AddCreatedByUsername(string Username)
        {
            if(!DoesKeyExist(EmailTemplateStrings.EmailArgumentCreatedByUsername))
              EmailArguments.Add(EmailTemplateStrings.EmailArgumentCreatedByUsername,Username);
            return this;
        }
        public EmailArgumentsBuilder AddStartTime(string startTime)
        {
            if(!DoesKeyExist(EmailTemplateStrings.EmailArgumentStartTime))
                EmailArguments.Add(EmailTemplateStrings.EmailArgumentStartTime,startTime);
            return this;
        }
        public EmailArgumentsBuilder AddEngagementDateCreated(string Date) {
            if(!DoesKeyExist(EmailTemplateStrings.EmailArgumentEngagementDateCreated))
                EmailArguments.Add(EmailTemplateStrings.EmailArgumentEngagementDateCreated,Date);
            return this;
        }
        public EmailArgumentsBuilder AddMentorFullName(string fullName) {
            if(!DoesKeyExist(EmailTemplateStrings.EmailArgumentMentorFullName))
                EmailArguments.Add(EmailTemplateStrings.EmailArgumentMentorFullName,fullName);
            return this;
        }
        public EmailArgumentsBuilder AddDetails(string details) {
            if(!DoesKeyExist(EmailTemplateStrings.EmailArgumentDetails))
                EmailArguments.Add(EmailTemplateStrings.EmailArgumentDetails,details);
            return this;
        }
        public EmailArgumentsBuilder AddCategory(string category) {
            if(!DoesKeyExist(EmailTemplateStrings.EmailArgumentCategory))
                EmailArguments.Add(EmailTemplateStrings.EmailArgumentCategory,category);
            return this;
        }
        public EmailArgumentsBuilder AddTaskEndDate(DateTime taskenddate) {
            if(!DoesKeyExist(EmailTemplateStrings.EmailArgumentTaskEndDate))
                EmailArguments.Add(EmailTemplateStrings.EmailArgumentTaskEndDate,taskenddate.ToString("dd/MM/yyyy"));
            return this;
        }
        public Dictionary<string,string> Build()
        {
           return EmailArguments;
        }
        public void Clear(){
            EmailArguments.Clear();
        }
        private bool DoesKeyExist(string key)
        {
            if(EmailArguments.ContainsKey(key))
               return true;
            return false;
        }
    }
}
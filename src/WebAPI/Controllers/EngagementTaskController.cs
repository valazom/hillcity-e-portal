using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using AutoMapper;
using Common.Email.MessageInfo;
using Common.Resoures;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebAPI.Common;
using WebAPI.Data.Repository;
using WebAPI.Dtos.EngagementTask;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class EngagementTaskController : ControllerBase
    {
        private readonly IEngagementTaskRepository engagementTaskRepository;
        private readonly IMapper mapper;
        private readonly ILogger<EngagementTask> logger;
        private readonly Microsoft.Extensions.Configuration.IConfiguration config;

        public readonly IEmailQueueRepository emailQueueRepository;

        public readonly IEngagementRepository engagementRepository;
        public EngagementTaskController(IEngagementTaskRepository engagementTaskRepository,
                IMapper mapper, ILogger<EngagementTask> logger,
                Microsoft.Extensions.Configuration.IConfiguration config,
                IEmailQueueRepository emailQueueRepository,
                IEngagementRepository engagementRepository)
        {
            this.config = config;
            this.emailQueueRepository = emailQueueRepository;
            this.engagementRepository = engagementRepository;
            this.engagementTaskRepository = engagementTaskRepository;
            this.mapper = mapper;
            this.logger = logger;
        }

        [Description("Gets all EngagementTasks in the system")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        [HttpGet("all")]
        public async Task<IActionResult> GetAllEngagmentTask()
        {
            try
            {
                var engagementTasks = await engagementTaskRepository.GetAllEngagementTask();
                var engagementTasksDto = mapper.Map<IEnumerable<EngagementTask>>(engagementTasks);
                if (engagementTasksDto == null)
                    throw new System.Exception($"Unable to map Dto of type {typeof(EngagementTask)} to domain object of type {typeof(ReturnEngagementTaskDto)}");
                return Ok(engagementTasksDto);
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500);
            }
        }

        [Description("Gets a specific engagementTask in the system")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpGet("{Id}", Name = "engagementTask")]
        public async Task<IActionResult> GetEngagementTask(int Id)
        {
            try
            {
                var engagementTask = await engagementTaskRepository.GetEngagementTaskbyId(Id);
                if (engagementTask == null)
                    return NotFound(MessageStrings.MessageFailureEngagmentTaskDoesNotExist);
                var engagementTaskDto = mapper.Map<ReturnEngagementTaskDto>(engagementTask);
                if (engagementTaskDto == null)
                    throw new System.Exception($"Unable to map Dto of type {typeof(EngagementTask)} to domain object of type {typeof(ReturnEngagementTaskDto)}");
                return Ok(engagementTaskDto);
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500);
            }
        }

        [Description("Completes an engagementTask in the system")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpGet("Complete/{Id}")]
        public async Task<IActionResult> CompleteEngagementTask(int Id)
        {
            try
            {
                var engagementTask = await engagementTaskRepository.GetEngagementTaskbyId(Id);
                if (engagementTask == null)
                    return NotFound(MessageStrings.MessageFailureEngagmentTaskDoesNotExist);
                if (await engagementTaskRepository.CompleteEngagementTask(engagementTask))
                    return NoContent();
                throw new System.Exception("The EngagmentTask was not completed");
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500);
            }
        }

        [Description("Creates an engagementTask in the system")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        [HttpPost("create")]
        public async Task<IActionResult> CreateEngagementTask(CreateEngagementTaskDto engagementTaskDto)
        {
            try
            {
                var engagementTask = mapper.Map<EngagementTask>(engagementTaskDto);
                if (engagementTask == null)
                    throw new System.Exception($"Unable to map Dto of type {typeof(CreateEngagementTaskDto)} to domain object of type {typeof(EngagementTask)}");
                // expiration date for engagement task is 3 months
                engagementTask.EndDate = engagementTask.StartDate.AddMonths(3);
                engagementTask.Engagement = await engagementRepository.GetEngagementbyId(engagementTask.EngagementId);
                if(engagementTask.Engagement == null)
                    return BadRequest("Task not attached to Engagement");
                var engagementTaskCreated = await engagementTaskRepository.CreateEngagementTask(engagementTask);
               
                // build mentee engagement task email arguments
                var emailBuilder = new EmailArgumentsBuilder();
                var menteeEmailArguments = emailBuilder.AddFullName(engagementTaskCreated.Engagement.Mentee.User.FullName)
                                                                .AddMentorFullName(engagementTaskCreated.Engagement
                                                                .Mentor.User.FullName)
                                                                .AddDate(engagementTaskCreated.StartDate)
                                                                .AddName(engagementTaskCreated.Engagement.Subject)
                                                                .AddDetails(engagementTaskCreated.Details)
                                                                .AddCategory(engagementTaskCreated.TaskCategory.ToString())
                                                                .AddTaskEndDate(engagementTaskCreated.EndDate)
                                                                .AddToAddressArgument(engagementTaskCreated.Engagement
                                                                .Mentee.User.Email)
                                                                .AddFromAddressArgument(config
                                                                .GetSection("EmailSettings:FromAddress").Value)
                                                                .AddSubjectArgument("New Engagement Task")
                                                                .Build();
                // create email queue
                await emailQueueRepository.CreateEmailQueue(menteeEmailArguments,EmailMessageType.EngagementTask);

               return NoContent();
            }
            catch (System.Exception ex)
            {

                logger.LogError(ex, ex.Message);
                return StatusCode(500, "The engagement already has a task. Kindly complete that to create a new one.");
            }
        }

        [Description("Updates an engagementTask in the system")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpPut("update")]
        public async Task<IActionResult> UpdateEngagementTask(UpdateEngagementTaskDto engagementTaskDto)
        {
            try
            {
                var engagementTask = await engagementTaskRepository.GetEngagementTaskbyId(engagementTaskDto.EngagementTaskId);
                if (engagementTask == null)
                    return NotFound(MessageStrings.MessageFailureEngagmentTaskDoesNotExist);
                mapper.Map(engagementTaskDto, engagementTask);
                if (await engagementTaskRepository.Update(engagementTask))
                    return NoContent();
                throw new System.Exception("The engagementTask was not updated");
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500);
            }
        }

        [Description("Updates specific information of an engagementTask in the system")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpPatch("update/{Id}")]
        public async Task<IActionResult> UpdateEngagementTask(int Id, JsonPatchDocument<PatchEngagementTaskDto> engagementTaskDtoPatch)
        {
            try
            {
                var engagementTask = await engagementTaskRepository.GetEngagementTaskbyId(Id);
                if (engagementTask == null)
                    return NotFound(MessageStrings.MessageFailureEngagmentTaskDoesNotExist);
                var engagementTaskDto = mapper.Map<PatchEngagementTaskDto>(engagementTask);
                if (engagementTaskDto == null)
                    throw new System.Exception($"Unable to map Dto of type {typeof(EngagementTask)} to domain object of type {typeof(PatchEngagementTaskDto)}");
                // Apply the json patch
                engagementTaskDtoPatch.ApplyTo(engagementTaskDto);
                mapper.Map(engagementTaskDto, engagementTask);
                if (await engagementTaskRepository.Update(engagementTask))
                    return NoContent();
                throw new System.Exception("The engagementTask was not updated");

            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500, ex.Message);
            }
        }

        [Description("Deletes engagementTask in the system")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpDelete("delete/{Id}")]
        public async Task<IActionResult> DeleteEngagementTask(int Id)
        {
            try
            {
                var engagementTask = await engagementTaskRepository.GetEngagementTaskbyId(Id);
                if (engagementTask == null)
                {
                    return NotFound("Engagement task not found");
                }
                if (await engagementTaskRepository.Delete(engagementTask))
                {
                    return NoContent();
                }
                throw new System.Exception("The engagement Task was not completed");
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500, ex.Message);
            }
        }
    }
}
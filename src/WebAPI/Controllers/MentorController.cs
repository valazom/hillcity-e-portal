using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using WebAPI.Data.Repository;
using AutoMapper;
using Microsoft.Extensions.Logging;
using System.ComponentModel;
using System.Collections.Generic;
using Domain.Models;
using WebAPI.Dtos.Mentor;
using System.Threading.Tasks;
using Common.Resoures;
using WebAPI.Dtos.Mentee;
using WebAPI.Dtos.Engagement;
using Microsoft.AspNetCore.JsonPatch;
using System.Linq;
using Domain.Enums;
using WebAPI.Dtos.EngagementTask;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MentorController : ControllerBase
    {
        private readonly IMentorRepository mentorRepository;
        private readonly IUserRepository userRepository;
        private readonly IMapper mapper;
        private readonly ILogger<MenteeController> logger;

        public MentorController(IMentorRepository mentorRepository, IUserRepository userRepository,
           IMapper mapper, ILogger<MenteeController> logger)
        {
            this.mentorRepository = mentorRepository;
            this.userRepository = userRepository;
            this.mapper = mapper;
            this.logger = logger;
        }

        [Description("Gets all mentors in the system")]               
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        [HttpGet("allmentors")]
        [Authorize(Roles = "Mentor,Administrator")]
        public async Task<IActionResult> GetAllMentors()
        {
            try
            {
                var mentors = await mentorRepository.GetAllMentors();
                var mentorsDto = mapper.Map<IEnumerable<ReturnMentorDto>>(mentors);
                if(mentorsDto == null)
                  throw new System.Exception($"Unable to map Dto of type {typeof(Mentor)} to domain object of type {typeof(ReturnMentorDto)}");
                return Ok(mentorsDto);
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex,ex.Message);
                return StatusCode(500);
            }
        }

        [Description("Gets a particular mentor in the system")]               
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpGet("{Id}", Name="mentor")]
        [Authorize(Roles = "Mentor,Administrator")]
        public async Task<IActionResult> GetMentor(int Id)
        {
            try
            {
                var mentor = await mentorRepository.GetMentorById(Id);
                if(mentor == null)
                   return NotFound(MessageStrings.MessageFailureMentorDoesNotExist);
                var mentorDto = mapper.Map<ReturnMentorDto>(mentor);
                if(mentorDto == null)
                   throw new System.Exception($"Unable to map Dto of type {typeof(Mentor)} to domain object of type {typeof(ReturnMentorDto)}");
                return Ok(mentorDto);
            }
            catch (System.Exception ex)
            {
               logger.LogError(ex,ex.Message);
               return StatusCode(500);
            }
        }

        [Description("Creates a new mentor in the system")]               
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        [HttpPost("creatementor")]
        [Authorize(Roles = "Mentor,Administrator")]
        public async Task<IActionResult> CreateMentor(CreateMentorDto mentorDto)
        {
            try
            {
                var mentor = mapper.Map<Mentor>(mentorDto);
                if(mentor == null)
                  throw new System.Exception($"Unable to map Dto of type {typeof(CreateMentorDto)} to domain object of type {typeof(Mentor)}");
                var mentorInDb = await mentorRepository.GetMentorById(mentor.UserId);
                if(mentorInDb != null)
                   return BadRequest(MessageStrings.MessageFailureMentorAlreadyExist);
                var mentorCreated = await mentorRepository.CreateMentor(mentor);
                if(mentorCreated == null)
                  throw new System.Exception("Mentor was not created");
                var mentorCreatedDto = mapper.Map<ReturnMentorDto>(mentorCreated);
                return CreatedAtRoute(routeName:"mentor", 
                                      routeValues:new {Id = mentorCreated.UserId},
                                      value: mentorCreatedDto );
                
            }
            catch (System.Exception ex)
            {
               logger.LogError(ex,ex.Message);
               return StatusCode(500);
            }
        }
        
        [Description("Gets mentees assigned to a particular mentor")]               
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpGet("mentees/{userId}")]
        [Authorize(Roles = "Mentor,Administrator")]
        public async Task<IActionResult> GetMenteesAssigned(int userId)
        {
            try
            {
                var mentor = await mentorRepository.GetMentorById(userId);
                if(mentor == null)
                  return NotFound(MessageStrings.MessageFailureMentorDoesNotExist);
                var mentees = mentor.Mentees;
                var menteesDto = mapper.Map<IEnumerable<AssignedMenteeDto>>(mentees);
                if(menteesDto == null)
                  throw new System.Exception($"Unable to map Dto of type {typeof(Mentee)} to domain object of type {typeof(ReturnMenteeDto)}");
                var menteeViewDto = new MenteeViewDto();
                menteeViewDto.AssignedMentees = menteesDto;
                menteeViewDto.NumberOfCompletedEngagements = mentor.Engagements.Where(c => c.Status == EngagementStatus.Completed).Count();
                menteeViewDto.NumberOfPendingEnagagaments = mentor.Engagements.Where(c => c.Status == EngagementStatus.Pending).Count();
                return Ok(menteeViewDto);
            }
            catch (System.Exception ex)
            {
               logger.LogError(ex,ex.Message);
               return StatusCode(500);
            }
        }

        [Description("Gets mentor's engagements")]               
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpGet("engagements/{userId}")]
        [Authorize(Roles = "Mentor,Administrator")]
        public async Task<IActionResult> GetEnagagements(int userId)
        {
           try
           {
                var mentor = await mentorRepository.GetMentorById(userId);
                if (mentor == null)
                    return NotFound(MessageStrings.MessageFailureMentorDoesNotExist);
                var engagements = mentor.Engagements;
                var enagagementDto = mapper.Map<IEnumerable<ReturnEngagementDto>>(engagements);
                if(enagagementDto == null)
                   throw new System.Exception($"Unable to map Dto of type {typeof(Engagement)} to domain object of type {typeof(ReturnEngagementDto)}");
                var mentorEngagements = new MentorEngagement();
                mentorEngagements.Engagements = enagagementDto.OrderByDescending(c => c.ScheduledDateUtc);
                mentorEngagements.NumberOfCompletedEngagements = engagements.Where(c => c.Status == EngagementStatus.Completed).Count();
                mentorEngagements.NumberOfPendingEngagements = engagements.Where(c => c.Status == EngagementStatus.Pending).Count();
                mentorEngagements.NumberOfAssignedMentees = mentor.Mentees.Count();
                return Ok(mentorEngagements);
           }
           catch (System.Exception ex)
           {
               logger.LogError(ex,ex.Message);
               return StatusCode(500);
           }
        }

        [Description("Gets engagement task created by a mentor")]               
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpGet("engagementtask/{Id}")]
        [Authorize(Roles = "Mentor,Administrator")]
        public async Task<IActionResult> GetEngagementTask(int Id)
        {
            try
            {
                var mentor = await mentorRepository.GetMentorById(Id);
                if(mentor == null) 
                    return NotFound(MessageStrings.MessageFailureMentorDoesNotExist);
                var engagementTasks = mentor.Engagements.Where(c => c.Task != null).Select(e => e.Task);
                var engagementTaskDtos = mapper.Map<IEnumerable<ReturnEngagementTaskDto>>(engagementTasks);
                if(engagementTaskDtos == null)
                     throw new System.Exception($"Unable to map Dto of type {typeof(EngagementTask)} to domain object of type {typeof(ReturnEngagementTaskDto)}");
                return Ok(engagementTaskDtos);

            }
            catch (System.Exception ex)
            {
               logger.LogError(ex, ex.Message);
               return StatusCode(500);
            }
        } 

        [Description("Updates mentor information")]               
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpPut("updatementor")]
        [Authorize(Roles = "Mentor,Administrator")]
        public async Task<IActionResult> UpdateMentor(UpdateMentorDto mentorDto)
        {
           try
           {
                var mentor = await mentorRepository.GetMentorById(mentorDto.userId);
                if (mentor == null)
                    return NotFound(MessageStrings.MessageFailureMentorDoesNotExist);
                mapper.Map(mentorDto,mentor);
                if(await mentorRepository.Update(mentor))
                   return NoContent();
                throw new System.Exception("The mentor was not updated");
           }
           catch (System.Exception ex)
           {
               logger.LogError(ex, ex.Message);
               return StatusCode(500);
           }
        }

        [Description("Updates mentor specific information")]               
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpPatch("updatementor/{id}")]
        [Authorize(Roles = "Mentor,Administrator")]
        public async Task<IActionResult> UpdateMentor(int id, JsonPatchDocument<PatchMentorDto> mentorDtoPatch)
        {
            try
            {
                var mentor = await mentorRepository.GetMentorById(id);
                if (mentor == null)
                    return NotFound(MessageStrings.MessageFailureMentorDoesNotExist);
                var mentorDto = mapper.Map<PatchMentorDto>(mentor);
                if(mentorDto == null)
                   throw new System.Exception($"Unable to map Dto of type {typeof(Mentor)} to domain object of type {typeof(PatchMentorDto)}");
                //Apply jsonDocument to the Dto
                mentorDtoPatch.ApplyTo(mentorDto);
                mapper.Map(mentorDto,mentor);
                if(await mentorRepository.Update(mentor))
                  return NoContent();
                throw new System.Exception("The mentor information was not updated");   
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex,ex.Message);
                return StatusCode(500);
            }
        }

    }
}
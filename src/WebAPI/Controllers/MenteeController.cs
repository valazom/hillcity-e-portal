using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Common.Resoures;
using Domain.Enums;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebAPI.Data.Repository;
using WebAPI.Dtos.Engagement;
using WebAPI.Dtos.EngagementTask;
using WebAPI.Dtos.Mentee;
using WebAPI.Dtos.Mentor;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MenteeController : ControllerBase
    {
        private readonly IMenteeRepository menteeRepository;
        private readonly IUserRepository userRepository;
        private readonly ILogger<MenteeController> logger;
        private readonly IMapper mapper;

        public MenteeController(IMenteeRepository menteeRepository, IUserRepository userRepository,
           ILogger<MenteeController> logger, IMapper mapper)
        {
            this.menteeRepository = menteeRepository;
            this.userRepository = userRepository;
            this.logger = logger;
            this.mapper = mapper;
        }

        [Description("Gets all mentees in the system")]               
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        [HttpGet("allmentees")]
        //[Authorize(Roles = "Administrator")]
        public async Task<IActionResult> GetAllMentees()
        {
           try
           {
               var mentees = await menteeRepository.GetAllMentees();
               var menteesDto = mapper.Map<IEnumerable<ReturnMenteeDto>>(mentees);
               if(menteesDto == null)
                  throw new System.Exception($"Unable to map Dto of type {typeof(Mentee)} to domain object of type {typeof(ReturnMenteeDto)}");
                return Ok(menteesDto);
           }
           catch (System.Exception ex)
           {
              logger.LogError(ex,ex.Message);
              return StatusCode(500);
           }
        }

        [Description("Get mentee information based on the userId")]               
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpGet("{userId}",Name="mentee")]
        [Authorize(Roles = "Mentee,Administrator")]
        public async Task<IActionResult> GetMentee(int userId)
        {
           try
           {
               var mentee = await menteeRepository.GetMenteebyId(userId);
               if(mentee == null)
                 return NotFound(MessageStrings.MessageFailureMenteeDoesNotExist);
               var menteeDto = mapper.Map<ReturnMenteeDto>(mentee);
               if(menteeDto == null)
                  throw new System.Exception($"Unable to map Dto of type {typeof(Mentee)} to domain object of type {typeof(ReturnMenteeDto)}");
                return Ok(menteeDto);
           }
           catch (System.Exception ex)
           {
              logger.LogError(ex,ex.Message);
              return StatusCode(500);
           }
        }

        [Description("Create a new mentee information")]               
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        [HttpPost("creatementee")]
        [Authorize(Roles = "Mentee,Administrator")]
        public async Task<IActionResult> CreateMentee(CreateMenteeDto menteeDto)
        {
           try
           {
               var menteeToCreate = mapper.Map<Mentee>(menteeDto);
               if(menteeToCreate == null)
                  throw new System.Exception($"Unable to map Dto of type {typeof(CreateMenteeDto)} to domain object of type {typeof(Mentee)}");
               var menteeInDb = await menteeRepository.GetMenteebyId(menteeToCreate.UserId);
               if(menteeInDb != null)
                  return BadRequest(MessageStrings.MessageFailureMenteeAlreadyExist);
               var menteeCreated =await menteeRepository.Create(menteeToCreate);
               var menteeCreatedDto = new ReturnMenteeDto();
               menteeCreatedDto.UserId = menteeCreated.UserId;
               menteeCreatedDto.University = menteeCreated.University;
               menteeCreatedDto.Course = menteeCreated.Course;
               menteeCreatedDto.DateOfAdmission = menteeCreated.DateOfAdmission;
               menteeCreatedDto.DateOfGraduation = menteeCreated.DateOfGraduation;
               return CreatedAtRoute(routeName:"mentee",
                                     routeValues:new {userId = menteeCreated.UserId},
                                     value:menteeCreatedDto);
           }
           catch (System.Exception ex)
           {
              logger.LogError(ex,ex.Message);
              return StatusCode(500);
           }
        }

        [Description("Get the mentee's assigned mentor")]               
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        [HttpGet("mentor/{userId}")]
        [Authorize(Roles = "Mentee,Administrator")]
        public async Task<IActionResult> GetMentorAssigned(int userId)
        {
            try
            {
               var mentee = await menteeRepository.GetMenteebyId(userId);
               if(mentee == null)
                 return NotFound(MessageStrings.MessageFailureMenteeDoesNotExist);
               var mentor = mentee.Mentor;
               var mentorDto = mapper.Map<AssignedMentorDto>(mentor);
               if(mentorDto == null)
                  throw new System.Exception($"Unable to map Dto of type {typeof(Mentor)} to domain object of type {typeof(AssignedMentorDto)}");
               var mentorViewDto = new MentorViewDto();
               mentorViewDto.NumberOfCompletedEngagements = mentee.Engagements.Where(c => c.Status == EngagementStatus.Completed).Count();
               mentorViewDto.NumberOfPendingEngagements = mentee.Engagements.Where(c => c.Status == EngagementStatus.Pending).Count();
               mentorViewDto.AssignedMentor = mentorDto;
               return Ok(mentorViewDto);
            }
            catch (System.Exception ex)
            {
              logger.LogError(ex,ex.Message);
              return StatusCode(500);
            }
        }
        
        [Description("Get the mentee's Engagements")]               
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        [HttpGet("engagements/{userId}")]
        [Authorize(Roles = "Mentee,Administrator")]
        public async Task<IActionResult> GetEngagements(int userId)
        {
            try
            {
               var mentee = await menteeRepository.GetMenteebyId(userId);
               if(mentee == null)
                 return NotFound(MessageStrings.MessageFailureMenteeDoesNotExist);
               var engagements = mentee.Engagements;
               var engagementsDto = mapper.Map<IEnumerable<ReturnEngagementDto>>(engagements);
               if(engagementsDto == null)
                 throw new System.Exception($"Unable to map Dto of type {typeof(Engagement)} to domain object of type {typeof(ReturnEngagementDto)}");
               var menteeEnagagement = new MenteeEnagagement();
               menteeEnagagement.NumberOfCompletedEngagements = mentee.Engagements.Where(c => c.Status == EngagementStatus.Completed).Count();
               menteeEnagagement.NumberOfPendingEngagements = mentee.Engagements.Where(c => c.Status == EngagementStatus.Pending).Count();
               menteeEnagagement.Engagements = engagementsDto.OrderByDescending(c => c.ScheduledDateUtc);
               return Ok(menteeEnagagement);
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500);
            }
        }
        [Description("Gets engagement task created by a mentee")]               
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpGet("engagementtask/{Id}")]
        [Authorize(Roles = "Mentee,Administrator")]
        public async Task<IActionResult> GetEngagementTask(int Id)
        {
            try
            {
                var mentee = await menteeRepository.GetMenteebyId(Id);
                if(mentee == null) 
                    return NotFound(MessageStrings.MessageFailureMentorDoesNotExist);
                var engagementTasks = mentee.Engagements.Where(c => c.Task != null).Select(e => e.Task);
                var engagementTaskDtos = mapper.Map<IEnumerable<ReturnEngagementTaskDto>>(engagementTasks);
                if(engagementTaskDtos == null)
                     throw new System.Exception($"Unable to map Dto of type {typeof(EngagementTask)} to domain object of type {typeof(ReturnEngagementTaskDto)}");
                return Ok(engagementTaskDtos);

            }
            catch (System.Exception ex)
            {
               logger.LogError(ex, ex.Message);
               return StatusCode(500);
            }
        }

        [Description("Updates mentee information")]               
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpPut("updatementee")]
        [Authorize(Roles = "Mentee,Administrator")]
        public async Task<IActionResult> UpdateMentee(UpdateMenteeDto menteeDto)
        {
           try
           {
               var mentee = await menteeRepository.GetMenteebyId(menteeDto.userId);
               if(mentee == null)
                 return NotFound(MessageStrings.MessageFailureMenteeDoesNotExist);
               mapper.Map(menteeDto,mentee);
               if(await menteeRepository.Update(mentee))
                 return NoContent();
               throw new System.Exception("Mentee information was not successfully updated");
           }
           catch (System.Exception ex)
           {
              logger.LogError(ex,ex.Message);
              return StatusCode(500);
           }
        }

        [Description("Updates particular mentee information")]               
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpPatch("updatementee/{id}")]
        [Authorize(Roles = "Mentee,Administrator")]
        public async Task<IActionResult> UpdateMentee(int id,JsonPatchDocument<PatchMenteeDto> menteeDtoPatch)
        {
           try
           {
               var mentee = await menteeRepository.GetMenteebyId(id);
               if(mentee == null)
                 return NotFound(MessageStrings.MessageFailureMenteeDoesNotExist);
               var menteeDto = mapper.Map<PatchMenteeDto>(mentee);
               if(menteeDto == null)
                 throw new System.Exception($"Unable to map Dto of type {typeof(Mentee)} to domain object of type {typeof(PatchMenteeDto)}");
                //Apply json patch document
               menteeDtoPatch.ApplyTo(menteeDto);
               mapper.Map(menteeDto,mentee);
               if(await menteeRepository.Update(mentee))
                 return NoContent();
               throw new System.Exception("The mentee information was not updated successfully");
           }
           catch (System.Exception ex)
           {
              logger.LogError(ex,ex.Message);
              return StatusCode(500);
           }
        }
    }
}
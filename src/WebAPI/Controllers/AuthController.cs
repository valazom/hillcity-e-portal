using System.Threading.Tasks;
using AutoMapper.Configuration;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Data.Repository;
using WebAPI.Dtos.User;
using Common.Resoures;
using AutoMapper;
using Domain.Models;
using Microsoft.Extensions.Configuration;
using System.Security.Claims;
using System.Linq;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System;
using System.IdentityModel.Tokens.Jwt;
using Common.Email.MessageInfo;
using WebAPI.Common;
using Common.Email.Service;
using System.Collections.Generic;
using System.ComponentModel;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthRepository _repo;
        private readonly Microsoft.Extensions.Configuration.IConfiguration _config;
        private readonly IMapper _mapper;
        private readonly EmailArgumentsBuilder _emailArgumentsBuilder;
        private readonly IEmailQueueRepository _emailQueueRepository;
        private readonly ILogger<AuthController> _logger;

        public AuthController(IAuthRepository repo,
              Microsoft.Extensions.Configuration.IConfiguration config,
              IMapper mapper,
              EmailArgumentsBuilder emailArgumentsBuilder,
              IEmailQueueRepository emailQueueRepository,
              ILogger<AuthController> logger
              )
        {
            _repo = repo;
            _config = config;
            _mapper = mapper;
            _emailArgumentsBuilder = emailArgumentsBuilder;
            _emailQueueRepository = emailQueueRepository;
            _logger = logger;
        }
        
        [Description("Perform new registration on the system")]   
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(500)]
        [HttpPost("register")]
        public async Task<IActionResult> Register(UserRegisterDto userRegisterDto)
        {
            try
            {
                if (userRegisterDto == null)
                    return BadRequest(MessageStrings.MessageFailureUserRegInfo);

                var userToCreate = _mapper.Map<User>(userRegisterDto);
                if (userToCreate == null)
                    throw new System.Exception($"Unable to map Dto of type {typeof(UserRegisterDto)} to domain object of type {typeof(User)}");

                if (await _repo.UsernameExist(userToCreate.Username))
                    return BadRequest(MessageStrings.MessageFailureUsernameExist);

                if (await _repo.EmailExist(userToCreate.Email))
                    return BadRequest(MessageStrings.MessageFailureEmailExist);

                var userCreated = await _repo.Register(userToCreate, userRegisterDto.Password);

                _logger.LogInfo($"User with username : {userCreated.UserId} has been created");

                var roleToAdd = await _repo.GetRoleByName(userRegisterDto.RoleName);

                if (userCreated != null && roleToAdd != null){
                   await _repo.AddUserToRole(userCreated, roleToAdd);
                   _logger.LogInfo($"User with username : {userCreated.UserId} has been added to role : {roleToAdd.RoleName}");
                }
                    
                // build emailArguments for user registration
                var emailArguments = _emailArgumentsBuilder
                                      .AddUserArguments(userCreated)
                                      .AddSubjectArgument(EmailTemplateStrings.EmailArgumentRegistrationSubject)
                                      .AddToAddressArgument(userCreated.Email)
                                      .AddFromAddressArgument(_config.GetSection("EmailSettings:FromAddress").Value)
                                      .AddAdminEmail(_config.GetSection("EmailSettings:FromAddress").Value)
                                      .AddRoleArgument(roleToAdd.RoleName)
                                      .Build();
                //create EmailQueue
                await _emailQueueRepository.CreateEmailQueue(emailArguments, EmailMessageType.Registration);
                _logger.LogInfo($"EmailQueue for registraion of user : {userCreated.UserId} has been created");
                return StatusCode(201);
            }
            catch(Exception ex){
                _logger.LogError(ex,ex.Message);
                return StatusCode(500);
            }
            
        }

        [Description("Login into the system with user credentials")]   
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        [HttpPost("login")]
        public async Task<IActionResult> Login(UserLoginDto userLoginDto)
        {
            try
            {
                var user = await _repo.Login(userLoginDto.Username, userLoginDto.Password);
                if (user == null)
                    return Unauthorized();
                _logger.LogInfo($"User : {user.UserId} was successfully authenticated");
                // generate JWT token for the user
                var token = GenerateToken(user);
                _logger.LogInfo($"User token: {token} successfully generated");
                return Ok(
                    new
                    {
                        token = token
                    });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex,ex.Message);
                return StatusCode(500);
            }
            
        }

        [Description("forget password request")]   
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpPost("forgetpassword")]
        public async Task<IActionResult> Forgetpassword(UserForgetPasswordDto userForgetPasswordDto)
        {
            try
            {
                var user = await _repo.GetUserByEmail(userForgetPasswordDto.Email);
                if (user == null)
                    return NotFound("Did not find a user with the email specified");
                var link = BuildLink(user, userForgetPasswordDto);
                _logger.LogInfo($"Password reset link {link} generated for user: {user.UserId}");
                // build email arguments
                var emailArguments = _emailArgumentsBuilder
                                        .AddUserArguments(user)
                                        .AddSubjectArgument(EmailTemplateStrings.EmailArgumentPasswordResetSubject)
                                        .AddToAddressArgument(user.Email)
                                        .AddFromAddressArgument(_config.GetSection("EmailSettings:FromAddress").Value)
                                        .AddAdminEmail(_config.GetSection("EmailSettings:FromAddress").Value)
                                        .AddLinkArgument(link)
                                        .Build();
                // Create Email Queue
                var EmailQueueCreated = await _emailQueueRepository.CreateEmailQueue(emailArguments, EmailMessageType.ForgetPassword);
                if (EmailQueueCreated == null){
                    _logger.LogError($"EmailQueue password reset was not created for user:{user.UserId}");
                    return StatusCode(500);
                } 
                _logger.LogInfo($"EmailQueue password reset created for user:{user.UserId}");
                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex,ex.Message);
                return StatusCode(500);
            }
           
        }
        
        [Description("Perform reset password request")]   
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpPost("resetpassword")]
        public async Task<IActionResult> ResetPassword(UserResetPasswordDto userResetPasswordDto)
        {
            try
            {
                var username = Encryption.DecryptString(userResetPasswordDto.UsernameHash,
                        _config.GetSection("AppSettings:encryptKey").Value);
                if (!(await _repo.UsernameExist(username)))
                    return NotFound(MessageStrings.MessageFailureForgetPasswordUsernameDoesNotExist);
                var user = await _repo.GetUserByUsername(username);
                if (await _repo.ChangePassword(user, userResetPasswordDto.Password))
                    return NoContent();
                return StatusCode(500);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex,ex.Message);
                return StatusCode(500);
            }
        }
        private string BuildLink(User user, UserForgetPasswordDto userForgetPasswordDto)
        {
            var usernameHash = Encryption.EncryptString(user.Username,
                  _config.GetSection("AppSettings:encryptKey").Value);
            var link = userForgetPasswordDto.DomainName + "?username=" + usernameHash;
            return link;
        }

        private string GenerateToken(User user)
        {
            // generate user claims 
            var claims = new []{
               new Claim(ClaimTypes.NameIdentifier, user.UserId.ToString()),
               new Claim(ClaimTypes.GivenName,user.Username),
               new Claim(ClaimTypes.Role, string.Join(",",user.UserRoles.Select(x => x.Role.RoleName)))
            };
            // get security key to encode the token
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.GetSection("AppSettings:Token").Value));
            
            // Create signing credentials with hash algorithm
            var creds = new SigningCredentials(key,SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor{
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddHours(5),
                SigningCredentials = creds
            };
            // create a token handler to write token to the web reponse
            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}
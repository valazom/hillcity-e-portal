using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Common.Email.MessageInfo;
using Common.Resoures;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebAPI.Common;
using WebAPI.Data.Repository;
using WebAPI.Dtos;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize()]
    public class AdministratorController : ControllerBase
    {
        private readonly IEngagementRepository engagementRepository;
        private readonly IUserRepository userRepository;
        private readonly IMenteeRepository menteeRepository;
        private readonly IMentorRepository mentorRepository;
        private readonly IEmailQueueRepository emailQueueRepository;
        private readonly CommonEmailArgument emailArgument;
        private readonly Microsoft.Extensions.Configuration.IConfiguration config;
        private readonly IMapper mapper;
        private readonly ILogger<AdministratorController> logger;

        public AdministratorController(IEngagementRepository engagementRepository, 
            IUserRepository userRepository,
            IMenteeRepository menteeRepository,
            IMentorRepository mentorRepository,
            IEmailQueueRepository emailQueueRepository,
            CommonEmailArgument emailArgument,
            Microsoft.Extensions.Configuration.IConfiguration config,
            IMapper mapper, ILogger<AdministratorController> logger)
        {
            this.engagementRepository = engagementRepository;
            this.userRepository = userRepository;
            this.menteeRepository = menteeRepository;
            this.mentorRepository = mentorRepository;
            this.emailQueueRepository = emailQueueRepository;
            this.emailArgument = emailArgument;
            this.config = config;
            this.mapper = mapper;
            this.logger = logger;
        }

        [Description("Assigns a mentor to a mentee")]               
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpPost("assignmentor")]
        public async Task<IActionResult> AssignMentorToMentee(AssignMentorMenteeDto assignMentorMenteeDto)
        {
            try
            {
                var mentor = await ValidateMentorinformation(assignMentorMenteeDto);
                if(mentor == null)
                    return NotFound(MessageStrings.MessageFailureMentorDoesNotExist);
                var mentee = await ValidateMenteeinformation(assignMentorMenteeDto);
                if(mentee == null)
                    return NotFound(MessageStrings.MessageFailureMenteeDoesNotExist);
                // Update a mentee information
                mentee.Mentor = mentor;
                if(await menteeRepository.Update(mentee))
                {
                    // build email arguments for the mentee
                    var menteeEmailArguments = emailArgument.GetMenteeEmailArguments(mentor,mentee);
                    // build email arguments for the mentor
                    var mentorEmailArguments = emailArgument.GetMentorEmailArguments(mentor,mentee);
                    //Create email queue for mentee assignemnt
                    await emailQueueRepository.CreateEmailQueue(menteeEmailArguments,EmailMessageType.MentorAssignment);
                    //Create email queue for mentor assignemnt
                    await emailQueueRepository.CreateEmailQueue(mentorEmailArguments,EmailMessageType.MenteeAssgnment);
                    return NoContent();
                }
                    
                throw new System.Exception("Mentee information was not updated");
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex,ex.Message);
                return StatusCode(500);
            }
        }
        
        private async Task<Mentor> ValidateMentorinformation(AssignMentorMenteeDto assignMentorMenteeDto)
        {
            //Get mentor in the database
            var mentor = await mentorRepository.GetMentorById(assignMentorMenteeDto.MentorId);
            //Do null check
            if (mentor == null)
                return null;
            return mentor;
        }
        private async Task<Mentee> ValidateMenteeinformation(AssignMentorMenteeDto assignMentorMenteeDto)
        {
            var mentee = await menteeRepository.GetMenteebyId(assignMentorMenteeDto.MenteeId);
            if(mentee == null)
                return null;
            return mentee;
        }
    }
}
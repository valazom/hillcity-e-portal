using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Common.Resoures;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NSwag.Annotations;
using WebAPI.Data.Repository;
using WebAPI.Dtos.Role;
using WebAPI.Dtos.User;
using WebAPI.Helpers;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository userRepository;
        private readonly IAuthRepository authRepository;
        private readonly IMapper mapper;
        private readonly ILogger<UserController> logger;

        public IOptions<CloudinarySettings> CloudinaryConfig;

        public Cloudinary Cloudinary;

        public UserController(IUserRepository userRepository, IAuthRepository authRepository,
        IMapper mapper, ILogger<UserController> logger, IOptions<CloudinarySettings> cloudinaryConfig)
        {
            this.userRepository = userRepository;
            this.authRepository = authRepository;
            this.mapper = mapper;
            this.logger = logger;
            this.CloudinaryConfig = cloudinaryConfig;

            Account acc = new Account(
                this.CloudinaryConfig.Value.CloudName,
                this.CloudinaryConfig.Value.ApiKey,
                this.CloudinaryConfig.Value.ApiSecret
            );

            Cloudinary = new Cloudinary(acc);
        }

        [Description("Upload photo for a user in the system")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        [HttpPut("{userId}/photo")]
        public async Task<IActionResult> AddPhotoForUser(int userId, [FromForm] UserPhotoDto userPhotoDto)
        {
            try
            {
                var user = await userRepository.GetUserById(userId);
                if (user == null)
                    return NotFound("User does not exist");

                var file = userPhotoDto.File;
                var uploadResult = new ImageUploadResult();

                if (file.Length > 0)
                {
                    using (var stream = file.OpenReadStream())
                    {
                        var uploadParams = new ImageUploadParams()
                        {
                            File = new FileDescription(file.Name, stream),
                            Transformation = new Transformation().Width(500).Height(500).Crop("fill").Gravity("face")
                        };

                        uploadResult = Cloudinary.Upload(uploadParams);
                    }
                }
                user.Photo = uploadResult.Uri.ToString();
                user.PublicId = uploadResult.PublicId;
                
                if(await userRepository.Update(user)){
                    var userDto = mapper.Map<UserDto>(user);
                    if(userDto == null)
                        throw new System.Exception($"Unable to map Dto of type {typeof(User)} to domain object of type {typeof(UserDto)}");
                    return Ok(userDto);
                }
                throw new System.Exception("User photo was not updated");
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500);
            }

        }

        [Description("Gets all users in the system")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        [HttpGet("allUsers")]
        public async Task<IActionResult> AllUsers()
        {
            try
            {
                var users = await userRepository.GetAllUsers();
                var usersDto = mapper.Map<IEnumerable<UserDto>>(users);
                if (usersDto == null)
                    throw new System.Exception($"Unable to map Dto of type {typeof(User)} to domain object of type {typeof(UserDto)}");
                return Ok(usersDto.ToList());
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500);
            }
        }

        [Description("Gets a user with username provided")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpGet("userbyusername/{username}")]
        public async Task<IActionResult> GetUser(string username)
        {
            try
            {
                var user = await userRepository.GetUserByUsername(username);
                if (user == null)
                    return NotFound(MessageStrings.MessageFailureUsernameExist);
                var userDto = mapper.Map<UserDto>(user);
                if (userDto == null)
                    throw new System.Exception($"Unable to map Dto of type {typeof(User)} to domain object of type {typeof(UserDto)}");
                return Ok(userDto);
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500);
            }
        }

        [Description("Gets a user with an email provided")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpGet("userbyemail/{email}")]
        public async Task<ActionResult> GetUserByEmail(string email)
        {
            try
            {
                var user = await userRepository.GetUserByEmail(email);
                if (user == null)
                    return NotFound(MessageStrings.MessageFailureUsernameExist);
                var userDto = mapper.Map<UserDto>(user);
                if (userDto == null)
                    throw new System.Exception($"Unable to map Dto of type {typeof(User)} to domain object of type {typeof(UserDto)}");
                return Ok(userDto);
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500);
            }
        }

        [Description("Gets a user with an id provided")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpGet("user/{id}")]
        public async Task<IActionResult> GetUser(int id)
        {
            try
            {
                var user = await userRepository.GetUserById(id);
                if (user == null)
                    return NotFound(MessageStrings.MessageFailureUsernameExist);
                var userDto = mapper.Map<UserDto>(user);
                if (userDto == null)
                    throw new System.Exception($"Unable to map Dto of type {typeof(User)} to domain object of type {typeof(UserDto)}");
                return Ok(user);
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500);
            }
        }

        [Description("Gets all roles in the system")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        [HttpGet("allroles")]
        public async Task<IActionResult> GetAllRoles()
        {
            try
            {
                var roles = await userRepository.GetAllRoles();
                var rolesDto = mapper.Map<IEnumerable<RoleDto>>(roles);
                if (rolesDto == null)
                    throw new System.Exception($"Unable to map Dto of type {typeof(Role)} to domain object of type {typeof(RoleDto)}");
                return Ok(rolesDto);
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500);
            }
        }

        [Description("Gets set of users that belong to the role name provided")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpGet("userinrole/{rolename}")]
        public async Task<IActionResult> GetUsersInRole(string roleName)
        {
            try
            {
                var role = authRepository.GetRoleByName(roleName);
                if (role == null)
                    return NotFound(MessageStrings.MessageFailureRoleDoesNotExist);
                var users = await userRepository.GetUsersInRole(roleName);
                var usersDto = mapper.Map<IEnumerable<UserDto>>(users);
                if (usersDto == null)
                    throw new System.Exception($"Unable to map Dto of type {typeof(User)} to domain object of type {typeof(UserDto)}");
                return Ok(usersDto);
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500);
            }
        }

        [Description("Gets set of roles a user belongs to")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpGet("userroles/{username}")]
        public async Task<IActionResult> GetUserRoles(string username)
        {
            try
            {
                var user = await userRepository.GetUserByUsername(username);
                if (user == null)
                    return NotFound(MessageStrings.MessageFailureUsernameExist);
                var userroles = user.UserRoles.ToList();
                var roleDto = mapper.Map<IEnumerable<RoleDto>>(userroles);
                if (roleDto == null)
                    throw new System.Exception($"Unable to map Dto of type {typeof(Role)} to domain object of type {typeof(RoleDto)}");
                return Ok(roleDto);
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500);
            }
        }

        [Description("Updates user details supplied")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpPut("updateuser")]
        public async Task<IActionResult> UpdateUser(UserDto userDto)
        {
            try
            {

                var userToUpdate = await authRepository.GetUserByUsername(userDto.Username);
                if (userToUpdate == null)
                    return NotFound(MessageStrings.MessageFailureUsernameExist);
                mapper.Map(userDto, userToUpdate);
                if (await userRepository.Update(userToUpdate))
                    return NoContent();
                throw new System.Exception("The user information was not updated in the database");
            }
            catch (System.Exception ex)
            {

                logger.LogError(ex, ex.Message);
                return StatusCode(500);
            }
        }

        [Description("Updates a sub set of user information")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpPatch("updateuser/{id}")]
        public async Task<IActionResult> UpdateUser(int id, JsonPatchDocument<UserUpdateDto> userDtoPatch)
        {
            try
            {
                var userInDb = await userRepository.GetUserById(id);
                if (userInDb == null)
                    return NotFound(MessageStrings.MessageFailureUsernameExist);
                var userDto = mapper.Map<UserUpdateDto>(userInDb);
                if (userDto == null)
                    throw new System.Exception($"Unable to map Dto of type {typeof(User)} to domain object of type {typeof(UserUpdateDto)}");
                // Apply Json patch
                userDtoPatch.ApplyTo(userDto);
                mapper.Map(userDto, userInDb);
                if (await userRepository.Update(userInDb))
                    return NoContent();
                throw new System.Exception("The user information was not updated in the database");
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500);
            }
        }
        [Description("Deletes a particular user")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpDelete("deleteuser")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            try
            {
                var userInDb = await userRepository.GetUserById(id);
                if (userInDb == null)
                    return NotFound(MessageStrings.MessageFailureUsernameExist);
                userInDb.IsDeleted = true;
                if (await userRepository.Update(userInDb))
                    return NoContent();
                throw new System.Exception("User was not deleted successfully");
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500);
            }
        }
    }
}
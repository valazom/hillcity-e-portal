using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using AutoMapper;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Common.Email.MessageInfo;
using Common.Resoures;
using Common.Wrappers;
using Domain.Enums;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WebAPI.Common;
using WebAPI.Data.Repository;
using WebAPI.Dtos.Engagement;
using WebAPI.Dtos.EngagementTask;
using WebAPI.Dtos.Mentee;
using WebAPI.Dtos.Mentor;
using WebAPI.Helpers;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class EngagementController : ControllerBase
    {
        private readonly IEngagementRepository engagementRepository;
        private readonly IMapper mapper;
        private readonly IMentorRepository mentorRepository;
        private readonly IMenteeRepository menteeRepository;
        private readonly IEmailQueueRepository emailQueueRepository;
        private readonly EmailArgumentsBuilder emailArgumentsBuilder;
        private readonly ILogger<EngagementController> logger;

        public Microsoft.Extensions.Configuration.IConfiguration Config { get; }
        public IOptions<CloudinarySettings> CloudinarySettings { get; }
        public Cloudinary Cloudinary { get; }

        public EngagementController(IEngagementRepository engagementRepository, IMapper mapper,
           IMentorRepository mentorRepository, IMenteeRepository menteeRepository,
           IEmailQueueRepository emailQueueRepository,
           EmailArgumentsBuilder emailArgumentsBuilder,
           ILogger<EngagementController> logger,
           Microsoft.Extensions.Configuration.IConfiguration config,
           IOptions<CloudinarySettings> cloudinarySettings
        )
        {
            this.engagementRepository = engagementRepository;
            this.mapper = mapper;
            this.mentorRepository = mentorRepository;
            this.menteeRepository = menteeRepository;
            this.emailQueueRepository = emailQueueRepository;
            this.emailArgumentsBuilder = emailArgumentsBuilder;
            this.logger = logger;
            Config = config;
            CloudinarySettings = cloudinarySettings;

            Account acc = new Account(
              this.CloudinarySettings.Value.CloudName,
              this.CloudinarySettings.Value.ApiKey,
              this.CloudinarySettings.Value.ApiSecret
            );

            Cloudinary = new Cloudinary(acc);
        }

        [Description("Gets all Engagements in the system")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(500)]
        [HttpGet("allengagements")]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> GetAllEngagements()
        {
            try
            {
                var engagements = await engagementRepository.GetAllEngagement();
                var engagementsDto = mapper.Map<IEnumerable<ReturnEngagementDto>>(engagements);
                if (engagementsDto == null)
                    throw new System.Exception($"Unable to map Dto of type {typeof(Engagement)} to domain object of type {typeof(ReturnEngagementDto)}");
                return Ok(engagementsDto);
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500);
            }
        }

        [Description("Gets a particular engagement from the system")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpGet("{id}", Name = "engagement")]
        public async Task<IActionResult> GetEngagement(int id)
        {
            try
            {
                var engagement = await engagementRepository.GetEngagementbyId(id);
                if (engagement == null)
                    return NotFound(MessageStrings.MessageFailureEngagementDoesNotExist);
                var engagementDto = mapper.Map<ReturnEngagementDto>(engagement);
                if (engagementDto == null)
                    throw new System.Exception($"Unable to map Dto of type {typeof(Engagement)} to domain object of type {typeof(ReturnEngagementDto)}");
                return Ok(engagementDto);
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500);
            }
        }

        [Description("Creates an engagement in the system")]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpPost("createengagement")]
        public async Task<IActionResult> CreateEngagement(CreateEngagementDto engagementDto)
        {
            try
            {

                var engagement = mapper.Map<Engagement>(engagementDto);
                if (engagement == null)
                    throw new System.Exception($"Unable to map Dto of type {typeof(CreateEngagementDto)} to domain object of type {typeof(Engagement)}");
                //get the mentor and mentee to be assigned to the engagement
                if (!(await UpdateMenteeOrMentorByCreateDto(engagementDto, engagement)))
                    return NotFound("Either mentor or mentee does not exist");
                // create the engagement
                var engagementCreated = await engagementRepository.CreateEngagement(engagement);
                if (engagementCreated == null)
                    throw new System.Exception("Engagement was not created");
                var engagementCreatedDto = mapper.Map<ReturnEngagementDto>(engagementCreated);
                var engagementMenteeEmailArguments = GetMenteeEngagementEmailArguments(engagementCreated);
                var engagementMentorEmailArguments = GetMentorEngagementEmailArguments(engagementCreated);
                await emailQueueRepository.CreateEmailQueue(engagementMenteeEmailArguments, EmailMessageType.Engagement);
                await emailQueueRepository.CreateEmailQueue(engagementMentorEmailArguments, EmailMessageType.Engagement);
                return CreatedAtRoute("engagement", new { id = engagementCreated.EngagementId }, engagementCreatedDto);
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500);
            }
        }

        private Dictionary<string, string> GetMenteeEngagementEmailArguments(Engagement engagementCreated)
        {
            var emailMenteeArgumentsBuilder = new EmailArgumentsBuilder();
            emailMenteeArgumentsBuilder.AddFullName(engagementCreated.Mentee.User.FullName);
            emailMenteeArgumentsBuilder.AddCreatedByUsername(engagementCreated.Mentee.User.FullName);
            emailMenteeArgumentsBuilder.AddSubjectArgument("New Engagement");
            emailMenteeArgumentsBuilder.AddToAddressArgument(engagementCreated.Mentee.User.Email);
            emailMenteeArgumentsBuilder.AddFromAddressArgument(Config.GetSection("EmailSettings:FromAddress").Value);

            AddEngagementArguments(emailMenteeArgumentsBuilder, engagementCreated);
            return emailMenteeArgumentsBuilder.Build();
        }
        private Dictionary<string, string> GetMentorEngagementEmailArguments(Engagement engagementCreated)
        {
            var emailMentorArgumentsBuilder = new EmailArgumentsBuilder();
            emailMentorArgumentsBuilder.AddFullName(engagementCreated.Mentor.User.FullName);
            emailMentorArgumentsBuilder.AddCreatedByUsername(engagementCreated.Mentee.User.FullName);
            emailMentorArgumentsBuilder.AddSubjectArgument("New Engagement");
            emailMentorArgumentsBuilder.AddToAddressArgument(engagementCreated.Mentor.User.Email);
            emailMentorArgumentsBuilder.AddFromAddressArgument(Config.GetSection("EmailSettings:FromAddress").Value);
            AddEngagementArguments(emailMentorArgumentsBuilder, engagementCreated);
            return emailMentorArgumentsBuilder.Build();
        }
        private void AddEngagementArguments(EmailArgumentsBuilder emailArgumentsBuidler, Engagement engagementCreated)
        {
            emailArgumentsBuidler.AddName(engagementCreated.Subject);
            emailArgumentsBuidler.AddDate(engagementCreated.ScheduledDateUtc);
            emailArgumentsBuidler.AddStartTime(engagementCreated.StartTime.ToString());
            emailArgumentsBuidler.AddMode(engagementCreated.CommunicationMode.ToString());
            if (engagementCreated.CommunicationMode == CommunicationMode.FaceToFace)
                emailArgumentsBuidler.AddAddress(engagementCreated.Location.Address);
            else
                emailArgumentsBuidler.AddAddress(string.Empty);
        }

        [Description("Get mentee assigned to particular engagement in the system")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpGet("mentee/{id}")]
        public async Task<IActionResult> GetMenteeFromEngagment(int id)
        {
            try
            {
                var engagement = await engagementRepository.GetEngagementbyId(id);
                if (engagement == null)
                    return NotFound(MessageStrings.MessageFailureMentorDoesNotExist);
                var mentee = engagement.Mentee;
                var menteeDto = mapper.Map<ReturnMenteeDto>(mentee);
                if (menteeDto == null)
                    throw new System.Exception($"Unable to map Dto of type {typeof(Mentee)} to domain object of type {typeof(ReturnMenteeDto)}");
                return Ok(menteeDto);
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500);
            }
        }

        [Description("Get mentor assigned to particular engagement in the system")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpGet("mentor/{id}")]
        public async Task<IActionResult> GetMentorFromEngagment(int id)
        {
            try
            {
                var engagement = await engagementRepository.GetEngagementbyId(id);
                if (engagement == null)
                    return NotFound(MessageStrings.MessageFailureMentorDoesNotExist);
                var mentor = engagement.Mentor;
                var mentorDto = mapper.Map<ReturnMentorDto>(mentor);
                if (mentorDto == null)
                    throw new System.Exception($"Unable to map Dto of type {typeof(Mentor)} to domain object of type {typeof(ReturnMentorDto)}");
                return Ok(mentorDto);

            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500);
            }
        }

        [Description("Get the engagementTask of a particular engagement in the system")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpGet("engagementtask/{id}")]
        public async Task<IActionResult> GetEngagementTaskFromEngagement(int id)
        {
            try
            {
                var engagement = await engagementRepository.GetEngagementbyId(id);
                if (engagement == null)
                    return NotFound(MessageStrings.MessageFailureMentorDoesNotExist);
                var engagementTask = engagement.Task;
                var engagementTaskDto = mapper.Map<ReturnEngagementTaskDto>(engagementTask);
                if (engagementTaskDto == null)
                    throw new System.Exception($"Unable to map Dto of type {typeof(EngagementTask)} to domain object of type {typeof(ReturnEngagementTaskDto)}");
                return Ok(engagementTaskDto);
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500);
            }
        }

        [Description("Completes an engagement in the system")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpPost("completeengagement/{id}")]
        [Authorize(Roles = "Mentor,Administrator")]
        public async Task<IActionResult> CompleteEngagement(int id)
        {
            try
            {
                var engagement = await engagementRepository.GetEngagementbyId(id);
                if (engagement == null)
                    return NotFound(MessageStrings.MessageFailureMentorDoesNotExist);
                //check to see if engagement can be completed
                var message = CanCompleteEnagagement(engagement);
                if (!message.isValid)
                    return StatusCode(500, message.Message);
                engagement.Status = EngagementStatus.Completed;
                engagement.DateCompleted = DateTime.Now;
                if (await engagementRepository.Update(engagement))
                    return NoContent();
                throw new System.Exception("Engagement was not completed");
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500);
            }
        }

        public MessageWrapper CanCompleteEnagagement(Engagement engagement)
        {
            var messageWrapper = new MessageWrapper();
            messageWrapper.isValid = true;
            //check to make sure 
            if (engagement.Task != null)
            {
                if (engagement.Task.Status != EngagementTaskStatus.Completed)
                {
                    messageWrapper.isValid = false;
                    messageWrapper.Message = "Engagement Task attached to the engagement has not been completed";
                }
            }
            if (string.IsNullOrEmpty(engagement.EngagementReport))
            {
                messageWrapper.isValid = false;
                messageWrapper.Message = "Engagement report for the engagement has not been uploaded";
            }
            return messageWrapper;
        }

        [Description("Uploads engagement report in the system")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpPut("{Id}/uploadreport")]
        [Authorize(Roles = "Mentee,Administrator")]
        public async Task<IActionResult> UploadEngagement(int Id, [FromForm] EngagementUploadDto engagementUploadDto)
        {
            try
            {
                var engagement = await engagementRepository.GetEngagementbyId(Id);
                if (engagement == null)
                    return NotFound(MessageStrings.MessageFailureEngagementDoesNotExist);
                var file = engagementUploadDto.File;
                var uploadResult = new ImageUploadResult();
                if (file.Length > 0)
                {
                    using (var stream = file.OpenReadStream())
                    {
                        var uploadparams = new ImageUploadParams
                        {
                            File = new FileDescription(file.Name, stream)
                        };
                        uploadResult = Cloudinary.Upload(uploadparams);
                    }
                }
                engagement.EngagementReport = uploadResult.Uri.ToString();
                if (await engagementRepository.Update(engagement))
                {
                    var returnEngagementDto = mapper.Map<ReturnEngagementDto>(engagement);
                    if (returnEngagementDto == null)
                        throw new System.Exception($"Unable to map Dto of type {typeof(Engagement)} to domain object of type {typeof(ReturnEngagementDto)}");
                    return Ok(returnEngagementDto);
                }
                throw new System.Exception("Engagement report was not uploaded");
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500);
            }
        }

        [Description("Update engagement information in the system")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpPut("Update")]
        public async Task<IActionResult> UpdateEngagement(UpdateEngagementDto engagementDto)
        {
            try
            {
                var engagement = await engagementRepository.GetEngagementbyId(engagementDto.EngagementId);
                if (engagement == null)
                    return NotFound(MessageStrings.MessageFailureEngagementDoesNotExist);
                mapper.Map(engagementDto, engagement);
                if (!(await UpdateMenteeOrMentorByUpdateDto(engagementDto, engagement)))
                    return NotFound("either mentor or mentee does not exist");
                if (await engagementRepository.Update(engagement))
                    return NoContent();
                throw new System.Exception("Enagagement was not updated");
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500);
            }
        }

        [Description("Update specific engagement information in the system")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        [HttpPatch("Update/{id}")]
        public async Task<IActionResult> UpdateEngagement(int id, JsonPatchDocument<PatchEngagementDto> engagementDtoPatch)
        {
            try
            {
                var engagement = await engagementRepository.GetEngagementbyId(id);
                if (engagement == null)
                    return NotFound(MessageStrings.MessageFailureEngagementDoesNotExist);
                var engagementDto = mapper.Map<PatchEngagementDto>(engagement);
                if (engagementDto == null)
                    throw new System.Exception($"Unable to map Dto of type {typeof(Engagement)} to domain object of type {typeof(PatchEngagementDto)}");
                // Apply the json Patch
                engagementDtoPatch.ApplyTo(engagementDto);
                mapper.Map(engagementDto, engagement);
                // check to see if mentor or mentee information is mean't to be updated 
                if (engagement.ScheduledDateUtc < DateTime.Now)
                {
                    return BadRequest("Engagement cannot be in the past. Kindly adjust engagement to a future date and try again");
                }

                if (!(await UpdateEngagementUsingPatchDto(engagementDto, engagement)))
                {
                    return NotFound("either mentor or mentee does not exist");
                }
                    
                if (await engagementRepository.Update(engagement))
                {
                    var returnEngagementDto = mapper.Map<ReturnEngagementDto>(engagement);
                    if (returnEngagementDto == null)
                        throw new System.Exception($"Unable to map Dto of type {typeof(Engagement)} to domain object of type {typeof(ReturnEngagementDto)}");
                    // if the engagement status have been updated to Accept
                    // forward and email to the mentee
                    if (engagement.Status == EngagementStatus.Accept)
                    {
                        // get email arguments for engagement being accepted.
                        var menteeEmailArguments = GetMenteeEngagementAcceptEmailArguments(engagement);
                        // create an email queue for the mentee
                        await emailQueueRepository.CreateEmailQueue(menteeEmailArguments,EmailMessageType.EngagementAccepted);
                    }
                    return Ok(returnEngagementDto);
                }
                throw new System.Exception("Engagement was not updated");
            }
            catch (System.Exception ex)
            {
                logger.LogError(ex, ex.Message);
                return StatusCode(500);
            }
        }
        private Dictionary<string,string> GetMenteeEngagementAcceptEmailArguments(Engagement engagement) {
            var memailArgumentsBuidler = new EmailArgumentsBuilder();
            return memailArgumentsBuidler.AddFullName(engagement.Mentee.User.FullName)
                                        .AddEngagementDateCreated(engagement.DateCreatedUtc.ToString("dd/MM/yyyy"))
                                        .AddMentorFullName(engagement.Mentor.User.FullName)
                                        .AddName(engagement.Subject)
                                        .AddDate(engagement.ScheduledDateUtc)
                                        .AddStartTime(engagement.StartTime.ToString())
                                        .AddMode(engagement.CommunicationMode.ToString())
                                        .AddAddress(engagement.Location.Address)
                                        .AddToAddressArgument(engagement.Mentee.User.Email)
                                        .AddFromAddressArgument(Config.GetSection("EmailSettings:FromAddress").Value)
                                        .AddSubjectArgument("Engagement Accepted")
                                        .Build();
        }
        private async Task<bool> UpdateEngagementUsingPatchDto(PatchEngagementDto engagementDto, Engagement engagement)
        {

            if (engagementDto.MentorId.HasValue)
            {
                var mentor = await mentorRepository.GetMentorById(engagementDto.MentorId.Value);
                if (mentor == null)
                    return false;
                engagement.Mentor = mentor;
            }
            // check to see if mentee information is mean't to be updated 
            if (engagementDto.MenteeId.HasValue)
            {
                var mentee = await menteeRepository.GetMenteebyId(engagementDto.MenteeId.Value);
                if (mentee == null)
                    return false;
                engagement.Mentee = mentee;
            }
            return true;
        }
        private async Task<bool> UpdateMenteeOrMentorByCreateDto(CreateEngagementDto createEngagementDto, Engagement engagement)
        {
            var mentor = await mentorRepository.GetMentorById(createEngagementDto.MentorId);
            var mentee = await menteeRepository.GetMenteebyId(createEngagementDto.MenteeId);
            return UpdateMenteeAndMentorInformation(mentor, mentee, engagement);
        }
        private async Task<bool> UpdateMenteeOrMentorByUpdateDto(UpdateEngagementDto updateEngagementDto, Engagement engagement)
        {
            var mentor = await mentorRepository.GetMentorById(updateEngagementDto.MentorId);
            var mentee = await menteeRepository.GetMenteebyId(updateEngagementDto.MenteeId);
            return UpdateMenteeAndMentorInformation(mentor, mentee, engagement);
        }
        private bool UpdateMenteeAndMentorInformation(Mentor mentor, Mentee mentee, Engagement engagement)
        {
            if (mentor == null)
                return false;
            if (mentee == null)
                return false;
            engagement.Mentee = mentee;
            engagement.Mentor = mentor;
            return true;
        }
    }
}
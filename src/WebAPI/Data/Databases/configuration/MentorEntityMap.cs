using Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WebAPI.Data.Databases.configuration
{
    public class MentorEntityMap : IEntityTypeConfiguration<Mentor>
    {
        public void Configure(EntityTypeBuilder<Mentor> builder)
        {
            builder.HasOne(c => c.User).WithMany()
                 .HasForeignKey(c => c.UserId).HasPrincipalKey(c => c.UserId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
using Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WebAPI.Data.Databases.configuration
{
    public class EngagementEntityMap : IEntityTypeConfiguration<Engagement>
    {
        public void Configure(EntityTypeBuilder<Engagement> builder)
        {
            builder.HasOne(c =>c.Mentor).WithMany(c =>c.Engagements)
                 .OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(c => c.Mentee).WithMany(c => c.Engagements)
                  .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
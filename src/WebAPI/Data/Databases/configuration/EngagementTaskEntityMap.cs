using Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WebAPI.Data.Databases.configuration
{
    public class EngagementTaskEntityMap : IEntityTypeConfiguration<EngagementTask>
    {
        public void Configure(EntityTypeBuilder<EngagementTask> builder)
        {
            builder.HasOne(c => c.Engagement).WithOne(c => c.Task)
                .HasPrincipalKey(typeof(Engagement),"EngagementId");
        }
    }
}
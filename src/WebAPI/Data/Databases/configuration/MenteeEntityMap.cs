using Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WebAPI.Data.Databases.configuration
{
    public class MenteeEntityMap : IEntityTypeConfiguration<Mentee>
    {
        public void Configure(EntityTypeBuilder<Mentee> builder)
        {
            builder.HasOne(c => c.User).WithMany()
                .HasForeignKey(c => c.UserId).HasPrincipalKey(c => c.UserId).OnDelete(DeleteBehavior.Restrict);
            
        }
    }
}
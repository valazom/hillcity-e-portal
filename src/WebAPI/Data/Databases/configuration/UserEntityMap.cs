using Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WebAPI.Data.Databases.configuration
{
    public class UserEntityMap : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasIndex(c => c.Email).IsUnique();
            builder.HasIndex(c => c.Username).IsUnique();
            builder.Ignore(c => c.FullName);
        }
    }
}
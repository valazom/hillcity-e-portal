using Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WebAPI.Data.Databases.configuration
{
    public class RoleUserEntityMap : IEntityTypeConfiguration<RoleUser>
    {
        public void Configure(EntityTypeBuilder<RoleUser> builder)
        {
            builder.ToTable("RoleUsers");
            builder.HasKey(c => new { c.UserId,c.RoleId});
            builder.HasOne(c => c.Role).WithMany(c => c.RoleUsers).HasForeignKey(c => c.RoleId);
            builder.HasOne(c => c.User).WithMany(c => c.UserRoles).HasForeignKey(c => c.UserId);

        }
    }
}
using Domain.Models;
using Microsoft.EntityFrameworkCore;
using WebAPI.Data.Databases.configuration;

namespace WebAPI.Data.Databases
{
    public class HepDataContext : DbContext
    {
        public HepDataContext(DbContextOptions options) : base(options)
        {
            
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<RoleUser> RoleUsers { get; set; }
        public DbSet<Mentee> Mentees { get; set; }
        public DbSet<Mentor> Mentors { get; set; }
        public DbSet<Engagement> Engagements { get; set; }
        public DbSet<EngagementTask> EngagementTasks { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<EmailQueue> EmailQueues {get; set;}
        public DbSet<EmailTemplate> EmailTemplates {get; set;}
        public DbSet<Algorithm> Algorithms {get; set;}

        protected override void OnModelCreating(ModelBuilder modelBuilder){

             modelBuilder.ApplyConfiguration(new RoleUserEntityMap());
             modelBuilder.ApplyConfiguration(new EngagementTaskEntityMap());
             modelBuilder.ApplyConfiguration(new EngagementEntityMap());
             modelBuilder.ApplyConfiguration(new MenteeEntityMap());
             modelBuilder.ApplyConfiguration(new MentorEntityMap());
             modelBuilder.ApplyConfiguration(new RoleEntityMap());
             modelBuilder.ApplyConfiguration(new UserEntityMap());
        }
    }
}
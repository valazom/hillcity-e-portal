using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace WebAPI.Data.Databases
{
    public class HepDataContextFactory : IDesignTimeDbContextFactory<HepDataContext>
    {
        public HepDataContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.Development.json")
            .AddJsonFile("appsettings.json")
            .Build();

            var builder = new DbContextOptionsBuilder<HepDataContext>();

            string connectionString = string.Empty;

            if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Production")
            {
                connectionString  = configuration.GetConnectionString("HepProdConnection");
            }
            else
            {
                connectionString = configuration.GetConnectionString("HepConnection");
            }
            
            builder.UseSqlServer(connectionString);

            return new HepDataContext(builder.Options);
        }
    }
}
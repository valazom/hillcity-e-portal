using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Models;
using WebAPI.Data.Repository.Common;

namespace WebAPI.Data.Repository
{
    public interface IEngagementTaskRepository : IRepository
    {
        Task<EngagementTask> CreateEngagementTask(EngagementTask engagementTask);
        Task<bool> CompleteEngagementTask(EngagementTask engagementTask);
        Task<EngagementTask> GetEngagementTaskbyId(int Id);
        Task<IEnumerable<EngagementTask>> GetAllEngagementTask();
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Enums;
using Domain.Models;
using Microsoft.EntityFrameworkCore;
using WebAPI.Data.Databases;
using WebAPI.Data.Repository.Common;

namespace WebAPI.Data.Repository
{
    public class MenteeRepository : RepositoryBase, IMenteeRepository
    {
        public MenteeRepository(HepDataContext context) : base(context)
        {
            
        }
        public async Task<Mentee> CreateMentee(Mentee mentee)
        {
            return await Create(mentee);
        }

        public async Task<IEnumerable<Mentee>> GetAllMentees()
        {
            return await All<Mentee>().ToListAsync();
        }

        public async Task<Mentee> GetMenteebyId(int userId)
        {
            return await Get<Mentee>(c => c.UserId == userId && c.User.IsDeleted == false).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Engagement>> GetMenteeEngagement(int id)
        {
            var mentee = await Get<Mentee>(c => c.UserId == id && c.User.IsDeleted == false).FirstOrDefaultAsync(); 
            if(mentee == null)
               return null;
            return mentee.Engagements;
        }

        public async Task<IEnumerable<Mentee>> GetMenteesWithoutMentors()
        {
            var mentees = await Get<Mentee>(c => c.Mentor == null).ToListAsync();
            return mentees;
        }

        public async Task<Mentor> GetMentorAssigned(int userId)
        {
            var mentee = await Get<Mentee>(c => c.UserId == userId && c.User.IsDeleted == false).FirstOrDefaultAsync();
            if(mentee == null)
               return null;
            return mentee.Mentor;
        }
    }
}
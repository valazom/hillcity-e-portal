using WebAPI.Data.Repository.Common;
using WebAPI.Data.Databases;
using Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace WebAPI.Data.Repository
{
    public class UserRepository : RepositoryBase, IUserRepository
    {
        public UserRepository(HepDataContext context) : base(context)
        {
            
        }

        public async Task<IEnumerable<Role>> GetAllRoles()
        {
            return await All<Role>().ToListAsync();
        }

        public async Task<IEnumerable<User>> GetAllUsers()
        {
            return await Get<User>(c => c.IsDeleted == false).Include(c => c.Location).ToListAsync();
        }

        public async Task<User> GetUserByEmail(string Email)
        {
            return await Get<User>(c => c.Email == Email && c.IsDeleted == false).Include(c => c.Location).FirstOrDefaultAsync();
        }

        public async Task<User> GetUserById(int userId)
        {
            return await Get<User>(c => c.UserId == userId && c.IsDeleted == false).Include(c => c.Location).FirstOrDefaultAsync();
        }

        public async Task<User> GetUserByUsername(string username)
        {
            return await Get<User>(c => c.Username == username && c.IsDeleted == false).Include(c => c.Location).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Role>> GetUserRoles(int id)
        {
            return await Get<RoleUser>(c => c.UserId == id).Select(x => x.Role).ToListAsync();
        }

        public async Task<IEnumerable<Role>> GetUserRoles(string username)
        {
            return await Get<RoleUser>(c => c.User.Username == username).Select(x => x.Role).ToListAsync();
        }

        public async Task<IEnumerable<User>> GetUsersInRole(int roleId)
        {
            return await Get<RoleUser>(c => c.RoleId == roleId).Select(x => x.User).ToListAsync();
        }

        public async Task<IEnumerable<User>> GetUsersInRole(string roleName)
        {
            return await Get<RoleUser>(c => c.Role.RoleName == roleName).Select(x => x.User).ToListAsync();
        }
    }
}
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Models;
using WebAPI.Data.Repository.Common;

namespace WebAPI.Data.Repository
{
    public interface IUserRepository : IRepository
    {
         Task<IEnumerable<User>> GetAllUsers();
         Task<User> GetUserById(int userId);
         Task<User> GetUserByUsername(string username);
         Task<User> GetUserByEmail (string Email);
         Task<IEnumerable<Role>> GetUserRoles(int id);
         Task<IEnumerable<Role>> GetUserRoles(string username);
         Task<IEnumerable<User>> GetUsersInRole(int roleId);
         Task<IEnumerable<User>> GetUsersInRole(string roleName);
         Task<IEnumerable<Role>> GetAllRoles();
    }
}
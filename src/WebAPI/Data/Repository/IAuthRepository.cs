using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Models;
using WebAPI.Data.Repository.Common;

namespace WebAPI.Data.Repository
{
    public interface IAuthRepository : IRepository
    {
         Task<bool> UsernameExist(string username);
         Task<bool> EmailExist(string email);
         Task<User> Register(User user,string password);
         Task<User> Login(string username, string password);
         Task<IEnumerable<Role>> GetAllRoles();
         Task<RoleUser> AddUserToRole(User user,Role role);
         Task<Role> GetRoleByName(string roleName);
         Task<User> GetUserByUsername(string username);
         Task<User> GetUserByEmail(string email);
         Task<bool> ChangePassword(User user,string password);
    }
}
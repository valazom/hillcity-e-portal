using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Models;
using WebAPI.Data.Repository.Common;

namespace WebAPI.Data.Repository
{
    public interface IEngagementRepository : IRepository
    {
         Task<Engagement> CreateEngagement(Engagement engagement);
         Task<IEnumerable<Engagement>> GetAllEngagement();
         Task<Engagement> GetEngagementbyId(int engagementId);
         Task<Mentee> GetMenteeFromEngagement(int engagementId);
         Task<Mentor> GetMentorFromEngagement(int engagementId);
         Task<EngagementTask> GetEngagementTaskFromEngagement(int engagementId);

    }
}
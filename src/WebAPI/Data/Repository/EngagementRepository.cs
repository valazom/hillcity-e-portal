using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Models;
using Microsoft.EntityFrameworkCore;
using WebAPI.Data.Databases;
using WebAPI.Data.Repository.Common;

namespace WebAPI.Data.Repository
{
    public class EngagementRepository : RepositoryBase, IEngagementRepository
    {
        public EngagementRepository(HepDataContext context) : base(context)
        {
            
        }
        public async Task<Engagement> CreateEngagement(Engagement engagement)
        {
            return  await Create(engagement);
        }

        public async Task<IEnumerable<Engagement>> GetAllEngagement()
        {
            return await All<Engagement>().ToListAsync();
        }

        public async Task<Engagement> GetEngagementbyId(int engagementId)
        {
            return await Get<Engagement>(c => c.EngagementId == engagementId).FirstOrDefaultAsync();
        }

        public async Task<EngagementTask> GetEngagementTaskFromEngagement(int engagementId)
        {
            var engagement = await Get<Engagement>(c => c.EngagementId == engagementId).FirstOrDefaultAsync();
            if(engagement == null)
              return null;
            return engagement.Task;
        }

        public async Task<Mentee> GetMenteeFromEngagement(int engagementId)
        {
            var engagement = await Get<Engagement>(c => c.EngagementId == engagementId).FirstOrDefaultAsync();
            if(engagement == null)
              return null;
            return engagement.Mentee;
        }

        public async Task<Mentor> GetMentorFromEngagement(int engagementId)
        {
            var engagement = await Get<Engagement>(c => c.EngagementId == engagementId).FirstOrDefaultAsync();
            if(engagement == null)
              return null;
            return engagement.Mentor;
        }
    }
}
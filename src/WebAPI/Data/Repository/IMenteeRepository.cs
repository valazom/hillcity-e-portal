using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Models;
using WebAPI.Data.Repository.Common;

namespace WebAPI.Data.Repository
{
    public interface IMenteeRepository : IRepository
    {
         Task<Mentee> CreateMentee(Mentee mentee);
         Task<IEnumerable<Engagement>> GetMenteeEngagement(int id);
         Task<IEnumerable<Mentee>> GetAllMentees();
         Task<Mentee> GetMenteebyId(int userId);
         Task<Mentor> GetMentorAssigned(int userId);
         Task<IEnumerable<Mentee>> GetMenteesWithoutMentors();
    }
}
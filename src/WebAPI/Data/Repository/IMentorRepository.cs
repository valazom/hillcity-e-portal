using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Models;
using WebAPI.Data.Repository.Common;

namespace WebAPI.Data.Repository
{
    public interface IMentorRepository : IRepository
    {
         Task<Mentor> CreateMentor(Mentor mentor);
         Task<IEnumerable<Mentor>> GetAllMentors();
         Task<Mentor> GetMentorById(int Id);
         Task<IEnumerable<Mentee>> GetMenteesAssigned(int userId);
         Task<IEnumerable<Engagement>> GetEngagements(int userId);
         Task<IEnumerable<Mentor>> GetMentorsWithoutMentees();

    }
}
using System.Collections.Generic;
using System.Threading.Tasks;
using Common.Email.MessageInfo;
using Domain.Enums;
using Domain.Models;
using WebAPI.Data.Repository.Common;

namespace WebAPI.Data.Repository
{
    public interface IEmailQueueRepository : IRepository
    {
         Task<EmailTemplate> GetEmailTemplate(EmailMessageType messageType);
         Task<EmailQueue> CreateEmailQueue(Dictionary<string,string> emailArguments,EmailMessageType messageType);
         Task<IEnumerable<EmailQueue>> GetPendingEmailQueues();
         Task<IEnumerable<EmailQueue>> GetErrorEmailQueues();
    }
}
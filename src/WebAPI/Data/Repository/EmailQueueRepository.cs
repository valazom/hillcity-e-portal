using System.Collections.Generic;
using System.Threading.Tasks;
using Common.Email.MessageInfo;
using Domain.Models;
using WebAPI.Data.Databases;
using WebAPI.Data.Repository.Common;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Common.Resoures;
using Domain.Enums;

namespace WebAPI.Data.Repository
{
    public class EmailQueueRepository : RepositoryBase, IEmailQueueRepository
    {
        public EmailQueueRepository(HepDataContext context) : base(context)
        {
            
        }
        public async Task<EmailQueue> CreateEmailQueue(Dictionary<string, string> emailArguments,EmailMessageType messageType)
        {
            // Get the email template based on the message Type;
            var emailTemplate = await GetEmailTemplate(messageType);
            string body = string.Empty;
            // build the email message body
            switch(messageType){
                case EmailMessageType.Registration:
                   body = emailTemplate.TemplateHtml
                         .Replace("{Username}",emailArguments[EmailTemplateStrings.EmailTemplateArgumentUsername])
                         .Replace("{Email}",emailArguments[EmailTemplateStrings.EmailTemplateArgumentEmail])
                         .Replace("{RoleName}",emailArguments[EmailTemplateStrings.EmailTemplateArgumentRoleName])
                         .Replace("{adminEmail}",emailArguments[EmailTemplateStrings.EmailTemplateArgumentAdminEmail]);
                   break;
                case EmailMessageType.ForgetPassword:
                   body = emailTemplate.TemplateHtml
                           .Replace("{Username}",emailArguments[EmailTemplateStrings.EmailTemplateArgumentUsername])
                           .Replace("{link}",emailArguments[EmailTemplateStrings.EmailTemplateArgumentLink])
                           .Replace("{adminEmail}",emailArguments[EmailTemplateStrings.EmailTemplateArgumentAdminEmail]);
                   break;
                case EmailMessageType.MentorAssignment:
                    body = emailTemplate.TemplateHtml
                            .Replace("{AssignedFullName}",emailArguments[EmailTemplateStrings.EmailArgumentAssignedFullName])
                            .Replace("{Email}",emailArguments[EmailTemplateStrings.EmailTemplateArgumentEmail])
                            .Replace("{FullName}",emailArguments[EmailTemplateStrings.EmailArgumentFullName])
                            .Replace("{Occupation}",emailArguments[EmailTemplateStrings.EmailArgumentOccupation])
                            .Replace("{State}",emailArguments[EmailTemplateStrings.EmailArgumentState])
                            .Replace("{Country}",emailArguments[EmailTemplateStrings.EmailArgumentCountry])
                            .Replace("{Gender}",emailArguments[EmailTemplateStrings.EmailArgumentGender]);
                    break;
                case EmailMessageType.MenteeAssgnment:
                    body = emailTemplate.TemplateHtml
                            .Replace("{AssignedFullName}", emailArguments[EmailTemplateStrings.EmailArgumentAssignedFullName])
                            .Replace("{Email}", emailArguments[EmailTemplateStrings.EmailTemplateArgumentEmail])
                            .Replace("{FullName}",emailArguments[EmailTemplateStrings.EmailArgumentFullName])
                            .Replace("{University}",emailArguments[EmailTemplateStrings.EmailArgumentUniversity])
                            .Replace("{Course}", emailArguments[EmailTemplateStrings.EmailArgumentCourse])
                            .Replace("{Gender}", emailArguments[EmailTemplateStrings.EmailArgumentGender]);
                    break;
                case EmailMessageType.Engagement:
                    body = emailTemplate.TemplateHtml
                            .Replace("{FullName}", emailArguments[EmailTemplateStrings.EmailArgumentFullName])
                            .Replace("{CreatedByUsername}", emailArguments[EmailTemplateStrings.EmailArgumentCreatedByUsername])
                            .Replace("{Name}", emailArguments[EmailTemplateStrings.EmailArgumentName])
                            .Replace("{Date}", emailArguments[EmailTemplateStrings.EmailArgumentDate])
                            .Replace("{StartTime}", emailArguments[EmailTemplateStrings.EmailArgumentStartTime])
                            .Replace("{Mode}",emailArguments[EmailTemplateStrings.EmailArgumentMode])
                            .Replace("{Address}", emailArguments[EmailTemplateStrings.EmailArgumentAddress]);

                    break;
                case EmailMessageType.EngagementAccepted:
                    body = emailTemplate.TemplateHtml
                            .Replace("{FullName}", emailArguments[EmailTemplateStrings.EmailArgumentFullName])
                            .Replace("{EngagementDateCreated}", emailArguments[EmailTemplateStrings.EmailArgumentEngagementDateCreated])
                            .Replace("{MentorFullName}",emailArguments[EmailTemplateStrings.EmailArgumentMentorFullName])
                            .Replace("{Name}", emailArguments[EmailTemplateStrings.EmailArgumentName])
                            .Replace("{Date}", emailArguments[EmailTemplateStrings.EmailArgumentDate])
                            .Replace("{StartTime}", emailArguments[EmailTemplateStrings.EmailArgumentStartTime])
                            .Replace("{Mode}",emailArguments[EmailTemplateStrings.EmailArgumentMode])
                            .Replace("{Address}", emailArguments[EmailTemplateStrings.EmailArgumentAddress]);
                    break;
                case EmailMessageType.EngagementTask:
                    body = emailTemplate.TemplateHtml
                            .Replace("{FullName}", emailArguments[EmailTemplateStrings.EmailArgumentFullName])
                            .Replace("{MentorFullName}",emailArguments[EmailTemplateStrings.EmailArgumentMentorFullName])
                            .Replace("{TaskEndDate}", emailArguments[EmailTemplateStrings.EmailArgumentTaskEndDate])
                            .Replace("{Name}",emailArguments[EmailTemplateStrings.EmailArgumentName])
                            .Replace("{Details}",emailArguments[EmailTemplateStrings.EmailArgumentDetails])
                            .Replace("{Category}", emailArguments[EmailTemplateStrings.EmailArgumentCategory])
                            .Replace("{Date}", emailArguments[EmailTemplateStrings.EmailArgumentDate]);
                    break;
                default:
                   throw new System.Exception("Message type not supported");
            }
            // create email queue object
            var emailQueue = new EmailQueue{
               Subject = emailArguments[EmailTemplateStrings.EmailSubject],
               ToAddress = emailArguments[EmailTemplateStrings.EmailToAddress],
               FromAddress = emailArguments[EmailTemplateStrings.EmailFromAddress],
               Body = body,
               Status = EmailSentStatus.Pending
            };
            return await Create(emailQueue);
        }

        public async Task<EmailTemplate> GetEmailTemplate(EmailMessageType messageType)
        {
            var emailTemplate = await Get<EmailTemplate>(c => c.MessageType == messageType).FirstOrDefaultAsync();
            return emailTemplate;
        }

        public async Task<IEnumerable<EmailQueue>> GetErrorEmailQueues()
        {
            // get email Queues were the status in Error and sentAttempts is less than 10
            return await Get<EmailQueue>(c => c.Status == EmailSentStatus.Error && c.SentAttempts <= 10).ToListAsync();
        }

        public async Task<IEnumerable<EmailQueue>> GetPendingEmailQueues()
        {
            return await Get<EmailQueue>(c => c.Status == EmailSentStatus.Pending).ToListAsync();
        }
    }
}
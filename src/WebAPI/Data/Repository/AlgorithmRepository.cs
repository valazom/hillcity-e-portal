using System.Threading.Tasks;
using Domain.Enums;
using Domain.Models;
using Microsoft.EntityFrameworkCore;
using WebAPI.Data.Databases;
using WebAPI.Data.Repository.Common;

namespace WebAPI.Data.Repository
{
    public class AlgorithmRepository : RepositoryBase, IAlgorithmRepository
    {
        public AlgorithmRepository(HepDataContext context) : base(context)
        {
            Context = context;
        }

        public HepDataContext Context { get; }

        public async Task<Algorithm> GetAlgorithmByType(AlgorithmType type)
        {
           return await Get<Algorithm>(c => c.Type == type).FirstOrDefaultAsync();
        }

        public async Task ToggleAlgorithmStatus(AlgorithmType algorithmType)
        {
            var algorithm = await GetAlgorithmByType(algorithmType);
            if(algorithm == null)
                return;
            algorithm.IsActive = !algorithm.IsActive;
            await Context.SaveChangesAsync();
        }
    }
}
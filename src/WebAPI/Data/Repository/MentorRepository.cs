using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Models;
using Microsoft.EntityFrameworkCore;
using WebAPI.Data.Databases;
using WebAPI.Data.Repository.Common;

namespace WebAPI.Data.Repository
{
    public class MentorRepository : RepositoryBase, IMentorRepository
    {
        public MentorRepository(HepDataContext context) : base(context)
        {
            
        }
        public async Task<Mentor> CreateMentor(Mentor mentor)
        {
            return  await Create(mentor);
        }

        public async Task<IEnumerable<Mentor>> GetAllMentors()
        {
            return  await All<Mentor>().ToListAsync();
        }

        public async Task<IEnumerable<Engagement>> GetEngagements(int userId)
        {
            var mentor = await Get<Mentor>(c => c.UserId == userId && c.User.IsDeleted == false).FirstOrDefaultAsync();
            if(mentor == null)
              return null;
            return mentor.Engagements;
        }

        public async Task<IEnumerable<Mentee>> GetMenteesAssigned(int userId)
        {
            var mentor = await Get<Mentor>(c => c.UserId == userId && c.User.IsDeleted == false).FirstOrDefaultAsync();
            if(mentor == null)
              return null;
            return mentor.Mentees;
        }

        public async Task<Mentor> GetMentorById(int Id)
        {
            return await Get<Mentor>(c => c.UserId == Id && c.User.IsDeleted == false).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Mentor>> GetMentorsWithoutMentees()
        {
            return await Get<Mentor>(c => c.Mentees == null).ToListAsync();
        }
    }
}
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Enums;
using Domain.Models;
using Microsoft.EntityFrameworkCore;
using WebAPI.Data.Databases;
using WebAPI.Data.Repository.Common;

namespace WebAPI.Data.Repository
{
    public class EngagementTaskRepository : RepositoryBase, IEngagementTaskRepository
    {
        public EngagementTaskRepository(HepDataContext context) : base(context)
        {
            
        }

        public async Task<bool> CompleteEngagementTask(EngagementTask engagementTask)
        {
            engagementTask.Status = EngagementTaskStatus.Completed;
            return await Update(engagementTask);
        }

        public async Task<EngagementTask> CreateEngagementTask(EngagementTask engagementTask)
        {
            return await Create(engagementTask);
        }

        public async Task<IEnumerable<EngagementTask>> GetAllEngagementTask()
        {
            return await All<EngagementTask>().ToListAsync();
        }

        public async Task<EngagementTask> GetEngagementTaskbyId(int Id)
        {
            return await Get<EngagementTask>(c => c.EngagementTaskId == Id).FirstOrDefaultAsync();
        }
    }
}
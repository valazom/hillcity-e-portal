using System.Threading.Tasks;
using Domain.Enums;
using Domain.Models;
using WebAPI.Data.Repository.Common;

namespace WebAPI.Data.Repository
{
    public interface IAlgorithmRepository : IRepository
    {
        Task<Algorithm> GetAlgorithmByType(AlgorithmType type);
        Task ToggleAlgorithmStatus(AlgorithmType algorithmType);
    }
}
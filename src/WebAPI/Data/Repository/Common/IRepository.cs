using System.Threading.Tasks;

namespace WebAPI.Data.Repository.Common
{
    public interface IRepository : IReadOnlyRepository
    {
         Task<T> Create<T> (T entity) where T : class;
         Task<bool> Delete<T>(T entity) where T : class;
         Task<bool> Update<T>(T entity) where T : class;
    }
}
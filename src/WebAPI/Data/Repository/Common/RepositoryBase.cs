using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace WebAPI.Data.Repository.Common
{
    public class RepositoryBase : ReadOnlyRepositorybase, IRepository
    {
        private readonly DbContext _context;
        public RepositoryBase(DbContext context) : base(context)
        {
            _context = context;
        }
        public async Task<T> Create<T>(T entity) where T : class
        {
            await _context.Set<T>().AddAsync(entity);
            await _context.SaveChangesAsync();
            return entity;
        }

        public async Task<bool> Delete<T>(T entity) where T : class
        {
            _context.Set<T>().Remove(entity);
            var rownum = await _context.SaveChangesAsync();
            return rownum > 0;
        }

        public async Task<bool> Update<T>(T entity) where T : class
        {
            var entry = _context.Entry(entity);
            _context.Set<T>().Attach(entity);
            entry.State = EntityState.Modified;
            var rownum = await _context.SaveChangesAsync();
            return rownum > 0;
        }
    }
}
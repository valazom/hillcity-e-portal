using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace WebAPI.Data.Repository.Common
{
    public class ReadOnlyRepositorybase : IReadOnlyRepository
    {
        private readonly DbContext _context;

        public ReadOnlyRepositorybase(DbContext context)
        {
            _context = context;
        }

        public IQueryable<T> All<T>() where T : class
        {
            return _context.Set<T>().AsQueryable();
        }

        public IQueryable<T> Get<T>(Expression<Func<T,bool>> predicate) where T : class
        {
            return _context.Set<T>().Where(predicate);
        }

        public T GetById<T>(int key) where T : class
        {
            return _context.Set<T>().Find(key);
        }

        public async Task<bool> GetExist<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            return await _context.Set<T>().AnyAsync(predicate);
        }
    }
}
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace WebAPI.Data.Repository.Common
{
    public interface IReadOnlyRepository
    {
         IQueryable<T> All<T>() where T : class;
         IQueryable<T> Get<T>(Expression<Func<T,bool>> predicate) where T: class;
         T GetById<T>(int key) where T: class;
         Task<bool> GetExist<T> (Expression<Func<T,bool>> predicate) where T: class;
    }
}
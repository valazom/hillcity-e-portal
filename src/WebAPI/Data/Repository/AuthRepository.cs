using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Domain.Models;
using Microsoft.EntityFrameworkCore;
using WebAPI.Data.Databases;
using WebAPI.Data.Repository.Common;

namespace WebAPI.Data.Repository
{
    public class AuthRepository : RepositoryBase, IAuthRepository
    {
        private readonly HepDataContext _context;

        public AuthRepository(HepDataContext context) : base(context)
        {
            _context = context;
        }
        public async Task<bool> EmailExist(string email)
        {
            // check if the email is already in the database
            var emailExist =  await GetExist<User>(c => c.Email == email);
            if(emailExist){
                return true;
            }
            return false;
        }

        public async Task<User> Login(string username, string password)
        {
            var user = await Get<User>(c => c.Username == username && c.IsDeleted == false).FirstOrDefaultAsync();
            if(user == null)
              return null;
            if(!VerifyPasswordHash(password,user.PasswordHash,user.PasswordSalt))
              return null;
            return user;
        }

        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using(var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt)){
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for(int i = 0;i<computedHash.Length;i++)
                   if(computedHash[i] != passwordHash[i]) return false;
                return true;
            }
        }
        
        public async Task<User> Register(User user, string password)
        {
            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(password,out passwordHash,out passwordSalt);
            user.PasswordHash = passwordHash; // set the password hash
            user.PasswordSalt = passwordSalt; // set the password salt
            await Create(user); // create the user in the database
            return user;
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            // compute the password hash and salt that will be stored in the database
            using(var hmac = new System.Security.Cryptography.HMACSHA512()){
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        public async Task<bool> UsernameExist(string username)
        {
            var usernameExist = await GetExist<User>(c => c.Username == username);
            if(usernameExist){
                return true;
            }
            return false;
        }

        public async Task<IEnumerable<Role>> GetAllRoles()
        {
            return await All<Role>().ToListAsync();
        }

        public async Task<RoleUser> AddUserToRole(User user, Role role)
        {
            var userRole = new RoleUser{
                UserId = user.UserId,
                RoleId = role.RoleId,
                User = user,
                Role = role
            };
            user.UserRoles.Add(userRole);
            await _context.SaveChangesAsync();
            return userRole;
        }

        public async Task<Role> GetRoleByName(string roleName)
        {
            return await Get<Role>(x => x.RoleName == roleName).FirstOrDefaultAsync();
        }

        public async Task<User> GetUserByUsername(string username)
        {
            var user = await Get<User>(c => c.Username == username).FirstOrDefaultAsync();
            if(user == null)
               return null;
            return user;
        }

        public async Task<bool> ChangePassword(User user,string password)
        {
            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(password,out passwordHash,out passwordSalt);
            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;
            return await Update(user);
        }

        public async Task<User> GetUserByEmail(string email)
        {
            var user = await Get<User>(x => x.Email == email).FirstOrDefaultAsync();
            if(user == null)
                return null;
            return user;
        }
    }
}
using Microsoft.AspNetCore.Http;

namespace WebAPI.Helpers
{
    public static class HttpResponseExtension
    {
        public static void AddApplicaionError(this HttpResponse response, string message){
            response.Headers.Add("Application-Error",message);
            response.Headers.Add("Access-Control-Expose-Headers","Application-Error");
            response.Headers.Add("Access-Control-Allow-Origin","*");
        }
    }
}
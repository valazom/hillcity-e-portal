SET IDENTITY_INSERT [dbo].[Algorithms] ON 

MERGE INTO [dbo].[Algorithms] AS TARGET
USING (VALUES 
    (1, 1, N'Algorthm to assign mentees', 1),
    (2, 2, N'Algorthm to calculate mentee score', 0)
) AS Source ([AlgorithmId],[Type],[Description],[IsActive])
ON Target.[AlgorithmId] = Source.[AlgorithmId]
WHEN MATCHED THEN
   UPDATE SET [Type] = Source.[Type],
              [Description] = Source.[Description],
              [IsActive]    = Source.[IsActive]
WHEN NOT MATCHED BY TARGET THEN
   INSERT ([AlgorithmId],[Type],[Description],[IsActive])
   VALUES ([AlgorithmId],[Type],[Description],[IsActive])
WHEN NOT MATCHED BY SOURCE THEN
 DELETE;
GO

SET IDENTITY_INSERT [dbo].[Algorithms] OFF 
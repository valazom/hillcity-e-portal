SET IDENTITY_INSERT [dbo].[EmailTemplates] ON
MERGE INTO [dbo].[EmailTemplates] AS TARGET
USING (VALUES
    (1, 1, N'User Registration', N'
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>User Registration</title>
</head>
<body>
<table align="center" cellpadding="20" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:20px; border:solid thin #CCC; color:#333;">
    <tr bgcolor="#2f4050" style="color:white; font-size:14px;">
        <td>
             User Registration  
        </td>
        <td align="right">
            HillCitye-Portal
        </td>
    </tr>
    <tr>
        <td colspan="2" style="border-bottom: solid thin #004795;">
            <strong style="font-size: 16px; color: #2f4050">Newly created User</strong>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <span style="font-size: 16px; color: #2f4050; padding-left: 10px;">Dear {Username}</span>
            <br />
            <div style="padding-left: 10px;">
                <p>You have successfully created an account on HillCitye-Portal with the following credentials:</p>
                <p>Username : {Username}</p>
                <p>Email:     {Email}</p>
                <p>Role:      {RoleName}</p>
                <p>Kindly let us know at {adminEmail} if you did not request this service.</p>
                <p>Regards</p>
                <p>HillCitye-Portal team.</p>
            </div>
                <tr>
                    <td>
                        
                    </td>
                </tr>
            </table>
            <br/>
            <br />
        </td>
    </tr>
    <tr bgcolor="#2f4050" style="color:white; font-size:12px;">
        <td>
            
        </td>
        <td align="right"></td>
    </tr>
</table>
</body>
</html>'),
    (2,2,N'Reset Password',N'
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>User Registration</title>
</head>
<body>
<table align="center" cellpadding="20" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:20px; border:solid thin #CCC; color:#333;">
    <tr bgcolor="#2f4050" style="color:white; font-size:14px;">
        <td>
             Reset Password Request
        </td>
        <td align="right">
            HillCitye-Portal
        </td>
    </tr>
    <tr>
        <td colspan="2" style="border-bottom: solid thin #004795;">
            <strong style="font-size: 16px; color: #2f4050">Reset Password</strong>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <span style="font-size: 16px; color: #2f4050; padding-left: 10px;">Dear {Username}</span>
            <br />
            <div style="padding-left: 10px;">
                <p>You have requested a password reset on the HillCitye-Portal. Kindly follow this link {link} to reset your password.</p>
                <p>If you did not request the password change kindly us know by sending an email to {adminEmail}.</p>
                <p>Regards</p>
                <p>HillCitye-Portal team.</p>
            </div>
                <tr>
                    <td>
                        
                    </td>
                </tr>
            </table>
            <br/>
            <br />
        </td>
    </tr>
    <tr bgcolor="#2f4050" style="color:white; font-size:12px;">
        <td>
            
        </td>
        <td align="right"></td>
    </tr>
</table>
</body>
</html>'),
(3,3,N'Mentor Assignment',N'
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Mentor Assignment</title>
</head>
<body>
<table align="center" cellpadding="20" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:20px; border:solid thin #CCC; color:#333;">
    <tr bgcolor="#2f4050" style="color:white; font-size:14px;">
        <td>
            Mentor Assignment 
        </td>
        <td align="right">
            HillCitye-Portal
        </td>
    </tr>
    <tr>
        <td colspan="2" style="border-bottom: solid thin #004795;">
            <strong style="font-size: 16px; color: #2f4050">Newly assigned mentor</strong>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <span style="font-size: 16px; color: #2f4050; padding-left: 10px;">Dear {FullName}</span>
            <br />
            <div style="padding-left: 10px;">
                <p>A mentor with the following credentials has been assigned to you:</p>
                <p>FullName :           {AssignedFullName}</p>
                <p>Email:               {Email}</p>
                <p>Occupation:          {Occupation}</p>
                <p>State:               {State}</p>
                <p>Country:             {Country}</p>
                <p>Gender               {Gender}</p>       
                <p>Regards</p>
                <p>HillCitye-Portal team.</p>
            </div>
                <tr>
                    <td>
                        
                    </td>
                </tr>
            </table>
            <br/>
            <br />
        </td>
    </tr>
    <tr bgcolor="#2f4050" style="color:white; font-size:12px;">
        <td>
            
        </td>
        <td align="right"></td>
    </tr>
</table>
</body>
</html>'),
(4,4,N'Mentee Assignment',N'
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Mentee Assignment</title>
</head>
<body>
<table align="center" cellpadding="20" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:20px; border:solid thin #CCC; color:#333;">
    <tr bgcolor="#2f4050" style="color:white; font-size:14px;">
        <td>
            Mentee Assignment 
        </td>
        <td align="right">
            HillCitye-Portal
        </td>
    </tr>
    <tr>
        <td colspan="2" style="border-bottom: solid thin #004795;">
            <strong style="font-size: 16px; color: #2f4050">Newly assigned mentee</strong>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <span style="font-size: 16px; color: #2f4050; padding-left: 10px;">Dear {FullName}</span>
            <br />
            <div style="padding-left: 10px;">
                <p>A mentee with the following credentials has been assigned to you:</p>
                <p>FullName :       {AssignedFullName}</p>
                <p>Email:           {Email}</p>
                <p>University:      {University}</p>
                <p>Course:          {Course}</p>
                <p>Gender           {Gender}</p>       
                <p>Regards</p>
                <p>HillCitye-Portal team.</p>
            </div>
                <tr>
                    <td>
                        
                    </td>
                </tr>
            </table>
            <br/>
            <br />
        </td>
    </tr>
    <tr bgcolor="#2f4050" style="color:white; font-size:12px;">
        <td>
            
        </td>
        <td align="right"></td>
    </tr>
</table>
</body>
</html>'),
(5,5,N'Engagement',N'
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Engagement</title>
</head>
<body>
<table align="center" cellpadding="20" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:20px; border:solid thin #CCC; color:#333;">
    <tr bgcolor="#2f4050" style="color:white; font-size:14px;">
        <td>
            Engagement
        </td>
        <td align="right">
            HillCitye-Portal
        </td>
    </tr>
    <tr>
        <td colspan="2" style="border-bottom: solid thin #004795;">
            <strong style="font-size: 16px; color: #2f4050">Newly created engagement</strong>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <span style="font-size: 16px; color: #2f4050; padding-left: 10px;">Dear {FullName}</span>
            <br />
            <div style="padding-left: 10px;">
                <p>An Engagement has been created by {CreatedByUsername} with following the credentials:</p>
                <p>Name :                    {Name}</p>
                <p>Scheduled date:           {Date}</p>
                <p>Time:                     {StartTime}</p>
                <p>CommunicationMode:        {Mode}</p>
                <p>Address:                  {Address}</p>   
                <p>Regards</p>
                <p>Kindly take note</p>
                <p>HillCitye-Portal team.</p>
            </div>
                <tr>
                    <td>
                        
                    </td>
                </tr>
            </table>
            <br/>
            <br />
        </td>
    </tr>
    <tr bgcolor="#2f4050" style="color:white; font-size:12px;">
        <td>
            
        </td>
        <td align="right"></td>
    </tr>
</table>
</body>
</html>'),
(6,6,N'Engagement Accepted',N'
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Engagement</title>
</head>
<body>
<table align="center" cellpadding="20" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:20px; border:solid thin #CCC; color:#333;">
    <tr bgcolor="#2f4050" style="color:white; font-size:14px;">
        <td>
            Engagement
        </td>
        <td align="right">
            HillCitye-Portal
        </td>
    </tr>
    <tr>
        <td colspan="2" style="border-bottom: solid thin #004795;">
            <strong style="font-size: 16px; color: #2f4050">Engagement Accepted</strong>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <span style="font-size: 16px; color: #2f4050; padding-left: 10px;">Dear {FullName}</span>
            <br />
            <div style="padding-left: 10px;">
                <p>The Engagement you created on {EngagementDateCreated} has been accepted by {MentorFullName}. Kindly find details below:</p>
                <p>Name :                    {Name}</p>
                <p>Scheduled date:           {Date}</p>
                <p>Time                      {StartTime}</p>
                <p>CommunicationMode:        {Mode}</p>
                <p>Address:                  {Address}</p>   
                <p>Regards</p>
                <p>Kindly take note</p>
                <p>HillCitye-Portal team.</p>
            </div>
                <tr>
                    <td>
                        
                    </td>
                </tr>
            </table>
            <br/>
            <br />
        </td>
    </tr>
    <tr bgcolor="#2f4050" style="color:white; font-size:12px;">
        <td>
            
        </td>
        <td align="right"></td>
    </tr>
</table>
</body>
</html>'),
(7,7,N'Engagement Task',N'
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Engagement Task</title>
</head>
<body>
<table align="center" cellpadding="20" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:20px; border:solid thin #CCC; color:#333;">
    <tr bgcolor="#2f4050" style="color:white; font-size:14px;">
        <td>
            Engagement Task
        </td>
        <td align="right">
            HillCitye-Portal
        </td>
    </tr>
    <tr>
        <td colspan="2" style="border-bottom: solid thin #004795;">
            <strong style="font-size: 16px; color: #2f4050">New Engagement Task</strong>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <span style="font-size: 16px; color: #2f4050; padding-left: 10px;">Dear {FullName}</span>
            <br />
            <div style="padding-left: 10px;">
                <p>Your mentor: {MentorFullName} has created an Engagement Task with deadline completion date as {TaskEndDate}. Kindly find details below:</p>
                <p>Engagement ID :                    {Name}</p>
                <p>Details:                           {Details}</p>
                <p>Category:                          {Category}</p>
                <p>Date Created:                      {Date}</p>   
                <p>Regards</p>
                <p>Kindly take note</p>
                <p>HillCitye-Portal team.</p>
            </div>
                <tr>
                    <td>
                        
                    </td>
                </tr>
            </table>
            <br/>
            <br />
        </td>
    </tr>
    <tr bgcolor="#2f4050" style="color:white; font-size:12px;">
        <td>
            
        </td>
        <td align="right"></td>
    </tr>
</table>
</body>
</html>
')
) AS SOURCE ([TemplateId],
             [MessageType],
             [Title],
             [TemplateHtml])
ON TARGET.[TemplateId] = SOURCE.[TemplateId]
WHEN MATCHED THEN 
    UPDATE SET [MessageType] = Source.[MessageType],
               [Title] = Source.[Title],
               [TemplateHtml] = Source.[TemplateHtml]

WHEN NOT MATCHED BY TARGET THEN
    INSERT ([TemplateId],
            [MessageType],
            [Title],
            [TemplateHtml])
    VALUES ([TemplateId],
            [MessageType],
            [Title],
            [TemplateHtml])

WHEN NOT MATCHED BY SOURCE THEN
DELETE;
GO  
          
SET IDENTITY_INSERT [dbo].[EmailTemplates] OFF 
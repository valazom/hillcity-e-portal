SET IDENTITY_INSERT [dbo].[Roles] ON 

MERGE INTO [dbo].[Roles] AS TARGET
USING (VALUES 
    (1, N'Administrator'),
    (2,N'Mentee'),
    (3,N'Mentor')
) AS Source ([RoleId],[RoleName])
ON Target.[RoleId] = Source.[RoleId]
WHEN MATCHED THEN
   UPDATE SET [RoleName] = Source.[RoleName]
WHEN NOT MATCHED BY TARGET THEN
   INSERT ([RoleId],[RoleName])
   VALUES ([RoleId],[RoleName])
WHEN NOT MATCHED BY SOURCE THEN
 DELETE;
GO

SET IDENTITY_INSERT [dbo].[Roles] OFF 
using System;
using AutoMapper;
using Domain.Models;
using WebAPI.Dtos.User;
using WebAPI.Dtos.Role;
using WebAPI.Dtos;
using WebAPI.Dtos.Mentee;
using WebAPI.Dtos.Mentor;
using WebAPI.Dtos.Engagement;
using WebAPI.Dtos.EngagementTask;

namespace WebAPI.AutoMap
{
    public class MapConfiguration : Profile
    {
        public MapConfiguration()
        {
            MapUsers();
            MapMentees();
            MapMentor();
            MapEngagements();
            MapEngagementTasks();
        }

        private void MapEngagementTasks()
        {
            CreateMap<CreateEngagementTaskDto,EngagementTask>();
            CreateMap<EngagementTask,CreateEngagementTaskDto>();
            CreateMap<ReturnEngagementTaskDto,EngagementTask>();
            CreateMap<EngagementTask,ReturnEngagementTaskDto>();
            CreateMap<UpdateEngagementTaskDto,EngagementTask>()
              .ForMember(x => x.EngagementId, s => s.Ignore());
            CreateMap<EngagementTask,UpdateEngagementTaskDto>();
            CreateMap<PatchEngagementTaskDto,EngagementTask>();
            CreateMap<EngagementTask,PatchEngagementTaskDto>();
        }

        private void MapEngagements()
        {
            CreateMap<CreateEngagementDto,Engagement>()
              .ForMember(x => x.Duration, s => s.MapFrom(k => TimeSpan.Parse(k.Duration)))
              .ForMember(x => x.StartTime, s => s.MapFrom(k => TimeSpan.Parse(k.StartTime)));
            CreateMap<Engagement,EngagementDto>();
            CreateMap<Engagement,CreateEngagementDto>()
              .ForMember(x => x.MentorId, s => s.MapFrom(k => k.Mentor.UserId))
              .ForMember(x => x.MenteeId, s => s.MapFrom(k => k.Mentee.UserId))
              .ForMember(x => x.Duration,s => s.MapFrom(k => k.Duration.ToString()))
              .ForMember(x => x.StartTime, s => s.MapFrom(k => k.StartTime.ToString()));
            CreateMap<ReturnEngagementDto,Engagement>()
               .ForMember(x => x.Duration, s => s.MapFrom(k => TimeSpan.Parse(k.Duration)))
               .ForMember(x => x.StartTime, s => s.MapFrom(k => TimeSpan.Parse(k.StartTime)));
            CreateMap<Engagement,ReturnEngagementDto>()
               .ForMember(x => x.Duration,s => s.MapFrom(k => k.Duration.ToString()))
               .ForMember(x => x.StartTime, s => s.MapFrom(k => k.StartTime.ToString()));
            CreateMap<UpdateEngagementDto, Engagement>()
               .ForMember(x => x.Duration, s => s.MapFrom(k => TimeSpan.Parse(k.Duration)))
               .ForMember(x => x.StartTime, s => s.MapFrom(k => TimeSpan.Parse(k.StartTime)));
            CreateMap<Engagement, UpdateEngagementDto>()
               .ForMember(x => x.MentorId, s => s.MapFrom(k => k.Mentor.UserId))
               .ForMember(x => x.MenteeId, s => s.MapFrom(k => k.Mentee.UserId))
               .ForMember(x => x.Duration,s => s.MapFrom(k => k.Duration.ToString()))
               .ForMember(x => x.StartTime, s => s.MapFrom(k => k.StartTime.ToString()));
            CreateMap<PatchEngagementDto, Engagement>()
               .ForMember(x => x.Duration, s => s.MapFrom(k => TimeSpan.Parse(k.Duration)))
               .ForMember(x => x.StartTime, s => s.MapFrom(k => TimeSpan.Parse(k.StartTime)));
            CreateMap<Engagement,PatchEngagementDto>()
               .ForMember(x => x.MentorId, s => s.MapFrom(k => k.Mentor.UserId))
               .ForMember(x => x.MenteeId, s => s.MapFrom(k => k.Mentee.UserId))
               .ForMember(x => x.Duration,s => s.MapFrom(k => k.Duration.ToString()))
               .ForMember(x => x.StartTime, s => s.MapFrom(k => k.StartTime.ToString()));
                
        }

        private void MapMentor()
        {
            CreateMap<CreateMentorDto, Mentor>();
            CreateMap<Mentor, CreateMentorDto>();
            CreateMap<ReturnMentorDto, Mentor>();
            CreateMap<Mentor, ReturnMentorDto>();
            CreateMap<UpdateMentorDto, Mentor>();
            CreateMap<Mentor, UpdateMentorDto>();
            CreateMap<Mentor, AssignedMentorDto>();
            CreateMap<PatchMentorDto,Mentor>()
                .ForMember(c => c.UserId,s => s.Ignore());
            CreateMap<Mentor,PatchMentorDto>();
        }

        private void MapMentees()
        {
           CreateMap<CreateMenteeDto,Mentee>();
           CreateMap<Mentee,CreateMenteeDto>();
           CreateMap<ReturnMenteeDto,Mentee>();
           CreateMap<Mentee,ReturnMenteeDto>();
           CreateMap<UpdateMenteeDto, Mentee>();
           CreateMap<Mentee,UpdateMenteeDto>();
           CreateMap<Mentee, AssignedMenteeDto>();
           CreateMap<PatchMenteeDto,Mentee>()
               .ForMember(c => c.UserId, s => s.Ignore());
           CreateMap<Mentee, PatchMenteeDto>();
        }

        private void MapUsers()
        {
            CreateMap<UserRegisterDto,User>()
               .ForMember(x => x.UserId,s => s.Ignore())
               .ForMember(x => x.LocationId, s => s.Ignore());
            CreateMap<User,UserRegisterDto>();
            CreateMap<UserDto,User>();
            CreateMap<User,UserDto>();
            CreateMap<UserUpdateDto,User>()
                .ForMember(x => x.UserId,s => s.Ignore());
            CreateMap<User, UserUpdateDto>();
            CreateMap<UserPhotoDto, User>();
           
            CreateMap<RoleDto,Role>()
                .ForMember(x => x.RoleId,s => s.Ignore());
            CreateMap<Role,RoleDto>();
            CreateMap<RoleUserDto,RoleUser>()
                .ForMember(x => x.RoleId,s => s.Ignore())
                .ForMember(x => x.UserId,s => s.Ignore());
            CreateMap<RoleUser,RoleUserDto>();
            CreateMap<LocationDto,Location>()
               .ForMember(x => x.LocationId, s => s.Ignore());
        }
    }
}
using System.ComponentModel.DataAnnotations;

namespace WebAPI.Dtos
{
    public class LocationDto
    {
        public string Address { get; set; }
    }
}
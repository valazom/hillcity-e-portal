using System.ComponentModel.DataAnnotations;
using Common.Resoures;
using Domain.Enums;

namespace WebAPI.Dtos.User
{
    public class UserRegisterDto 
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{8,15}$",
          ErrorMessage = MessageStrings.MessageFailurePasswordValidation)]
        public string Password { get; set; }
        [Required]
        public string Phonenumber { get; set; }
        [Required]
        public Gender Gender { get; set; }
        [Required]
        public string RoleName { get; set; }
        public LocationDto Location { get; set; }

    }
}
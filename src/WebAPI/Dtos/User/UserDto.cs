using System.Collections.Generic;
using Domain.Enums;
using WebAPI.Dtos.Role;

namespace WebAPI.Dtos.User
{
    public class UserDto : UserBaseDto
    {
        public int UserId { get; set; }       
    }
}
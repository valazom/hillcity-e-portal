using Domain.Enums;

namespace WebAPI.Dtos.User
{
    public class UserBaseDto
    {
        public string FirstName { get; set; } 
        public string  LastName { get; set; }
        public string Username { get; set; }
        public Gender Gender { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Photo { get; set; }
        public string PublicId { get; set; }
        public LocationDto Location { get; set; }
    }
}
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using WebAPI.CustomValidation;

namespace WebAPI.Dtos.User
{
    public class UserPhotoDto 
    {
        [Required]
        [FileUploadValidationAttributes.ImageExtensions("jpg|jpeg|png", ErrorMessage = "Invalid File Format")]
        public IFormFile File {get; set;}
    }
}
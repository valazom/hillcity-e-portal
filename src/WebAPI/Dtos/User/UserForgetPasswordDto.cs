using System.ComponentModel.DataAnnotations;

namespace WebAPI.Dtos.User
{
    public class UserForgetPasswordDto
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string DomainName { get; set; }
    }
}
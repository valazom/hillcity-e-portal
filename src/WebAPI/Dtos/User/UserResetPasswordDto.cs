using System.ComponentModel.DataAnnotations;

namespace WebAPI.Dtos.User
{
    public class UserResetPasswordDto
    {
        [Required]
        public string UsernameHash { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
namespace WebAPI.Dtos.Mentor
{
    public class MentorViewDto
    {
        public int NumberOfPendingEngagements { get; set; }
        public int NumberOfCompletedEngagements { get; set; }
        public AssignedMentorDto AssignedMentor { get; set; }
    }
}
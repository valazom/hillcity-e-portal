using System.Collections.Generic;
using WebAPI.Dtos.Engagement;

namespace WebAPI.Dtos.Mentor
{
    public class ReturnMentorDto : MentorBaseDto
    {
        public int userId { get; set; }
        
    }
}
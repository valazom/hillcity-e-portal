namespace WebAPI.Dtos.Mentor
{
    public class CreateMentorDto : MentorBaseDto
    {
        public int userId { get; set; }
    }
}
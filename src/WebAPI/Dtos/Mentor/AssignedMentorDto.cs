using System.Collections.Generic;
using WebAPI.Dtos.Engagement;
using WebAPI.Dtos.User;

namespace WebAPI.Dtos.Mentor
{
    public class AssignedMentorDto : MentorBaseDto
    {
        public UserDto User { get; set; }
        
    }
}
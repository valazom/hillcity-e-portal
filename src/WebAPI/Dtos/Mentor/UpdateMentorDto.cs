using System.ComponentModel.DataAnnotations;

namespace WebAPI.Dtos.Mentor
{
    public class UpdateMentorDto : MentorBaseDto
    {
        [Required]
        public int userId { get; set; }
    }
}
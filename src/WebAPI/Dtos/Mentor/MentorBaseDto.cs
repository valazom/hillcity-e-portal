namespace WebAPI.Dtos.Mentor
{
    public class MentorBaseDto
    {
        public string Occupation { get; set; }
        public string State { get; set; }
        public string  Country { get; set; }
    }
}
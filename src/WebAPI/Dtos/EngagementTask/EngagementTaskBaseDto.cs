using System;
using System.ComponentModel.DataAnnotations;
using Domain.Enums;

namespace WebAPI.Dtos.EngagementTask
{
    public class EngagementTaskBaseDto
    {
        [Required]
        public TaskCategory TaskCategory { get; set; }
        [Required]
        public string Details { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public EngagementTaskStatus Status { get; set; }
    }
}
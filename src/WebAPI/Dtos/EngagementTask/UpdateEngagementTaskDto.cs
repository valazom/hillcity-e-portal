using System.ComponentModel.DataAnnotations;

namespace WebAPI.Dtos.EngagementTask
{
    public class UpdateEngagementTaskDto : EngagementTaskBaseDto
    {
        [Required]
        public int EngagementTaskId { get; set; }
    }
}
using System.ComponentModel.DataAnnotations;

namespace WebAPI.Dtos.EngagementTask
{
    public class CreateEngagementTaskDto : EngagementTaskBaseDto
    {
        [Required]
        public int EngagementId { get; set; }
    }
}
using System;
using System.ComponentModel.DataAnnotations;
using Domain.Enums;
using Newtonsoft.Json;
using WebAPI.Dtos.Engagement;

namespace WebAPI.Dtos.EngagementTask
{
    public class ReturnEngagementTaskDto : EngagementTaskBaseDto
    {
        public int EngagementTaskId { get; set; }
        public ReturnEngagementDto Engagement { get; set; }
    }
}
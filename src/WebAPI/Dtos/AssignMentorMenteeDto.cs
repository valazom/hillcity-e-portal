using System.ComponentModel.DataAnnotations;

namespace WebAPI.Dtos
{
    public class AssignMentorMenteeDto
    {
        [Required]
        public int MentorId { get; set; }
        [Required]
        public int MenteeId { get; set; }
    }
}
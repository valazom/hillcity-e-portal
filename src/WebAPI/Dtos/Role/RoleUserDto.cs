using WebAPI.Dtos.User;

namespace WebAPI.Dtos.Role
{
    public class RoleUserDto
    {
        public UserDto User { get; set; }
        public RoleDto Role { get; set; }
    }
}
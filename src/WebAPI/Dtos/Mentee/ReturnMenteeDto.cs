using System;
using System.ComponentModel.DataAnnotations;
using WebAPI.Dtos.User;

namespace WebAPI.Dtos.Mentee
{
    public class ReturnMenteeDto : MenteeBaseDto
    {
        public int UserId { get; set; }
    }
}
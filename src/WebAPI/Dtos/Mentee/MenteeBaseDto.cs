using System;
using System.ComponentModel.DataAnnotations;

namespace WebAPI.Dtos.Mentee
{
    public class MenteeBaseDto
    {
        [Required]
        public string University { get; set; }
        [Required]
        public string Course {get; set;}
        [Required]
        public DateTime DateOfAdmission { get; set; }
        [Required]
        public DateTime DateOfGraduation { get; set; }
    }
}
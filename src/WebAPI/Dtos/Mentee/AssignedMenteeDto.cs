using WebAPI.Dtos.User;

namespace WebAPI.Dtos.Mentee
{
    public class AssignedMenteeDto : MenteeBaseDto
    {
        public UserDto User { get; set; }
    }
}
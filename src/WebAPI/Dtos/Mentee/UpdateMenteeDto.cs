using System.ComponentModel.DataAnnotations;

namespace WebAPI.Dtos.Mentee
{
    public class UpdateMenteeDto : MenteeBaseDto
    {
        [Required]
        public int userId { get; set; }
    }
}
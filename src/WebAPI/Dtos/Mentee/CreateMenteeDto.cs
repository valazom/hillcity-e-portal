namespace WebAPI.Dtos.Mentee
{
    public class CreateMenteeDto : MenteeBaseDto
    {
        public int UserId { get; set; }
    }
}
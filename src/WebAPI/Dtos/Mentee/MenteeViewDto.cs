using System.Collections.Generic;

namespace WebAPI.Dtos.Mentee
{
    public class MenteeViewDto
    {
        public int NumberOfPendingEnagagaments { get; set; }
        public int NumberOfCompletedEngagements { get; set; }

        public IEnumerable<AssignedMenteeDto> AssignedMentees { get; set; }
    }
}
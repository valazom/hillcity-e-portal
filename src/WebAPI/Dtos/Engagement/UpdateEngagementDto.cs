using System.ComponentModel.DataAnnotations;

namespace WebAPI.Dtos.Engagement
{
    public class UpdateEngagementDto : EngagementBaseDto
    {
        [Required]
        public int EngagementId { get; set; }
        [Required]
        public int MentorId { get; set; }
        [Required]
        public int MenteeId { get; set; }
    }
}
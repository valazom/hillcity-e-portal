namespace WebAPI.Dtos.Engagement
{
    public class EngagementDto : EngagementBaseDto
    {
        public int EngagementId { get; set; }
    }
}
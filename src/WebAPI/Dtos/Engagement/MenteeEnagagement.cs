using System.Collections.Generic;

namespace WebAPI.Dtos.Engagement
{
    public class MenteeEnagagement
    {
        public int NumberOfPendingEngagements { get; set; }
        public int NumberOfCompletedEngagements { get; set; }
        public IEnumerable<ReturnEngagementDto> Engagements { get; set; }
    }
}
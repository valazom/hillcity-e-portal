using System.Collections.Generic;

namespace WebAPI.Dtos.Engagement
{
    public class MentorEngagement
    {
        public int NumberOfPendingEngagements { get; set; }
        public int NumberOfCompletedEngagements { get; set; }
        public int NumberOfAssignedMentees { get; set; }
        public IEnumerable<ReturnEngagementDto> Engagements { get; set; }
    }
}
using System;
using System.ComponentModel.DataAnnotations;
using Domain.Enums;

namespace WebAPI.Dtos.Engagement
{
    public class CreateEngagementDto : EngagementBaseDto
    {
        
        [Required]
        public int MentorId { get; set; }
        [Required]
        public int MenteeId { get; set; }
        
    }
}
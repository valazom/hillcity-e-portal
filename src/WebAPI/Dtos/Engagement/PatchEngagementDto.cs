using System.ComponentModel.DataAnnotations;

namespace WebAPI.Dtos.Engagement
{
    public class PatchEngagementDto : EngagementBaseDto
    {
        public int? MentorId { get; set; }
        public int? MenteeId { get; set; }
    }
}
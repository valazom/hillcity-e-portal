using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using WebAPI.CustomValidation;

namespace WebAPI.Dtos.Engagement
{
    public class EngagementUploadDto
    {
        [Required]
        [EngagmentReportFile("pdf", ErrorMessage ="Invalid file format")]
        public IFormFile File { get; set; }
    }
}
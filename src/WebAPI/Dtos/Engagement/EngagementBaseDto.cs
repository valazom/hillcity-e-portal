using System;
using System.ComponentModel.DataAnnotations;
using Domain.Enums;
using WebAPI.CustomValidation;

namespace WebAPI.Dtos.Engagement
{
    public class EngagementBaseDto
    {
        [Required]
        public string Subject { get; set; }
        [Required]
        public string Duration {get; set;}
        [Required]
        public string StartTime { get; set; }
        [Required]
        public DateTime DateCreatedUtc { get; set; }
        [EngagementScheduleDate]
        public DateTime ScheduledDateUtc { get; set; }
        public string EngagementReport { get; set; }
        public bool HasEngagementTask { get; set; }
        [Required]
        public CommunicationMode CommunicationMode { get; set; }
        public EngagementStatus Status { get; set; }
        [EngagementLocation]
        public LocationDto Location {get; set;}
    }
}
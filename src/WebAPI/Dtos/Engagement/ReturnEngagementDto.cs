using WebAPI.Dtos.Mentee;
using WebAPI.Dtos.Mentor;
using WebAPI.Dtos.User;

namespace WebAPI.Dtos.Engagement
{
    public class ReturnEngagementDto : EngagementBaseDto
    {
        public int EngagementId { get; set; }
        public AssignedMenteeDto Mentee { get; set; }
        public AssignedMentorDto Mentor {get; set;}

    }
}
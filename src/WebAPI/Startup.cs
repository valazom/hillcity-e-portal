﻿using AutoMapper;
using Common.Email.Configuration;
using Common.Email.Service;
using Hangfire;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using NJsonSchema;
using NSwag.AspNetCore;
using System.Net;
using System.Text;
using WebAPI.Common;
using WebAPI.Data.Databases;
using WebAPI.Data.Repository;
using WebAPI.Filters;
using WebAPI.Helpers;
using WebAPI.Worker;
using WebAPI.Worker.Jobs;

namespace WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment environment)
        {
            Configuration = configuration;
            CurrentEnvironment = environment;
        }

        public IConfiguration Configuration { get; }
        public IHostingEnvironment CurrentEnvironment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.BuildServiceProvider().GetService<HepDataContext>().Database.Migrate();
            // detects environment and use connectionstring accordingly
            if (CurrentEnvironment.IsProduction())
                services
                  .AddDbContext<HepDataContext>(options => options
                    .UseLazyLoadingProxies()
                    .UseSqlServer("Server=tcp:hepappdb.database.windows.net,1433;Initial Catalog=HepAppDb;Persist Security Info=False;User ID=hepappadmin;Password=Az0m-12345@#;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"));
            else
                services.AddDbContext<HepDataContext>(option => option
                   .UseLazyLoadingProxies()
                   .UseSqlServer("Server= VALENTINEAZ-PC\\SQLEXPRESS02;Initial Catalog=HillCityApp; Integrated security=true; Trusted_Connection=True; MultipleActiveResultSets=true;"));
            services.AddAutoMapper();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddScoped<IAuthRepository, AuthRepository>();
            services.AddScoped<IEmailQueueRepository, EmailQueueRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IMenteeRepository, MenteeRepository>();
            services.AddScoped<IMentorRepository,MentorRepository>();
            services.AddScoped<IEngagementRepository,EngagementRepository>();
            services.AddScoped<IEngagementTaskRepository,EngagementTaskRepository>();
            services.AddScoped<IAlgorithmRepository, AlgorithmRepository>();
            services.AddScoped(typeof(ILogger<>), typeof(Logger<>));
            services.AddSingleton<IEmailConfiguration>(Configuration
                       .GetSection("EmailConfiguration").Get<EmailConfiguration>());
            services.AddTransient<IEmailService, EmailService>();
            services.AddTransient<EmailArgumentsBuilder>();
            services.AddTransient<CommonEmailArgument>();
            services.AddScoped<ISyncJobs, SyncJobs>();
            services.Configure<CloudinarySettings>(Configuration.GetSection("CloudinarySettings"));
            services.AddCors();
            services.AddSwagger();
            if (CurrentEnvironment.IsProduction())
                services.AddHangfire(config => config.UseSqlServerStorage("Server=tcp:hepappdb.database.windows.net,1433;Initial Catalog=HepAppDb;Persist Security Info=False;User ID=hepappadmin;Password=Az0m-12345@#;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"));
            else
                services.AddHangfire(config => config.UseSqlServerStorage("Server= VALENTINEAZ-PC\\SQLEXPRESS02;Initial Catalog=HillCityApp; Integrated security=true; Trusted_Connection=True; MultipleActiveResultSets=true;"));
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(Options =>
            {
                Options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8
                         .GetBytes(Configuration.GetSection("AppSettings:Token").Value)),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                //app.UseHsts();
                app.UseExceptionHandler(builder => {
                    builder.Run(async context => {
                        context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        var error = context.Features.Get<IExceptionHandlerFeature>();
                        if(error != null){
                            context.Response.AddApplicaionError(error.Error.Message);
                            await context.Response.WriteAsync(error.Error.Message);
                        }
                    });
                });
            }

            //app.UseHttpsRedirection();
            if(CurrentEnvironment.IsProduction())
                app.UseHangfireDashboard("/hangfire",new DashboardOptions(){
                    Authorization = new [] {new HangFireAuthorizationFilter()}
                });
            else
                app.UseHangfireDashboard();
            app.UseHangfireServer();
            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseAuthentication();
            app.UseSwaggerUi3WithApiExplorer(settings =>
            {
                settings.GeneratorSettings.DefaultPropertyNameHandling = PropertyNameHandling.CamelCase;
                settings.PostProcess = document =>
                {
                    document.Info.Version = "v1";
                    document.Info.Title = "HillCitye-Portal API";
                    document.Info.Description = "An Asp.net Core web api for HillCitye-Portal";
                    document.Info.TermsOfService = "None";
                    document.Info.Contact = new NSwag.SwaggerContact
                    {
                        Name = "Valentine Azom",
                        Email = "valazom@gmail.com",
                        Url = "https://www.linkedin.com/in/valentine-azom-6a435558/"
                    };
                    document.Info.License = new NSwag.SwaggerLicense
                    {
                        Name = "Use under LICX",
                        Url = "https://example.com/license"
                    };
                };
            });
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseMvc(routes => {
                routes.MapSpaFallbackRoute(
                    name: "Fallback",
                    defaults: new { controller = "Fallback", action= "Index"}
                );
            });
            JobManager.CreateRecurringJobs();
        }
    }
}

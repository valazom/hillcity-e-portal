﻿using System;

namespace WebAPI
{
    public interface ILogger<T>
    {
        void LogError(Exception ex, string message);
        void LogError(string message);
        void LogInfo(string message);
    }
}
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Http;

namespace WebAPI.CustomValidation
{
    public class FileUploadValidationAttributes : ValidationAttribute
    {
        [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
        public class ImageExtensionsAttribute : ValidationAttribute
        {
            private List<string> AllowedExtensions { get; set; }

            public ImageExtensionsAttribute(string fileExtensions)
            {
                AllowedExtensions = fileExtensions.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            }

            public override bool IsValid(object value)
            {
                var file = value as IFormFile;

                if (file != null)
                {
                    var fileName = file.FileName;

                    var afileType = AllowedExtensions.Any(y => fileName.EndsWith(y));
                    if (file.Length <= 1024000 && afileType) //image should not be greater than 5MB
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

                return false;
            }
        }

    }
}
using System.ComponentModel.DataAnnotations;
using Common.Resoures;
using WebAPI.Dtos.Engagement;

namespace WebAPI.CustomValidation
{
    public class EngagementScheduleDateAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var model = (CreateEngagementDto)validationContext.ObjectInstance;
            if(model.ScheduledDateUtc >= model.DateCreatedUtc)
                return ValidationResult.Success;
            ErrorMessage = MessageStrings.MessageFailureEngagementScheduleDate;
            return new ValidationResult(ErrorMessage);
        }
    }
}
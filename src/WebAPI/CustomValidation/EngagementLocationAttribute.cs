using System.ComponentModel.DataAnnotations;
using Common.Resoures;
using Domain.Enums;
using WebAPI.Dtos.Engagement;

namespace WebAPI.CustomValidation
{
    public class EngagementLocationAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var model = (CreateEngagementDto)validationContext.ObjectInstance;
            if(model.CommunicationMode == CommunicationMode.FaceToFace)
            {
                if(model.Location == null){
                    ErrorMessage = MessageStrings.MessageFailureEngagementLocation;
                    return new ValidationResult(ErrorMessage);
                }  
            }
            return ValidationResult.Success;
        } 
    }
}
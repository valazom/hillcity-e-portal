using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Http;

namespace WebAPI.CustomValidation
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class EngagmentReportFileAttribute : ValidationAttribute
    {
         private List<string> AllowedExtensions { get; set; }
         public EngagmentReportFileAttribute(string fileExtensions)
         {
             AllowedExtensions = fileExtensions.Split(new char[] {'|'},StringSplitOptions.RemoveEmptyEntries).ToList();
         }

         public override bool IsValid(object value)
         {
             var file = value as IFormFile;

             if(file != null)
             {
                 var filename = file.FileName;
                 var afileTYpe = AllowedExtensions.Any(y => filename.EndsWith(y));
                 if(file.Length <= 5120000 && afileTYpe)
                 {
                     return true;
                 }
                 else
                 {
                     return false;
                 }
             }
             return false;
         }
    }
}
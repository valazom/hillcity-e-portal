using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Email.MessageInfo;
using Common.Email.Service;
using Domain.Enums;
using Domain.Models;
using Troschuetz.Random;
using WebAPI.Common;
using WebAPI.Data.Repository;

namespace WebAPI.Worker.Jobs
{
    public class SyncJobs : ISyncJobs
    {
        private readonly IEmailQueueRepository _emailQueueRepository;
        private readonly IEmailService _emailService;
        private readonly ILogger<SyncJobs> _logger;
        public IAlgorithmRepository AlgorithmRepository;
        public IMenteeRepository _menteeRepository;
        public IMentorRepository MentorRepository;

        public Microsoft.Extensions.Configuration.IConfiguration config;
        private readonly CommonEmailArgument emailArgument;

        public SyncJobs(IEmailQueueRepository emailQueueRepository, IEmailService emailService,
         IAlgorithmRepository algorithmRepository, IMenteeRepository menteeRepository,
         IMentorRepository mentorRepository,
         Microsoft.Extensions.Configuration.IConfiguration config,
         CommonEmailArgument emailArgument,
         ILogger<SyncJobs> logger)
        {
            this.MentorRepository = mentorRepository;
            this.config = config;
            this.emailArgument = emailArgument;
            this._menteeRepository = menteeRepository;
            this.AlgorithmRepository = algorithmRepository;
            this._emailQueueRepository = emailQueueRepository;
            this._emailService = emailService;
            this._logger = logger;
        }
        public async Task SendErrorEmails()
        {
            try
            {
                var errorEmailQueues = await _emailQueueRepository.GetErrorEmailQueues();
                foreach (var errorEmailQueue in errorEmailQueues)
                {
                    //build emailMessage from errorEmails
                    var emailMessage = BuildEmailMessage(errorEmailQueue);
                    await SendMail(emailMessage, errorEmailQueue);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }
        }

        public async Task SendPendingEmails()
        {
            try
            {
                //get all emailQueues with pending status in the database
                var pendingEmailQueues = await _emailQueueRepository.GetPendingEmailQueues();
                foreach (var emailQueue in pendingEmailQueues)
                {
                    //build an emailMessage from the emailQueue
                    var emailMessage = BuildEmailMessage(emailQueue);
                    await SendMail(emailMessage, emailQueue);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }
        }
        public async Task SendMail(EmailMessage message, EmailQueue emailQueue)
        {
            var emailMessageResult = await _emailService.SendAsync(message);
            //increase email attempt
            emailQueue.SentAttempts++;
            //if sending the email succeeded change the status of the EmailQueue to Sent
            // if an error occured while send update the status to Error
            // and update in the database
            if (emailMessageResult.Succeeded)
            {
                emailQueue.Status = EmailSentStatus.Sent;
            }
            else
            {
                emailQueue.Status = EmailSentStatus.Error;
                _logger.LogError(emailMessageResult.Exception, emailMessageResult.ErrorMessage);
            }
            await _emailQueueRepository.Update(emailQueue);
        }
        public EmailMessage BuildEmailMessage(EmailQueue emailQueue)
        {
            var emailMessageBuilder = new EmailMessageBuilder();
            return emailMessageBuilder.AddFromAddress(emailQueue.FromAddress, emailQueue.FromAddress)
                        .AddToAddresses(emailQueue.ToAddress, emailQueue.ToAddress)
                        .AddSubject(emailQueue.Subject)
                        .AddBody(emailQueue.Body)
                        .Build();
        }

        public async Task AssignMentees()
        {
            try
            {
                int menteeportionSize = 0;
                var randomGenerator = new TRandom();
                // get algorithm by type
                var algorithm = await AlgorithmRepository.GetAlgorithmByType(AlgorithmType.AssignMentee);
                // get all mentee without mentors
                var mentees = (await _menteeRepository.GetMenteesWithoutMentors()).ToList();
                // get all mentors
                var mentors = (await MentorRepository.GetAllMentors()).ToList();
                if (algorithm == null)
                    return;
                if (mentees == null)
                    return;
                if (mentors == null)
                    return;
                 var numOfMentees = mentees.Count();
                 var numOfMentors = mentors.Count();
                // Check to see algorithm is active
                if (algorithm.IsActive && numOfMentors >= 2)
                {
                    // get the maximum number of mentees that can be assigned to a mentor
                    menteeportionSize = numOfMentees;
                    await ExecuteAlgorithm(menteeportionSize, randomGenerator, mentees, mentors);
                }
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }
        }

        private async Task ExecuteAlgorithm(int menteeportionSize, TRandom randomGenerator, 
            List<Mentee> mentees, List<Mentor> mentors)
        {
            while (mentees.Count() > 0)
            {
                Mentor selectedMentor;
                Mentee selectedMentee;
                // Perform first selection of mentee and mentor
                PerformSelection(randomGenerator, mentees, mentors, out selectedMentor,
                    out selectedMentee);
                // if the number of mentees for mentor is greater max specified then perform selection again;
                while (selectedMentor.Mentees.Count() > menteeportionSize)
                {
                    PerformSelection(randomGenerator, mentees, mentors, out selectedMentor,
                    out selectedMentee);
                }
                // assign the mentee to the mentor
                selectedMentee.Mentor = selectedMentor;
                //update mentee information in the database
                if (await _menteeRepository.Update(selectedMentee))
                {
                    // Create email Queue for mentee and mentor
                    await CreateEmailQueue(selectedMentor, selectedMentee);
                    // remove mentees that have already been asigned to a mentor
                    mentees.Remove(selectedMentee);
                }
            }
        }

        private async Task CreateEmailQueue(Mentor selectedMentor, Mentee selectedMentee)
        {
            // create email Queue
            var menteeEmailArguments = emailArgument.GetMenteeEmailArguments(selectedMentor, selectedMentee);
            var mentorEmailArguments = emailArgument.GetMentorEmailArguments(selectedMentor, selectedMentee);
            //Create email queue for mentee assignemnt
            await _emailQueueRepository.CreateEmailQueue(menteeEmailArguments, EmailMessageType.MentorAssignment);
            //Create email queue for mentor assignemnt
            await _emailQueueRepository.CreateEmailQueue(mentorEmailArguments, EmailMessageType.MenteeAssgnment);
        }

        
        private void PerformSelection(TRandom randomGenerator, System.Collections.Generic.List<Mentee> mentees,
            System.Collections.Generic.List<Mentor> mentors, 
            out Mentor selectedMentor, out Mentee selectedMentee)
        {
            // randomly select a mentor to assign
            var selectedMentorIndex = randomGenerator.Next(0, mentors.Count());
            selectedMentor = mentors[selectedMentorIndex];
            // randomly select a mentor to assign
            var selectedMenteeIndex = randomGenerator.Next(0, mentees.Count());
            selectedMentee = mentees[selectedMenteeIndex];
        }

        public async Task CalculateMeeteeScores()
        {
            try
            {
                // get algorithm by type
                var algorithm = await AlgorithmRepository.GetAlgorithmByType(AlgorithmType.CalculateMenteeScore);
                if (algorithm.IsActive)
                {
                    var endDate = DateTime.Now;
                    var startDate = endDate.AddMonths(-3);
                    var mentees = await _menteeRepository.GetAllMentees();
                    if (mentees != null)
                    {
                        foreach (var mentee in mentees)
                        {
                            // get mentee completed engagement for the quarter in question
                            var quarterlyCompletedEngagements = mentee.Engagements.Where(c => 
                            (c.DateCompleted?.Ticks >= startDate.Ticks && c.DateCompleted?.Ticks <= endDate.AddDays(-1).Ticks) &&
                            c.Status == EngagementStatus.Completed);
                           
                            // perform score calculation for the quarter and add it to the previous quarter score
                            if(quarterlyCompletedEngagements.Count() >= 1)
                               mentee.Score += 25.0d;
                            else
                               mentee.Score += 0;
                            if (await _menteeRepository.Update(mentee))
                            {
                                _logger.LogInfo($"The score of mentee with name {mentee.User.FullName} is {mentee.Score}");
                            }
                            else
                            {
                                _logger.LogError($"The score of mentee with name {mentee.User.FullName} was not updated");
                            }
                        }
                    }
                }

            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }

        }
    }
}
using System.Threading.Tasks;

namespace WebAPI.Worker.Jobs
{
    public interface ISyncJobs
    {
         Task SendPendingEmails();
         Task SendErrorEmails();
         Task AssignMentees();
         Task CalculateMeeteeScores();
    }
}
using Hangfire;
using WebAPI.Worker.Jobs;

namespace WebAPI.Worker
{
    public static class JobManager
    {
        public static void CreateRecurringJobs(){

            RecurringJob.AddOrUpdate<ISyncJobs>("SendPendingEmails", x => x.SendPendingEmails(),Cron.MinuteInterval(5));
            RecurringJob.AddOrUpdate<ISyncJobs>("SendErrorEmails",x => x.SendErrorEmails(),Cron.MinuteInterval(10));
            RecurringJob.AddOrUpdate<ISyncJobs>("AssignMentees", x => x.AssignMentees(),Cron.MinuteInterval(20));
            RecurringJob.AddOrUpdate<ISyncJobs>("CalculateMenteeScores", x => x.CalculateMeeteeScores(),"0 0 1 */3 *");
        }
    }
}
﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebAPI.Migrations
{
    public partial class removeMentorIdMenteeId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Engagements_Mentees_MenteeId",
                table: "Engagements");

            migrationBuilder.DropForeignKey(
                name: "FK_Engagements_Mentors_MentorId",
                table: "Engagements");

            migrationBuilder.DropIndex(
                name: "IX_Engagements_MenteeId",
                table: "Engagements");

            migrationBuilder.DropIndex(
                name: "IX_Engagements_MentorId",
                table: "Engagements");

            migrationBuilder.DropColumn(
                name: "MenteeId",
                table: "Engagements");

            migrationBuilder.DropColumn(
                name: "MentorId",
                table: "Engagements");

            migrationBuilder.AddColumn<int>(
                name: "MenteeUserId",
                table: "Engagements",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MentorUserId",
                table: "Engagements",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Engagements_MenteeUserId",
                table: "Engagements",
                column: "MenteeUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Engagements_MentorUserId",
                table: "Engagements",
                column: "MentorUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Engagements_Mentees_MenteeUserId",
                table: "Engagements",
                column: "MenteeUserId",
                principalTable: "Mentees",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Engagements_Mentors_MentorUserId",
                table: "Engagements",
                column: "MentorUserId",
                principalTable: "Mentors",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Engagements_Mentees_MenteeUserId",
                table: "Engagements");

            migrationBuilder.DropForeignKey(
                name: "FK_Engagements_Mentors_MentorUserId",
                table: "Engagements");

            migrationBuilder.DropIndex(
                name: "IX_Engagements_MenteeUserId",
                table: "Engagements");

            migrationBuilder.DropIndex(
                name: "IX_Engagements_MentorUserId",
                table: "Engagements");

            migrationBuilder.DropColumn(
                name: "MenteeUserId",
                table: "Engagements");

            migrationBuilder.DropColumn(
                name: "MentorUserId",
                table: "Engagements");

            migrationBuilder.AddColumn<int>(
                name: "MenteeId",
                table: "Engagements",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "MentorId",
                table: "Engagements",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Engagements_MenteeId",
                table: "Engagements",
                column: "MenteeId");

            migrationBuilder.CreateIndex(
                name: "IX_Engagements_MentorId",
                table: "Engagements",
                column: "MentorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Engagements_Mentees_MenteeId",
                table: "Engagements",
                column: "MenteeId",
                principalTable: "Mentees",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Engagements_Mentors_MentorId",
                table: "Engagements",
                column: "MentorId",
                principalTable: "Mentors",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

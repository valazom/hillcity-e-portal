﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebAPI.Migrations
{
    public partial class engagementCreatedByUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Engagements_CreatedByUserId",
                table: "Engagements",
                column: "CreatedByUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Engagements_Users_CreatedByUserId",
                table: "Engagements",
                column: "CreatedByUserId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Engagements_Users_CreatedByUserId",
                table: "Engagements");

            migrationBuilder.DropIndex(
                name: "IX_Engagements_CreatedByUserId",
                table: "Engagements");
        }
    }
}

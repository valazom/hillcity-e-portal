﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebAPI.Migrations
{
    public partial class EngagementUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Engagements_Users_CreatedByUserId",
                table: "Engagements");

            migrationBuilder.DropForeignKey(
                name: "FK_Engagements_Mentees_MenteeUserId",
                table: "Engagements");

            migrationBuilder.DropForeignKey(
                name: "FK_Engagements_Mentors_MentorUserId",
                table: "Engagements");

            migrationBuilder.DropIndex(
                name: "IX_Engagements_MenteeUserId",
                table: "Engagements");

            migrationBuilder.DropIndex(
                name: "IX_Engagements_MentorUserId",
                table: "Engagements");

            migrationBuilder.DropColumn(
                name: "MenteeUserId",
                table: "Engagements");

            migrationBuilder.DropColumn(
                name: "MentorUserId",
                table: "Engagements");

            migrationBuilder.RenameColumn(
                name: "ScheduledDate",
                table: "Engagements",
                newName: "ScheduledDateUtc");

            migrationBuilder.RenameColumn(
                name: "DateCreated",
                table: "Engagements",
                newName: "DateCreatedUtc");

            migrationBuilder.RenameColumn(
                name: "CreatedByUserId",
                table: "Engagements",
                newName: "MentorId");

            migrationBuilder.RenameIndex(
                name: "IX_Engagements_CreatedByUserId",
                table: "Engagements",
                newName: "IX_Engagements_MentorId");

            migrationBuilder.AddColumn<int>(
                name: "MenteeId",
                table: "Engagements",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Engagements_MenteeId",
                table: "Engagements",
                column: "MenteeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Engagements_Mentees_MenteeId",
                table: "Engagements",
                column: "MenteeId",
                principalTable: "Mentees",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Engagements_Mentors_MentorId",
                table: "Engagements",
                column: "MentorId",
                principalTable: "Mentors",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Engagements_Mentees_MenteeId",
                table: "Engagements");

            migrationBuilder.DropForeignKey(
                name: "FK_Engagements_Mentors_MentorId",
                table: "Engagements");

            migrationBuilder.DropIndex(
                name: "IX_Engagements_MenteeId",
                table: "Engagements");

            migrationBuilder.DropColumn(
                name: "MenteeId",
                table: "Engagements");

            migrationBuilder.RenameColumn(
                name: "ScheduledDateUtc",
                table: "Engagements",
                newName: "ScheduledDate");

            migrationBuilder.RenameColumn(
                name: "MentorId",
                table: "Engagements",
                newName: "CreatedByUserId");

            migrationBuilder.RenameColumn(
                name: "DateCreatedUtc",
                table: "Engagements",
                newName: "DateCreated");

            migrationBuilder.RenameIndex(
                name: "IX_Engagements_MentorId",
                table: "Engagements",
                newName: "IX_Engagements_CreatedByUserId");

            migrationBuilder.AddColumn<int>(
                name: "MenteeUserId",
                table: "Engagements",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MentorUserId",
                table: "Engagements",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Engagements_MenteeUserId",
                table: "Engagements",
                column: "MenteeUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Engagements_MentorUserId",
                table: "Engagements",
                column: "MentorUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Engagements_Users_CreatedByUserId",
                table: "Engagements",
                column: "CreatedByUserId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Engagements_Mentees_MenteeUserId",
                table: "Engagements",
                column: "MenteeUserId",
                principalTable: "Mentees",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Engagements_Mentors_MentorUserId",
                table: "Engagements",
                column: "MentorUserId",
                principalTable: "Mentors",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebAPI.Migrations
{
    public partial class azureInitial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "Mentors",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "State",
                table: "Mentors",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CreatedByUserId",
                table: "Engagements",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Country",
                table: "Mentors");

            migrationBuilder.DropColumn(
                name: "State",
                table: "Mentors");

            migrationBuilder.DropColumn(
                name: "CreatedByUserId",
                table: "Engagements");
        }
    }
}

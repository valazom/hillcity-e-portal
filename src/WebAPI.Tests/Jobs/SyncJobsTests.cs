using Common.Email.Service;
using Domain.Models;
using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;
using System.Threading.Tasks;
using WebAPI.Common;
using WebAPI.Data.Repository;
using WebAPI.Worker.Jobs;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Common.Email.MessageInfo;
using System;
using System.Xml;
using Domain.Enums;

namespace WebAPI.Tests.Jobs
{
    [TestFixture]
    public class SyncJobsTests
    {
        private Mock<IEmailQueueRepository> _emailQueueRepository;
        private Mock<IEmailService> _emailService;
        private Mock<IAlgorithmRepository> _algorithmRepository;
        private Mock<IMenteeRepository> _menteeRepository;
        private Mock<IMentorRepository> _mentorRepository;
        private Mock<IConfiguration> _configuration;
        private CommonEmailArgument _emailArgument;
        private Mock<ILogger<SyncJobs>> _logger;
        private SyncJobs _testJobs;
        private IEnumerable<EmailQueue> _emailQueues;
        private IEnumerable<Mentee> _mentees;
        private IEnumerable<Mentor> _mentors;
        [SetUp]
        public void InitializeInstances()
        {
            _emailQueueRepository = new Mock<IEmailQueueRepository>();
            _emailService = new Mock<IEmailService>();
            _algorithmRepository = new Mock<IAlgorithmRepository>();
            _menteeRepository = new Mock<IMenteeRepository>();
            _mentorRepository = new Mock<IMentorRepository>();
            _configuration = new Mock<IConfiguration>();
            _emailArgument = new CommonEmailArgument(_configuration.Object);
            _logger = new Mock<ILogger<SyncJobs>>();
            _testJobs = new SyncJobs(_emailQueueRepository.Object, _emailService.Object, _algorithmRepository.Object,
                _menteeRepository.Object, _mentorRepository.Object, _configuration.Object, _emailArgument, _logger.Object);
        }
        [SetUp]
        public void SetUpEmailObjects()
        {
            _emailQueues = new List<EmailQueue>
            {
                new EmailQueue
                {
                    QueueId = 1,
                    Status = Domain.Enums.EmailSentStatus.Pending,
                    ToAddress = "a",
                    FromAddress = "b",
                    Body = "ab"
                }
            };
        }
        [SetUp]
        public void SetMenteeObject()
        {
            _mentees = new List<Mentee>
            {
                new Mentee
                {
                    UserId = 1,
                    University = "a",
                    Course = "b",
                    DateOfAdmission = new System.DateTime(2019,1,12),
                    DateOfGraduation = new System.DateTime(2022,11,30)
                }
            };
        }
        [SetUp]
        public void SetMentorObject()
        {
            _mentors = new List<Mentor>
            {
                new Mentor
                {
                    UserId = 2,
                    Occupation ="m",
                    State = "n",
                    Country = "k"
                }
            };
        }
        [SetUp]
        public void SetupMocks()
        {
            _emailQueueRepository.Setup(c => c.GetErrorEmailQueues()).Returns(Task.FromResult(_emailQueues));
            _emailQueueRepository.Setup(c => c.GetPendingEmailQueues()).Returns(Task.FromResult(_emailQueues));
            _menteeRepository.Setup(c => c.GetMenteesWithoutMentors()).Returns(Task.FromResult(_mentees));
            _mentorRepository.Setup(c => c.GetAllMentors()).Returns(Task.FromResult(_mentors));
        }
                
                
        [Test]
        public async Task SendErrorEmails_WhenCalled_BuildsEmailMessage()
        {
            var emailQueues = await _emailQueueRepository.Object.GetErrorEmailQueues();
            var selectedEmailQueue = emailQueues.FirstOrDefault();
            var emailMessage = _testJobs.BuildEmailMessage(selectedEmailQueue);
            Assert.That(emailMessage.ToAddress.FirstOrDefault()?.Address, Is.EqualTo(selectedEmailQueue?.ToAddress));
            Assert.That(emailMessage.Content, Is.EqualTo(selectedEmailQueue?.Body));
            Assert.That(emailMessage.FromAddress.Address, Is.EqualTo(selectedEmailQueue?.FromAddress));
        }

        [Test]
        public async Task SendErrorEmails_WhenErrorEmailQueueIsNull_AnExceptionIsThrown()
        {
            _emailQueues = null;
            _emailQueueRepository.Setup(c => c.GetErrorEmailQueues()).Returns(Task.FromResult(_emailQueues));
            await _testJobs.SendErrorEmails();
            _logger.Verify(c => c.LogError(It.IsAny<Exception>(),It.IsAny<string>()));
        }

        [Test]
        public async Task SendEmails_WhenEmailIsSuccessfullySent_IncreasesEmailQueueSendAttempts()
        {
            _emailService.Setup(c => c.SendAsync(It.IsAny<EmailMessage>()))
                .Returns(Task.FromResult(new EmailMessageResult()));
            var emailQueue = new EmailQueue();
            await _testJobs.SendMail(new EmailMessage(), emailQueue);
            Assert.That(emailQueue.SentAttempts, Is.EqualTo(1));
        }

        [Test]
        public async Task SendEmails_WhenEmailIsSuccessfullySent_EnsureThatTheEmailQueueStatusIsChangedToSent()
        {
            _emailService.Setup(c => c.SendAsync(It.IsAny<EmailMessage>()))
                .Returns(Task.FromResult(new EmailMessageResult{Succeeded = true}));
            var emailQueue = new EmailQueue();
            await _testJobs.SendMail(new EmailMessage(), emailQueue);
            Assert.That(emailQueue.Status,Is.EqualTo(EmailSentStatus.Sent));
        }
        [Test]
        public async Task SendEmails_WhenEmailIsSuccessfullySent_EnsureThatEmailQueueIsUpdated()
        {
            _emailService.Setup(c => c.SendAsync(It.IsAny<EmailMessage>()))
                .Returns(Task.FromResult(new EmailMessageResult()));
            var emailQueue = new EmailQueue();
            await _testJobs.SendMail(new EmailMessage(), emailQueue);
            _emailQueueRepository.Verify(c =>c.Update(It.IsAny<EmailQueue>()),Times.Once);
        }
        [Test]
        public async Task SendEmails_WhenEmailIsNotSuccessfullySent_EnsureThatEmailQueueStatusIsChangedToError()
        {
            _emailService.Setup(c => c.SendAsync(It.IsAny<EmailMessage>()))
                .Returns(Task.FromResult(new EmailMessageResult{Succeeded = false}));
            var emailQueue = new EmailQueue();
            await _testJobs.SendMail(new EmailMessage(), emailQueue);
            Assert.That(emailQueue.Status,Is.EqualTo(EmailSentStatus.Error));
            _logger.Verify(c => c.LogError(It.IsAny<Exception>(),It.IsAny<string>()),Times.Once);
        }
        [Test]
        public async Task SendPendingEmails_WhenCalled_BuildsEmailMessage()
        {
            var emailQueues = await _emailQueueRepository.Object.GetPendingEmailQueues();
            var selectedEmailQueue = emailQueues.FirstOrDefault();
            var emailMessage = _testJobs.BuildEmailMessage(selectedEmailQueue);
            Assert.That(emailMessage.ToAddress.FirstOrDefault()?.Address, Is.EqualTo(selectedEmailQueue?.ToAddress));
            Assert.That(emailMessage.Content, Is.EqualTo(selectedEmailQueue?.Body));
            Assert.That(emailMessage.FromAddress.Address, Is.EqualTo(selectedEmailQueue?.FromAddress));
        }

        [Test]
        public async Task SendPendingEmails_WhenErrorEmailQueueIsNull_AnExceptionIsThrown()
        {
            _emailQueues = null;
            _emailQueueRepository.Setup(c => c.GetPendingEmailQueues()).Returns(Task.FromResult(_emailQueues));
            await _testJobs.SendPendingEmails();
            _logger.Verify(c => c.LogError(It.IsAny<Exception>(), It.IsAny<string>()));
        }
    }
}
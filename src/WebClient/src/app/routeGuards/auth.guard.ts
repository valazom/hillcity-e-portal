import { UserLoginDto } from './../models/user/userlogindto';
import { AuthService } from './../services/auth.service';
import { CanActivate, Router} from '@angular/router';
import { Injectable } from '@angular/core';
import { state } from '@angular/animations';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
constructor(private authService: AuthService, private router: Router) {}
canActivate(): boolean {
    if (this.authService.loggedIn()) {
        return true;
    }
    this.router.navigate(['account/login']);
    return false;
}
}

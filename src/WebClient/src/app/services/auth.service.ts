import { UserBaseDto } from './../models/user/userbasedto';
import { UserForgetPasswordDto } from './../models/user/userforgetpassworddto';
import { UserRegisterDto } from './../models/user/userregisterdto';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { UserResetPasswordDto } from '../models/user/userresetpassworddto';
import { UserLoginDto } from '../models/user/userlogindto';
import {JwtHelperService} from '@auth0/angular-jwt';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl = environment.appUrl + 'auth/';
  registeredEmail: string;
  private messageSource = new BehaviorSubject<any>('');
  currentMessage = this.messageSource.asObservable();
  jwtHepler = new JwtHelperService();
  decodedTokens: any;
  constructor(private httpclient: HttpClient) { }

  register(model: UserRegisterDto) {
    return this.httpclient.post(this.baseUrl + 'register', model);
  }
  forgetpassword(model: UserForgetPasswordDto) {
    return this.httpclient.post(this.baseUrl + 'forgetpassword', model);
  }
  resetpassword(model: UserResetPasswordDto) {
    return this.httpclient.post(this.baseUrl + 'resetpassword', model);
  }
  login(model: UserLoginDto) {
    return this.httpclient.post(this.baseUrl + 'login', model).pipe(
      map((response: any) => {
        const user = response;
        if (user) {
          localStorage.setItem('token', user.token);
          this.decodedTokens = this.jwtHepler.decodeToken(user.token);
          console.log(this.decodedTokens);
        }
      })
    );
  }
  loggedIn() {
    const token = localStorage.getItem('token');
    return !this.jwtHepler.isTokenExpired(token);
  }
  SetEmail(email: string) {
    this.messageSource.next(email);
  }
}


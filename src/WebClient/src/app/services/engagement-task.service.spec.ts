/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { EngagementTaskService } from './engagement-task.service';

describe('Service: EngagementTask', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EngagementTaskService]
    });
  });

  it('should ...', inject([EngagementTaskService], (service: EngagementTaskService) => {
    expect(service).toBeTruthy();
  }));
});

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CreateEngagementDto } from '../models/Engagement/createengagementdto';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { ReturnEngagementDto } from '../models/Engagement/returnengagementdto';

@Injectable({
  providedIn: 'root'
})
export class EngagementService {

  baseUrl = environment.appUrl;

constructor(private httpClient: HttpClient) { }

createEngagement(body: CreateEngagementDto): Observable<ReturnEngagementDto> {
  return this.httpClient.post<ReturnEngagementDto>(this.baseUrl + 'engagement/createengagement', body);
}
adjustEngagement(engagementId: number, patchDto: any): Observable<ReturnEngagementDto> {
  return this.httpClient.patch<ReturnEngagementDto>(this.baseUrl + 'engagement/update/' + engagementId, patchDto);
}
completeEngagement(engagementId: number) {
  return this.httpClient.post(this.baseUrl + 'engagement/completeengagement/' + engagementId, {});
}
}

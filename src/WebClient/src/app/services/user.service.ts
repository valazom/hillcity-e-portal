import { UserDto } from './../models/user/userdto';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
baseUrl = environment.appUrl;
private messageSource = new BehaviorSubject<UserDto>(null);
currentMessage = this.messageSource.asObservable();
constructor(private httpclient: HttpClient) { }
SetCurrentUser(user: UserDto) {
  this.messageSource.next(user);
}

getUser(username: any): Observable<UserDto> {
   return this.httpclient.get<UserDto>(this.baseUrl + 'user/userbyusername/' + username);
}
updateUserProfile(userId: number, patch: any) {
  return this.httpclient.patch(this.baseUrl + 'user/updateuser/' + userId, patch);
}
}

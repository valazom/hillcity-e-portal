import { MentorViewDto } from './../models/Mentor/mentorviewdto';
import { MenteeBaseDto } from './../models/mentee/menteebasedto';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { MenteeDto } from '../models/mentee/mentee';
import { AssignedMentor } from '../models/Mentor/assignedmentor';
import { MenteeEngagementDto } from '../models/Engagement/menteeengmentdto';
import { ReturnEngagementTaskDto } from '../models/EngagementTask/returnengagementtaskdto';

@Injectable({
  providedIn: 'root'
})
export class MenteeService {
  baseUrl = environment.appUrl;
  private messageSource = new BehaviorSubject<MenteeDto>(null);
  currentMessage = this.messageSource.asObservable();

  constructor(private httpClient: HttpClient) { }

  setMentee(mentee: MenteeDto) {
    this.messageSource.next(mentee);
  }

  resetMentee() {
    this.setMentee(null);
  }

  getMentee(userId: number): Observable<MenteeDto> {
    return this.httpClient.get<MenteeDto>(this.baseUrl + 'mentee/' + userId);
  }

  getAssignedMentor(userId: number): Observable<MentorViewDto> {
    return this.httpClient.get<MentorViewDto>(this.baseUrl + 'mentee/mentor/' + userId);
  }
  getMenteeEngagements(userId: number): Observable<MenteeEngagementDto> {
    return this.httpClient.get<MenteeEngagementDto>(this.baseUrl + 'mentee/engagements/' + userId);
  }
  getMenteeEngagementTasks(userId: number): Observable<ReturnEngagementTaskDto[]> {
    return this.httpClient.get<ReturnEngagementTaskDto[]>(this.baseUrl + 'mentee/engagementtask/' + userId);
  }
  creatementee(body: MenteeDto): Observable<MenteeDto> {
    return this.httpClient.post<MenteeDto>(this.baseUrl + 'mentee/creatementee', body);
  }
  updateMentee(userId: number, patch: any) {
    return this.httpClient.patch(this.baseUrl + 'mentee/updatementee/' + userId, patch);
  }
}


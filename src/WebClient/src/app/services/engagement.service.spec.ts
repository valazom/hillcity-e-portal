/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { EngagementService } from './engagement.service';

describe('Service: Engagement', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EngagementService]
    });
  });

  it('should ...', inject([EngagementService], (service: EngagementService) => {
    expect(service).toBeTruthy();
  }));
});

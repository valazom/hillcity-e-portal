import { CreateEngagementTaskDto } from './../models/EngagementTask/createengagementTaskdto';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { ReturnEngagementTaskDto } from '../models/EngagementTask/returnengagementtaskdto';

@Injectable({
  providedIn: 'root'
})
export class EngagementTaskService {
baseUrl = environment.appUrl;
constructor(private httpClient: HttpClient) { }

createEngagementTask(body: CreateEngagementTaskDto): Observable<ReturnEngagementTaskDto> {
  return this.httpClient.post<ReturnEngagementTaskDto>(this.baseUrl + 'engagementtask/create', body);
}
completeEngagementTask(engagementTaskId: number) {
  return this.httpClient.delete(this.baseUrl + 'engagementtask/delete/' + engagementTaskId);
}
}

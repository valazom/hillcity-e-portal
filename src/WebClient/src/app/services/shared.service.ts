import { JwtHelperService } from '@auth0/angular-jwt';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
jwtHelper = new JwtHelperService();
token: string;
constructor(private authService: AuthService) {
}
get userId(): number {
  return parseInt(this.authService.decodedTokens.nameid, 10);
}
get username(): string {
  return this.authService.decodedTokens.given_name;
}
get role(): string {
  return this.authService.decodedTokens.role;
}
get userToken(): string {
  return this.token;
}
LoadDecodedToken(token) {
  if (token) {
    this.authService.decodedTokens = this.jwtHelper.decodeToken(token);
    this.token = token;
  }
}
clearTokens() {
  this.authService.decodedTokens = {};
}

}

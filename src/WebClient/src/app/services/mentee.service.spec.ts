/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MenteeService } from './mentee.service';

describe('Service: Mentee', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MenteeService]
    });
  });

  it('should ...', inject([MenteeService], (service: MenteeService) => {
    expect(service).toBeTruthy();
  }));
});

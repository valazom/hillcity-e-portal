import { MentorEngagementDto } from './../models/Engagement/mentorengagementdto';
import { MenteeViewDto } from './../models/mentee/menteeviewdto';
import { AssignedMentee } from './../models/mentee/assignedMentee';
import { MenteeDto } from './../models/mentee/mentee';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Mentor } from '../models/Mentor/mentor';
import { ReturnEngagementDto } from '../models/Engagement/returnengagementdto';
import { ReturnEngagementTaskDto } from '../models/EngagementTask/returnengagementtaskdto';

@Injectable({
  providedIn: 'root'
})
export class MentorService {
baseUrl = environment.appUrl;
messageSource = new BehaviorSubject<Mentor>(null);
currentMessage = this.messageSource.asObservable();
constructor(private httpclient: HttpClient) { }

createMentor(body: Mentor): Observable<Mentor> {
  return this.httpclient.post<Mentor>(this.baseUrl + 'mentor/creatementor', body);
}
getMentor(userId: number): Observable<Mentor> {
  return this.httpclient.get<Mentor>(this.baseUrl + 'mentor/' + userId);
}
getAssigenedMentees(userId: number): Observable<MenteeViewDto> {
  return this.httpclient.get<MenteeViewDto>(this.baseUrl + 'mentor/mentees/' + userId);
}
getMentorEngagements(userId: number): Observable<MentorEngagementDto> {
  return this.httpclient.get<MentorEngagementDto>(this.baseUrl + 'mentor/engagements/' + userId);
}
updatementor(userId: number, patch: any) {
  return this.httpclient.patch(this.baseUrl + 'mentor/updatementor/' + userId, patch);
}
getMentorEngagementTasks(userId: number): Observable<ReturnEngagementTaskDto[]> {
  return this.httpclient.get<ReturnEngagementTaskDto[]>(this.baseUrl + 'mentor/engagementtask/' + userId);
}
SetMentor(mentor: Mentor) {
  this.messageSource.next(mentor);
}
resetMentor() {
  this.SetMentor(null);
}
}



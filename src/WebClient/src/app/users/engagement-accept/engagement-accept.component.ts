import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ReturnEngagementDto } from 'src/app/models/Engagement/returnengagementdto';
import { compare } from 'fast-json-patch';
import { EngagementService } from 'src/app/services/engagement.service';

@Component({
  selector: 'app-engagement-accept',
  templateUrl: './engagement-accept.component.html',
  styleUrls: ['./engagement-accept.component.scss']
})
export class EngagementAcceptComponent implements OnInit {

  constructor(private engagementService: EngagementService, private toastrservice: ToastrService) { }
  @Input() engagementInput: ReturnEngagementDto;
  @Output() engagementOutput = new EventEmitter();
  engagementResult: ReturnEngagementDto;
  IsengagementAccepted = false;
  engagementAccepted() {
    this.engagementOutput.emit(this.engagementResult);
  }
  onAccept() {
    const updatedEngagement = Object.assign({}, this.engagementInput);
    updatedEngagement.location = Object.assign({}, this.engagementInput.location);
    updatedEngagement.status = 2;
    const patchDto = compare(this.engagementInput, updatedEngagement);
    // make a call to the service
    this.engagementService.adjustEngagement(this.engagementInput.engagementId, patchDto)
    .subscribe((returnEngagementDto: ReturnEngagementDto) => {
      this.engagementResult = returnEngagementDto;
      this.engagementAccepted();
      this.IsengagementAccepted = true;
      this.toastrservice.success(`The engagement with ID: ${this.engagementInput.subject} has been successfully accepted`);
    }, error => {
      this.toastrservice.error(error);
    });
  }
  translateCommunicationMode(mode: number) {
    switch (mode) {
      case 1:
        return 'Face to Face';
      case 2:
        return 'WhatsApp Video Call';
      case 3:
        return 'WhatsApp Voice Call';
      case 4:
        return 'Skype Video Call';
      case 5:
        return 'Skype Voice Call';
      case 6:
        return 'Phone Call';
    }
  }

  ngOnInit() {
  }

}

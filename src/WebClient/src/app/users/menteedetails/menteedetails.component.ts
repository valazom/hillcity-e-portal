import { AssignedMentee } from './../../models/mentee/assignedMentee';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-menteedetails',
  templateUrl: './menteedetails.component.html',
  styleUrls: ['./menteedetails.component.scss']
})
export class MenteedetailsComponent implements OnInit {

  constructor() { }

  @Input() currentMentee: AssignedMentee;

  ngOnInit() {
  }

}

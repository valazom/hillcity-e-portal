import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { EngagementService } from 'src/app/services/engagement.service';
import { ReturnEngagementDto } from 'src/app/models/Engagement/returnengagementdto';
import { ToastrService } from 'ngx-toastr';
import { compare } from 'fast-json-patch';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-engagement-complete',
  templateUrl: './engagement-complete.component.html',
  styleUrls: ['./engagement-complete.component.scss']
})
export class EngagementCompleteComponent implements OnInit {

  constructor(private engagementService: EngagementService, private toastrservice: ToastrService) { }
  @Input() engagementInput: ReturnEngagementDto;
  @Input() engagementIndex: number;
  @Output() engagementOutput = new EventEmitter();
  engagementResult: ReturnEngagementDto;
  IsengagementAccepted = false;
  IsEngagementCompleted = true;
  engagementAccepted() {
    this.engagementOutput.emit(this.engagementResult);
  }
  onAccept() {
    // make a call to the service
    this.IsEngagementCompleted = false;
    this.engagementService.completeEngagement(this.engagementInput.engagementId)
    .subscribe(() => {
      this.engagementResult = Object.assign({}, this.engagementInput);
      this.engagementResult.location = Object.assign({}, this.engagementInput.location);
      this.engagementResult.status = 3;
      this.engagementAccepted();
      this.IsengagementAccepted = true;
      this.IsEngagementCompleted = true;
      this.toastrservice.success(`The engagement with ID: ${this.engagementInput.subject} has been successfully completed`);
    }, error => {
      this.toastrservice.error(error);
      this.IsEngagementCompleted = true;
    });
  }

  hasEngageCompleted() {
    return this.IsEngagementCompleted;
  }

  translateCommunicationMode(mode: number) {
    switch (mode) {
      case 1:
        return 'Face to Face';
      case 2:
        return 'WhatsApp Video Call';
      case 3:
        return 'WhatsApp Voice Call';
      case 4:
        return 'Skype Video Call';
      case 5:
        return 'Skype Voice Call';
      case 6:
        return 'Phone Call';
    }
  }

  ngOnInit() {
  }

}

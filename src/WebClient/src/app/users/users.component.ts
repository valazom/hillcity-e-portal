import { MenteeDto } from './../models/mentee/mentee';
import { Mentor } from './../models/Mentor/mentor';
import { MentorService } from 'src/app/services/mentor.service';
import { MenteeService } from './../services/mentee.service';
import { SharedService } from './../services/shared.service';
import { UserDto } from './../models/user/userdto';
import { UserService } from './../services/user.service';
import { Component, OnInit } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { ToastService } from 'ng-uikit-pro-standard';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  jwtHelper = new JwtHelperService();
  Role: any;
  userid: number;
  username: string;
  user: UserDto;
  profilemessage: string;
  constructor(public authService: AuthService, private router: Router,
    private userService: UserService, public sharedService: SharedService,
    private menteeService: MenteeService, private mentorService: MentorService, private toastrService: ToastrService) {}
  loggedOut() {
    localStorage.removeItem('token');
    this.userService.SetCurrentUser(null);
    this.menteeService.resetMentee();
    this.mentorService.resetMentor();
    this.router.navigate(['/home']);
  }
  get User(): UserDto {
    this.userService.currentMessage.subscribe(cuser => this.user = cuser);
    return this.user;
  }
  getPhoto() {
    this.userService.currentMessage.subscribe(cuser => this.user = cuser);
    if (this.user) {
      if (this.user.photo && this.user.photo !== '') {
        return this.user.photo;
      }
    }
    return 'assets/images/userimage.png';
  }

  LoadEngagement() {
    if (!this.isFunctionloaded()) {
      this.toastrService.error(this.profilemessage);
    } else {
      if (this.Role === 'Mentee') {
        this.router.navigate(['/user/users/menteeengagement']);
      } else if (this.Role === 'Mentor') {
        this.router.navigate(['/user/users/mentorengagement']);
      }
    }
  }

  LoadEngagementTaskView() {
    if (!this.isFunctionloaded()) {
      this.toastrService.error(this.profilemessage);
    } else {
       if (this.Role === 'Mentee') {
          this.router.navigate(['/user/users/menteeengagementtask']);
       } else if (this.Role === 'Mentor') {
        this.router.navigate(['/user/users/mentorengagementtask']);
       }
    }
  }

  LoadMenteeView() {
    if (!this.isFunctionloaded()) {
      this.toastrService.error(this.profilemessage);
    } else {
      this.router.navigate(['/user/users/menteeview']);
    }
  }

  LoadMentorView() {
    if (!this.isFunctionloaded()) {
      this.toastrService.error(this.profilemessage);
    } else {
      this.router.navigate(['/user/users/mentorview']);
    }
  }

  isFunctionloaded() {
    let result: boolean;
    if (this.Role === 'Mentee') {
      this.menteeService.currentMessage.subscribe((menteedto: MenteeDto) => {
          if (menteedto) {
            result = true;
          } else {
            this.profilemessage = 'Your mentee profile has not been created. kindly do that and try again';
            result = false;
          }
      });
    }
    if (this.Role === 'Mentor') {
      this.mentorService.currentMessage.subscribe((mentordto: Mentor) => {
        if (mentordto) {
          result = true;
        } else {
          this.profilemessage = 'Your mentor profile has not been created. kindly do that and try again';
          result = false;
        }
      });
    }
    return result;
  }

  IsMentee() {
    if (this.Role === 'Mentee') {
      return true;
    }
    return false;
  }

  loadRole() {
    if (this. Role === 'Mentee') {
      this.loadMentee();
    }

    if (this.Role === 'Mentor') {
      this.loadMentor();
    }
  }

  loadMentee() {
    this.menteeService.getMentee(this.sharedService.userId).subscribe((menteedto: MenteeDto) => {
      this.menteeService.setMentee(menteedto);
    });
  }
  loadMentor() {
    this.mentorService.getMentor(this.sharedService.userId).subscribe((mentordto: Mentor) => {
      this.mentorService.SetMentor(mentordto);
    });
  }

  IsMentor() {
    if (this.Role === 'Mentor') {
      return true;
    }
    return false;
  }
  ngOnInit() {
    const token = localStorage.getItem('token');
    this.sharedService.LoadDecodedToken(token);
    this.Role = this.sharedService.role;
    this.username = this.sharedService.username;
    this.userid = this.sharedService.userId;
    this.userService.getUser(this.username).subscribe((userdto: UserDto) => {
      this.userService.SetCurrentUser(userdto);
    }, error => {
      this.user = new UserDto();
    });
    this.loadRole();
  }

}

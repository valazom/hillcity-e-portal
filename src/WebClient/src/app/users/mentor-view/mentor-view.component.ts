import { MentorViewDto } from './../../models/Mentor/mentorviewdto';
import { AssignedMentor } from './../../models/Mentor/assignedmentor';
import { SharedService } from './../../services/shared.service';
import { Component, OnInit } from '@angular/core';
import { MenteeService } from 'src/app/services/mentee.service';

@Component({
  selector: 'app-mentor-view',
  templateUrl: './mentor-view.component.html',
  styleUrls: ['./mentor-view.component.scss']
})
export class MentorViewComponent implements OnInit {
  mentor: AssignedMentor;
  numberOfPendingEngagments: number;
  numberOfCompletedEngagements: number;
  constructor(private menteeService: MenteeService, private sharedService: SharedService) {
    this.GetAssignedMentor();
  }

  HasMentor() {
    if (this.mentor) {
      return true;
    }
    return false;
  }
  GetAssignedMentor() {
    this.menteeService.getAssignedMentor(this.sharedService.userId).subscribe((mentorviewDto: MentorViewDto) => {
      this.mentor = mentorviewDto.assignedMentor;
      this.numberOfCompletedEngagements = mentorviewDto.numberOfCompletedEngagements;
      this.numberOfPendingEngagments = mentorviewDto.numberOfPendingEngagements;
    });
  }
  getPhoto() {
    if (this.mentor) {
      if (this.mentor.user.photo && this.mentor.user.photo !== '') {
        return this.mentor.user.photo;
      }
    }
    return 'assets/images/userimage.png';
  }
  ngOnInit() {
  }

}

import { SharedService } from './../../services/shared.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { UserPatchDto } from './../../models/user/userpatchdto';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { UserDto } from 'src/app/models/user/userdto';
import { UserService } from 'src/app/services/user.service';
import { LoctionDto } from 'src/app/models/user/locationdto';
import { ToastrService } from 'ngx-toastr';
import { compare } from 'fast-json-patch';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  Role: any;
  user: UserDto;
  userpatchdto: UserPatchDto[] = [];
  userProfileForm: FormGroup;
  IsError = false;
  JwtHelper = new JwtHelperService();
  constructor(private authSeervice: AuthService, private userService: UserService,
    private toastr: ToastrService,
    private formbuilder: FormBuilder,
    private sharedService: SharedService) {
    this.getUser();
    this.userProfileForm = this.createUserProfileFormBuilder(formbuilder);
  }
  onSubmit() {
    if (this.userProfileForm.valid) {
       const initRequest = {FirstName: '', LastName: '', Location: {Address: ''} };
       const userUpdateRequest = Object.assign({}, this.userProfileForm.value);
       userUpdateRequest.Location = Object.assign({}, userUpdateRequest.Location);
       const patch = compare(initRequest, userUpdateRequest);
       console.log(patch);
       this.userService.updateUserProfile(this.user.userId, patch).subscribe(() => {
          this.user.firstName = userUpdateRequest.firstName;
          this.user.lastName = userUpdateRequest.lastName;
          if (!this.user.location) {
            this.user.location = new LoctionDto();
          }
          this.user.location.address = userUpdateRequest.Location.Address;
          this.userService.SetCurrentUser(this.user);
          this.toastr.success('your profile successfully updated');
       }, error => {
          this.toastr.error('Your user profile was not updated');
       });
    } else {
       this.IsError = true;
    }
    this.userpatchdto = [];
  }
  IsMentee() {
    if (this.Role === 'Mentee') {
      return true;
    }
    return false;
  }
  IsMentor() {
    if (this.Role === 'Mentor') {
      return true;
    }
    return false;
  }
  closeAlert() {
    this.IsError = false;
  }
  getUser() {
    const token = localStorage.getItem('token');
    this.sharedService.LoadDecodedToken(token);
    const username = this.sharedService.username;
    this.userService.getUser(username).subscribe((userdto: UserDto) => {
      this.user = userdto;
      this.userService.SetCurrentUser(this.user);
      this.userProfileForm.patchValue(this.user);
    });
  }
  createUserProfileFormBuilder(formbuilder: FormBuilder) {
    return formbuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      location: formbuilder.group({
        address: ''
      })
    });
  }
  ngOnInit() {
    this.Role = this.sharedService.role;
  }

}

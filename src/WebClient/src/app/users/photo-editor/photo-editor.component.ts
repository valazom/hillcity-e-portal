import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { SharedService } from 'src/app/services/shared.service';
import { UserService } from './../../services/user.service';
import { UserDto } from './../../models/user/userdto';
import { Component, OnInit, EventEmitter } from '@angular/core';
import { UploadFile, UploadInput, humanizeBytes, UploadOutput } from 'ng-uikit-pro-standard';
import { environment } from 'src/environments/environment';
import { url } from 'inspector';

@Component({
  selector: 'app-photo-editor',
  templateUrl: './photo-editor.component.html',
  styleUrls: ['./photo-editor.component.scss']
})
export class PhotoEditorComponent implements OnInit {
  user: UserDto;
  Url: string;
  formData: FormData;
  files: UploadFile[];
  uploadInput: EventEmitter<UploadInput>;
  humanizeBytes: Function;
  dragOver: boolean;
  constructor(private userService: UserService, private sharedService: SharedService, private toastrService: ToastrService) {
    this.loadUser();
    this.files = [];
    this.uploadInput = new EventEmitter<UploadInput>();
    this.humanizeBytes = humanizeBytes;
    this.Url = environment.appUrl + 'user/' + this.sharedService.userId + '/photo';
  }
  loadUser() {
    this.userService.currentMessage.subscribe((cuser: UserDto) => this.user = cuser);
  }
  showFiles() {
    let files = '';
    for (let i = 0; i < this.files.length; i ++) {
      files = this.files[i].name;
    }
    return files;
  }

  startUpload(): void {
    if (this.files.length > 0) {
      const event: UploadInput = {
        type: 'uploadFile',
        url: this.Url,
        method: 'PUT',
        file: this.files[this.files.length - 1],
        data: { foo: 'bar' },
        headers: {'Authorization' : 'Bearer ' + localStorage.getItem('token') }
      };
      this.files = [];
      this.uploadInput.emit(event);
    } else {
      this.toastrService.error('No file selected');
    }
  }

  cancelUpload(id: string): void {
    this.uploadInput.emit({ type: 'cancel', id: id });
  }

  onUploadOutput(output: UploadOutput | any): void {
    if (output.type === 'allAddedToQueue') {
    } else if (output.type === 'addedToQueue') {
      this.files.push(output.file); // add file to array when added
    } else if (output.type === 'uploading') {
      // update current data in files array for uploading file
      const index = this.files.findIndex(file => file.id === output.file.id);
      this.files[index] = output.file;
    } else if (output.type === 'removed') {
      // remove file from array when removed
      this.files = this.files.filter((file: UploadFile) => file !== output.file);
    } else if (output.type === 'done') {
      const statusCode = output.file.responseStatus;
      if (statusCode === 200) {
         this.user = output.file.response as UserDto;
         this.userService.SetCurrentUser(this.user);
         this.toastrService.success('Your photo image was successfully uploaded');
      } else if (statusCode === 400) {
         this.toastrService.error('Invalid file format');
      } else {
        this.toastrService.error('Your photo image was not uploaded');
      }
    } else if (output.type === 'dragOver') {
      this.dragOver = true;
    } else if (output.type === 'dragOut') {
    } else if (output.type === 'drop') {
      this.dragOver = false;
    }
    this.showFiles();
  }
  removeFile(id: string): void {
    this.uploadInput.emit({ type: 'remove', id: id });
  }

  removeFirstFileFromQueue() {
    if (this.files.length > 0) {
      this.removeFile(this.files[0].id);
    } else {
      this.toastrService.error('No file selected');
    }
  }
  ngOnInit() {
  }

}

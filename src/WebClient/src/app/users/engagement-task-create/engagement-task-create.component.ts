import { CreateEngagementDto } from './../../models/Engagement/createengagementdto';
import { CreateEngagementTaskDto } from './../../models/EngagementTask/createengagementTaskdto';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { ReturnEngagementDto } from 'src/app/models/Engagement/returnengagementdto';
import { EngagementTaskService } from 'src/app/services/engagement-task.service';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import { ReturnEngagementTaskDto } from 'src/app/models/EngagementTask/returnengagementtaskdto';

@Component({
  selector: 'app-engagement-task-create',
  templateUrl: './engagement-task-create.component.html',
  styleUrls: ['./engagement-task-create.component.scss']
})
export class EngagementTaskCreateComponent implements OnInit {
  @Input() engagementInput: ReturnEngagementDto;
  optionSelect: Array<any>;
  engagementTaskDto: CreateEngagementTaskDto;
  engagementTaskFormGroup: FormGroup;

  constructor(private engagementTaskservice: EngagementTaskService, private toastrService: ToastrService,
     private formbuilder: FormBuilder, private datepipe: DatePipe) {
      this.engagementTaskFormGroup = this.createEngagmentTaskFormGroup(formbuilder);
      this.engagementTaskDto = new CreateEngagementTaskDto();
  }

  onSubmit() {
    if (this.engagementTaskFormGroup.valid) {
      const engagemenTaskRequest = Object.assign({}, this.engagementTaskFormGroup.value);
      this.updateEngagementTask(engagemenTaskRequest);
      // call the engagement Task service
      // tslint:disable-next-line:max-line-length
      this.engagementTaskservice.createEngagementTask(this.engagementTaskDto)
      .subscribe((returnEngagementTaskDto: ReturnEngagementTaskDto) => {
        this.engagementTaskFormGroup.reset();
        this.toastrService.success('Engagement Task successfully created. View in EngagementTask dashboard');
      }, error => {
        this.toastrService.error(error);
      });
    }
  }

  updateEngagementTask(engagementTaskRequest: any) {
    this.engagementTaskDto.taskCategory = engagementTaskRequest.taskCategory;
    this.engagementTaskDto.details = engagementTaskRequest.details;
    this.engagementTaskDto.startDate = this.datepipe.transform(new Date(), 'yyyy-MM-dd');
    this.engagementTaskDto.status = 1;
    this.engagementTaskDto.engagementId = this.engagementInput.engagementId;
  }

  createEngagmentTaskFormGroup(formbuilder: FormBuilder) {
    return formbuilder.group({
      taskCategory: ['', Validators.required],
      details: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.loadOptions();
  }

  loadOptions() {
    this.optionSelect = [
      { value: '1', label: 'Principles And Values'},
      { value: '2', label: 'Academic Review' },
      { value: '3', label: 'General Well Being Of Mentee' },
      { value: '4', label: 'Mentee Life Goals And Complete,' },
      { value: '5', label: 'Any Other Business' },
    ];
  }

  translateCommunicationMode(mode: number) {
    switch (mode) {
      case 1:
        return 'Face to Face';
      case 2:
        return 'WhatsApp Video Call';
      case 3:
        return 'WhatsApp Voice Call';
      case 4:
        return 'Skype Video Call';
      case 5:
        return 'Skype Voice Call';
      case 6:
        return 'Phone Call';
    }
  }
}

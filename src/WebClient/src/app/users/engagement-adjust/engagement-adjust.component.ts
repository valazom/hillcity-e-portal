import { ToastrService } from 'ngx-toastr';

import { UpdateEngagementDto } from './../../models/Engagement/updateengagementdto';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ReturnEngagementDto } from 'src/app/models/Engagement/returnengagementdto';
import { IMyOptions } from 'ng-uikit-pro-standard';
import { LoctionDto } from 'src/app/models/user/locationdto';
import { compare } from 'fast-json-patch';
import { EngagementService } from 'src/app/services/engagement.service';

@Component({
  selector: 'app-engagement-adjust',
  templateUrl: './engagement-adjust.component.html',
  styleUrls: ['./engagement-adjust.component.scss']
})
export class EngagementAdjustComponent implements OnInit {
  @Input() engagementInput: ReturnEngagementDto;
  @Output() engagementOutput = new EventEmitter();
  optionCommMode: Array<any>;
  optionSelect: Array<any>;
  engagement: ReturnEngagementDto;
  engagementAdjustFormGroup: FormGroup;
  modeSelectedValue = -1;
  public myDatePickerOptions: IMyOptions = {
  };
  constructor(private formbuilder: FormBuilder, private engagementService: EngagementService, private toasrService: ToastrService) {
    this.engagementAdjustFormGroup = this.createEngagementAdjustFormGroup(formbuilder);
  }

  engagementAjusted() {
    this.engagementOutput.emit(this.engagement);
  }

  onSubmit() {
    if (this.engagementAdjustFormGroup.valid) {
      const oldEngagement = this.createUpdateEngagementDto(this.engagementInput);
      const newEngagementRequest = Object.assign({}, this.engagementAdjustFormGroup.value);
      const adjustedEngagement = this.updateEngagementModel(oldEngagement, newEngagementRequest);
      const patchDto = compare(oldEngagement, adjustedEngagement);
      // call engagement service to update engagement
      // tslint:disable-next-line:max-line-length
      this.engagementService.adjustEngagement(this.engagementInput.engagementId, patchDto).subscribe((returnengagementdto: ReturnEngagementDto) => {
        this.engagement = returnengagementdto;
        this.engagementAjusted();
        this.toasrService.success(`You have successfully adjusted engagement with ID: ${this.engagementInput.subject}`);
      }, error => {
        this.toasrService.error(error);
      });
    }
  }

  createUpdateEngagementDto(engagement: ReturnEngagementDto) {
    const  oldUpdateEngagement = new UpdateEngagementDto();
    oldUpdateEngagement.subject = engagement.subject;
    oldUpdateEngagement.startTime = engagement.startTime;
    oldUpdateEngagement.duration = engagement.duration;
    oldUpdateEngagement.communicationMode = engagement.communicationMode;
    oldUpdateEngagement.status = engagement.status;
    oldUpdateEngagement.engagementReport = engagement.engagementReport;
    oldUpdateEngagement.engagementId = engagement.engagementId;
    oldUpdateEngagement.mentorId = engagement.mentor.user.userId;
    oldUpdateEngagement.menteeId = engagement.mentee.user.userId;
    oldUpdateEngagement.dateCreatedUtc = engagement.dateCreatedUtc;
    oldUpdateEngagement.scheduledDateUtc = engagement.scheduledDateUtc;
    oldUpdateEngagement.location = new LoctionDto();
    oldUpdateEngagement.location.address = engagement.location.address;
    return oldUpdateEngagement;
  }

  updateEngagementModel(oldmodel: UpdateEngagementDto, newRequest: any) {
    const newUpdateEngagementDto = Object.assign({}, oldmodel);
    newUpdateEngagementDto.location = Object.assign({}, oldmodel.location);
    newUpdateEngagementDto.startTime = newRequest.startTime;
    newUpdateEngagementDto.duration = newRequest.duration;
    newUpdateEngagementDto.scheduledDateUtc = newRequest.scheduledDateUtc;
    const mode = parseInt(newRequest.communicationMode, 10);
    if (mode !== newUpdateEngagementDto.communicationMode) {
       newUpdateEngagementDto.subject = 'EN.' + this.translateCommunicationMode(mode)  + '.' + Date.now();
    }
    newUpdateEngagementDto.communicationMode = mode;
    newUpdateEngagementDto.location.address = newRequest.location.address;
    return newUpdateEngagementDto;
  }

  createEngagementAdjustFormGroup(formbuilder: FormBuilder) {
    return formbuilder.group({
      startTime: ['', Validators.required],
      duration: ['', Validators.required],
      scheduledDateUtc: ['', Validators.required],
      communicationMode: ['', Validators.required],
      location: formbuilder.group({
        address: ''
      })
    });
  }

  translateCommunicationMode(mode: number) {
    switch (mode) {
      case 1:
        return 'Face to Face';
      case 2:
        return 'WhatsApp Video Call';
      case 3:
        return 'WhatsApp Voice Call';
      case 4:
        return 'Skype Video Call';
      case 5:
        return 'Skype Voice Call';
      case 6:
        return 'Phone Call';
    }
  }

  loadOptions() {
    this.optionSelect = [
      { value: '01:00:00', label: '1hr' },
      { value: '02:00:00', label: '2hrs' },
      { value: '03:00:00', label: '3hrs' },
      { value: '04:00:00', label: '4hrs' },
    ];
    this.optionCommMode = [
      { value: '1', label: 'Face to Face' },
      { value: '2', label: 'WhatsApp Video Call' },
      { value: '3', label: 'WhatsApp Voice Call' },
      { value: '4', label: 'Skype Video Call' },
      { value: '5', label: 'Skype Voice Call' },
      { value: '6', label: 'Phone Call' },
    ];
  }

  ngOnInit() {
    this.loadOptions();
    // this.engagementAdjustFormGroup.patchValue(this.engagementInput);
    // tslint:disable-next-line:max-line-length
    this.engagementAdjustFormGroup.setValue({'startTime': this.engagementInput.startTime, 'duration': this.engagementInput.duration, 'scheduledDateUtc': this.engagementInput.scheduledDateUtc,
    // tslint:disable-next-line:max-line-length
    'communicationMode': this.engagementInput.communicationMode.toString(), 'location': {'address': this.engagementInput.location.address}});
  }

}

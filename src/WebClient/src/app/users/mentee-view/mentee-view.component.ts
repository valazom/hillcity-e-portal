import { MenteeViewDto } from './../../models/mentee/menteeviewdto';
import { AssignedMentee } from './../../models/mentee/assignedMentee';
import { Component, OnInit, AfterViewInit, ViewChild, ChangeDetectorRef, ElementRef, HostListener } from '@angular/core';
import { MdbTablePaginationComponent, MdbTableService, MdbTableDirective } from 'ng-uikit-pro-standard';
import { MentorService } from 'src/app/services/mentor.service';
import { SharedService } from 'src/app/services/shared.service';

@Component({
  selector: 'app-mentee-view',
  templateUrl: './mentee-view.component.html',
  styleUrls: ['./mentee-view.component.scss']
})
export class MenteeViewComponent implements OnInit, AfterViewInit {
  @ViewChild(MdbTableDirective) mdbTable: MdbTableDirective;
  @ViewChild(MdbTablePaginationComponent) mdbPagination: MdbTablePaginationComponent;
  @ViewChild('row') row: ElementRef;

  optionsSelect: Array<any>;
  selectedValue = 1;
  mentees: AssignedMentee[] = [];
  numberOfPendingEnagagaments: number;
  numberOfCompletedEngagements: number;
  headElements = ['PHOTO', 'NAME', 'SCHOOL', 'PHONE NO.', 'EMAIL', 'ACTIONS'];

  searchText: string;
  previous: string;
  firstItemIndex;
  lastItemIndex;
  IsSpinner = true;
  constructor(private mentorService: MentorService, private tableService: MdbTableService,
    private cdRef: ChangeDetectorRef, private sharedService: SharedService) {

  }
  getSelectedValue(event: any) {
    console.log(this.selectedValue);
    console.log(event);
  }
  @HostListener('input') oninput() {
    this.mdbPagination.searchText = this.searchText;
  }

  getMentees() {
    this.mentorService.getAssigenedMentees(this.sharedService.userId).subscribe((menteeview: MenteeViewDto) => {
      this.loadTableSource(menteeview.assignedMentees);
      this.numberOfPendingEnagagaments = menteeview.numberOfPendingEnagagaments;
      this.numberOfCompletedEngagements = menteeview.numberOfCompletedEngagements;
    });
  }

  loadPagination(entries: any) {
    this.mdbPagination.setMaxVisibleItemsNumberTo(entries);
    this.firstItemIndex = this.mdbPagination.firstItemIndex;
    this.lastItemIndex = this.mdbPagination.lastItemIndex;

    this.mdbPagination.calculateFirstItemIndex();
    this.mdbPagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }
  loadOptions() {
    this.optionsSelect = [
      { value: '5', label: 'show 5 entries' },
      { value: '10', label: 'show 10 entries' },
      { value: '20', label: 'show 20 entries' },
      { value: '50', label: 'show 50 entries' },
      ];
  }

  loadTableSource(mentees: AssignedMentee[]) {
    this.tableService.setDataSource(mentees);
    this.IsSpinner = false;
    this.mentees = this.tableService.getDataSource();
    this.previous = this.tableService.getDataSource();
  }

  ngOnInit() {
    this.getMentees();
    this.loadOptions();
  }

  ngAfterViewInit() {
    this.loadPagination(5);
  }
  getCurrentMentees() {
    return this.mentees;
  }
  onNextPageClick(data: any) {
    this.firstItemIndex = data.first;
    this.lastItemIndex = data.last;
  }

  onPreviousPageClick(data: any) {
    this.firstItemIndex = data.first;
    this.lastItemIndex = data.last;
  }

  getNumberOfMentees() {
    return this.tableService.getDataSource().length;
  }

  searchItems() {
    const prev = this.tableService.getDataSource();

    if (!this.searchText) {
      this.tableService.setDataSource(this.previous);
      this.mentees = this.tableService.getDataSource();
    }

    if (this.searchText) {
      // tslint:disable-next-line:max-line-length
      this.mentees = [];
      // tslint:disable-next-line:max-line-length
      this.mentees = this.tableService.getDataSource().filter((menteeModel: AssignedMentee) => menteeModel.user.firstName.indexOf(this.searchText) !== -1
       || menteeModel.user.lastName.indexOf(this.searchText) !== -1 );
      this.tableService.setDataSource(prev);
    }

    this.mdbPagination.calculateFirstItemIndex();
    this.mdbPagination.calculateLastItemIndex();

    this.tableService.searchDataObservable(this.searchText).subscribe((data: any) => {
      if (data.length === 0) {
        this.firstItemIndex = 0;
      }
    });
  }

}

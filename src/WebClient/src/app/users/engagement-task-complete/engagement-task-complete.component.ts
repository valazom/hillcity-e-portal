import { ToastrService } from 'ngx-toastr';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ReturnEngagementTaskDto } from 'src/app/models/EngagementTask/returnengagementtaskdto';
import { EngagementTaskService } from 'src/app/services/engagement-task.service';

@Component({
  selector: 'app-engagement-task-complete',
  templateUrl: './engagement-task-complete.component.html',
  styleUrls: ['./engagement-task-complete.component.scss']
})
export class EngagementTaskCompleteComponent implements OnInit {
  @Input() engagementTaskInput: ReturnEngagementTaskDto;
  @Output() engagementTaskOutput = new EventEmitter();
  engagementTaskResult: ReturnEngagementTaskDto;
  IsComplete = false;
  constructor(private engagementTaskService: EngagementTaskService, private toastrService: ToastrService) { }
  onEngagementTaskCompleted() {
    this.engagementTaskOutput.emit(this.engagementTaskResult);
  }
  updateEngagementTaskResult() {
    this.engagementTaskResult = Object.assign({}, this.engagementTaskInput);
  }
  onSubmit() {
    if (!this.engagementTaskInput.engagement.engagementReport || this.engagementTaskInput.engagement.engagementReport === '' ) {
      // tslint:disable-next-line:max-line-length
      this.toastrService.error(`Engagement report has not been uploaded. Kindly contact your mentee:
       ${this.engagementTaskInput.engagement.mentee.user.firstName} ${this.engagementTaskInput.engagement.mentee.user.lastName}
       to upload the report`);
       return;
    }
    this.engagementTaskService.completeEngagementTask(this.engagementTaskInput.engagementTaskId).subscribe(() => {
      this.updateEngagementTaskResult();
      this.onEngagementTaskCompleted();
      this.IsComplete = true;
      this.toastrService.success('Engagement Task successfully completed');
    }, error => {
      this.toastrService.error(error);
    });
  }
  hasEngageCompleted() {
    return this.IsComplete;
  }
  ngOnInit() {
  }

}

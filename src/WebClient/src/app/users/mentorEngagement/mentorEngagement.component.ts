import { MenteeEngagementDto } from './../../models/Engagement/menteeengmentdto';
import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, ChangeDetectorRef, HostListener } from '@angular/core';
import { MdbTableDirective, MdbTablePaginationComponent, MdbTableService } from 'ng-uikit-pro-standard';
import { MentorService } from 'src/app/services/mentor.service';
import { SharedService } from 'src/app/services/shared.service';
import { ReturnEngagementDto } from 'src/app/models/Engagement/returnengagementdto';
import { MentorEngagementDto } from 'src/app/models/Engagement/mentorengagementdto';

@Component({
  selector: 'app-mentorengagement',
  templateUrl: './mentorEngagement.component.html',
  styleUrls: ['./mentorEngagement.component.scss']
})
export class MentorEngagementComponent implements OnInit, AfterViewInit {
  @ViewChild(MdbTableDirective) mdbTable: MdbTableDirective;
  @ViewChild(MdbTablePaginationComponent) mdbPagination: MdbTablePaginationComponent;
  @ViewChild('row') row: ElementRef;
  optionsSelect: Array<any>;
  optionsEnselect: Array<any>;
  numberOfPendingEngagements: number;
  numberOfCompletedEngagements: number;
  numberOfAssignedMentees: number;
  engagements: ReturnEngagementDto[] = [];
  // tslint:disable-next-line:max-line-length
  headElements = ['ID', 'REQUESTED BY', 'SUBMITTED', 'SCHEDULED', 'MODE', 'TIME', 'STATUS', 'REPORT', 'ACTIONS'];

  selectedValue = 1;
  enSelectedValue = 1;
  searchText: string;
  previous: string;
  firstItemIndex;
  lastItemIndex;
  IsSpinner = true;
  constructor(private mentorService: MentorService, private tableService: MdbTableService,
    private cdRef: ChangeDetectorRef, private sharedService: SharedService) {
      this.getMentorEngagments();
    }

  loadOptions() {
    this.optionsSelect = [
      { value: '5', label: 'show 5 entries' },
      { value: '10', label: 'show 10 entries' },
      { value: '20', label: 'show 20 entries' },
      { value: '50', label: 'show 50 entries' },
      ];
  }
  loadEngagementOptions() {
    this.optionsEnselect = [
      { value: '-1', label: 'All' },
      { value: '1', label: 'Pending' },
      { value: '2', label: 'Accepted' },
      { value: '3', label: 'Completed' },
      ];
  }
  @HostListener('input') oninput() {
    this.mdbPagination.searchText = this.searchText;
  }

  loadPagination(entries: any) {
    this.mdbPagination.setMaxVisibleItemsNumberTo(entries);
    this.firstItemIndex = this.mdbPagination.firstItemIndex;
    this.lastItemIndex = this.mdbPagination.lastItemIndex;

    this.mdbPagination.calculateFirstItemIndex();
    this.mdbPagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  loadTableSource(mentees: ReturnEngagementDto[]) {
    this.tableService.setDataSource(mentees);
    this.IsSpinner = false;
    this.engagements = this.tableService.getDataSource();
    this.previous = this.tableService.getDataSource();
  }

  getMentorEngagments() {
     this.mentorService.getMentorEngagements(this.sharedService.userId).subscribe((mentorEngagementDto: MentorEngagementDto) => {
      if (!mentorEngagementDto.engagements) {
        mentorEngagementDto.engagements = [];
     }
     this.loadTableSource(mentorEngagementDto.engagements);
     this.numberOfPendingEngagements = mentorEngagementDto.numberOfPendingEngagements;
     this.numberOfCompletedEngagements = mentorEngagementDto.numberOfCompletedEngagements;
     this.numberOfAssignedMentees = mentorEngagementDto.numberOfAssignedMentees;
     });
  }

  hasEngagementReport(engagement: ReturnEngagementDto) {
    if (engagement.engagementReport && engagement.engagementReport !== '') {
      return true;
    }
    return false;
  }

  translateEnagementStatus(statusCode: number) {
    switch (statusCode) {
      case 1:
        return 'Pending';
      case 2:
        return 'Accepted';
      case 3:
        return 'Completed';
    }
  }

  translateCommunicationMode(mode: number) {
    switch (mode) {
      case 1:
        return 'Face to Face';
      case 2:
        return 'WhatsApp Video Call';
      case 3:
        return 'WhatsApp Voice Call';
      case 4:
        return 'Skype Video Call';
      case 5:
        return 'Skype Voice Call';
      case 6:
        return 'Phone Call';
    }
  }

  ngOnInit() {
    this.loadOptions();
    this.loadEngagementOptions();
  }

  ngAfterViewInit() {
    this.loadPagination(5);
  }

  onAjustEngagement(engagement: ReturnEngagementDto) {
    const en = this.engagements.find((c: ReturnEngagementDto) => c.engagementId === engagement.engagementId);
    en.subject = engagement.subject;
    en.startTime = engagement.startTime;
    en.duration = engagement.duration;
    en.scheduledDateUtc = engagement.scheduledDateUtc;
    en.communicationMode = engagement.communicationMode;
    en.location.address = engagement.location.address;
  }

  onAcceptEngagement(engagement: ReturnEngagementDto) {
    const en = this.engagements.find((c: ReturnEngagementDto) => c.engagementId === engagement.engagementId);
    en.status = engagement.status;
    this.numberOfPendingEngagements = this.numberOfPendingEngagements - 1;
    this.numberOfCompletedEngagements = this.numberOfCompletedEngagements + 1;
  }

  IsEngagementPending(engagement: ReturnEngagementDto) {
    if (engagement.status === 1) {
      return true;
    }
    return false;
  }

  downloadReport(engagement: ReturnEngagementDto) {
    window.open(engagement.engagementReport);
  }

  IsEngagementAccepted(engagement: ReturnEngagementDto) {
    if (engagement.status === 2) {
      return true;
    }
    return false;
  }

  IsEngagementComplete(engagement: ReturnEngagementDto) {
    if (engagement.status === 3) {
      return true;
    }
    return false;
  }

  onNextPageClick(data: any) {
    this.firstItemIndex = data.first;
    this.lastItemIndex = data.last;
  }

  onPreviousPageClick(data: any) {
    this.firstItemIndex = data.first;
    this.lastItemIndex = data.last;
  }

  filterEngagement(engagmentEntry: any) {
    const prev = this.tableService.getDataSource();
    const statusCode = parseInt(engagmentEntry, 10);
    if (statusCode === -1) {
      this.tableService.setDataSource(this.previous);
      this.engagements = this.tableService.getDataSource();
    } else {
      this.engagements = prev.filter((en: ReturnEngagementDto) => en.status === statusCode);
      this.tableService.setDataSource(prev);
    }

  }

  searchItems() {
    const prev = this.tableService.getDataSource();

    if (!this.searchText) {
      this.tableService.setDataSource(this.previous);
      this.engagements = this.tableService.getDataSource();
    }

    if (this.searchText) {
      // tslint:disable-next-line:max-line-length
      // this.mentees = [];
      // tslint:disable-next-line:max-line-length
      this.engagements = this.tableService.getDataSource().filter((engagementModel: ReturnEngagementDto) => engagementModel.mentee.user.firstName.indexOf(this.searchText) !== -1
       || engagementModel.mentee.user.lastName.indexOf(this.searchText) !== -1 );
      this.tableService.setDataSource(prev);
    }

    this.mdbPagination.calculateFirstItemIndex();
    this.mdbPagination.calculateLastItemIndex();

    this.tableService.searchDataObservable(this.searchText).subscribe((data: any) => {
      if (data.length === 0) {
        this.firstItemIndex = 0;
      }
    });
  }
}

import { MentorEngagementTaskComponent } from './mentor-engagement-task/mentor-engagement-task.component';
import { CreateengagmentComponent } from './createengagment/createengagment.component';
import { MentorEngagementComponent } from './mentorEngagement/mentorEngagement.component';
import { MenteeEngagementComponent } from './menteeEngagement/menteeEngagement.component';
import { PhotoEditorComponent } from './photo-editor/photo-editor.component';
import { MentorComponent } from './mentor/mentor.component';
import { MenteeComponent } from './mentee/mentee.component';
import { AuthGuard } from './../routeGuards/auth.guard';
import { ProfileComponent } from './profile/profile.component';
import { Routes } from '@angular/router';
import { UsersComponent } from './users.component';
import { MenteeViewComponent } from './mentee-view/mentee-view.component';
import { MentorViewComponent } from './mentor-view/mentor-view.component';
import { MenteeEngagemenntTaskComponent } from './mentee-engagemennt-task/mentee-engagemennt-task.component';


export const UserRoutes: Routes = [
    {path: '', redirectTo: 'users', pathMatch: 'full'},
    {path: 'users', component: UsersComponent,
     runGuardsAndResolvers: 'always',
     canActivate: [AuthGuard],
     children: [
         {path: 'profile', component: ProfileComponent},
         {path: 'mentee', component: MenteeComponent},
         {path: 'mentor', component: MentorComponent},
         {path: 'photoupload', component: PhotoEditorComponent},
         {path: 'menteeview', component: MenteeViewComponent},
         {path: 'mentorview', component: MentorViewComponent},
         {path: 'menteeengagement', component: MenteeEngagementComponent},
         {path: 'mentorengagement', component: MentorEngagementComponent},
         {path: 'createengagement', component: CreateengagmentComponent},
         {path: 'menteeengagementtask', component: MenteeEngagemenntTaskComponent},
         {path: 'mentorengagementtask', component: MentorEngagementTaskComponent}
     ]
    }
];

import { Mentor } from './../../models/Mentor/mentor';
import { SharedService } from 'src/app/services/shared.service';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { MentorService } from 'src/app/services/mentor.service';
import { compare } from 'fast-json-patch';

@Component({
  selector: 'app-mentor',
  templateUrl: './mentor.component.html',
  styleUrls: ['./mentor.component.scss']
})
export class MentorComponent implements OnInit {
  mentorFormGroup: FormGroup;
  editMode = false;
  mentor: Mentor;
  constructor(private mentorService: MentorService, private formbuilder: FormBuilder,
    private toatr: ToastrService, private sharedService: SharedService) {
    this.mentorFormGroup = this.createMentorFormGroup(formbuilder);
    this.getMentor();
  }
  onSubmit() {
    this.HasMentor();
    if (!this.editMode) {
      if (this.mentorFormGroup.valid) {
        const createMenteeRequest = Object.assign({}, this.mentorFormGroup.value);
        this.updateMentor(createMenteeRequest);
        this.mentorService.createMentor(this.mentor).subscribe((mentordto: Mentor) => {
           this.mentor = mentordto;
           this.mentorService.SetMentor(this.mentor);
           this.toatr.success('your mentor profile was successfully created');
        }, error => {
          this.mentor = null;
          this.toatr.error('Your mentor information was not created. Kindly try again later');
        });
      }
    } else {
      const editMentorRequest = Object.assign({}, this.mentorFormGroup.value);
      const oldMentor = Object.assign({}, this.mentor);
      this.updateMentor(editMentorRequest);
      const patch = compare(oldMentor, this.mentor);
      this.mentorService.updatementor(this.sharedService.userId, patch ).subscribe(() => {
        this.mentorService.SetMentor(this.mentor);
        this.toatr.success('Your mentor profile was successfully updated');
      }, error => {
        this.updateMentor(oldMentor);
        this.mentorFormGroup.patchValue(oldMentor);
        this.toatr.error('You mentor profile was not updated.\nKindly try again');
      });
    }
  }
  updateMentor(mentor: Mentor) {
    if (!this.mentor) {
      this.mentor = new Mentor();
    }
    this.mentor.country = mentor.country;
    this.mentor.occupation = mentor.occupation;
    this.mentor.state = mentor.state;
    this.mentor.userId = this.sharedService.userId;
  }
  HasMentor() {
    if (this.mentor) {
      this.editMode = true;
    }
  }
  getMentor() {
    this.mentorService.getMentor(this.sharedService.userId).subscribe((mentordto: Mentor) => {
      this.mentor = mentordto;
      this.mentorService.SetMentor(this.mentor);
      this.mentorFormGroup.patchValue(this.mentor);
    }, error => {
      this.toatr.error(error);
    });
  }
  createMentorFormGroup(formbuilder: FormBuilder) {
     return formbuilder.group({
        occupation: ['', Validators.required],
        state: ['', Validators.required],
        country: ['', Validators.required]
     });
  }
  ngOnInit() {
  }

}

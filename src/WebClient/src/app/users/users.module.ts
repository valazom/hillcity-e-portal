import { EngagementTaskCompleteComponent } from './engagement-task-complete/engagement-task-complete.component';
import { MentorEngagementTaskComponent } from './mentor-engagement-task/mentor-engagement-task.component';
import { MenteeEngagemenntTaskComponent } from './mentee-engagemennt-task/mentee-engagemennt-task.component';
import { CreateengagmentComponent } from './createengagment/createengagment.component';
import { MentorEngagementComponent } from './mentorEngagement/mentorEngagement.component';
import { MenteeEngagementComponent } from './menteeEngagement/menteeEngagement.component';
import { MentorViewComponent } from './mentor-view/mentor-view.component';
import { MenteeViewComponent } from './mentee-view/mentee-view.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MenteeComponent } from './mentee/mentee.component';
import { MentorComponent } from './mentor/mentor.component';
import { ProfileComponent } from './profile/profile.component';

import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './../services/auth.service';
import { NgModule, OnInit } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { UsersComponent } from './users.component';
import { RouterModule, Router } from '@angular/router';
import { UserRoutes } from './routes';
import { MDBBootstrapModulesPro } from 'ng-uikit-pro-standard';
import { JwtHelperService } from '@auth0/angular-jwt';
import { PhotoEditorComponent } from './photo-editor/photo-editor.component';
import { MenteedetailsComponent } from './menteedetails/menteedetails.component';
import { EngagementReportComponent } from './engagement-report/engagement-report.component';
import { EngagementAdjustComponent } from './engagement-adjust/engagement-adjust.component';
import { EngagementAcceptComponent } from './engagement-accept/engagement-accept.component';
import { EngagementTaskCreateComponent } from './engagement-task-create/engagement-task-create.component';
import { EngagementCompleteComponent } from './engagement-complete/engagement-complete.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(UserRoutes),
    MDBBootstrapModulesPro.forRoot(),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    UsersComponent,
    ProfileComponent,
    MentorComponent,
    MenteeComponent,
    PhotoEditorComponent,
    MenteeViewComponent,
    MentorViewComponent,
    MenteedetailsComponent,
    MenteeEngagementComponent,
    MentorEngagementComponent,
    CreateengagmentComponent,
    EngagementReportComponent,
    EngagementAdjustComponent,
    EngagementAcceptComponent,
    EngagementTaskCreateComponent,
    EngagementCompleteComponent,
    MenteeEngagemenntTaskComponent,
    MentorEngagementTaskComponent,
    EngagementTaskCompleteComponent
  ],
  providers: [
    AuthService,
    DatePipe
  ]

})
export class UsersModule {

}

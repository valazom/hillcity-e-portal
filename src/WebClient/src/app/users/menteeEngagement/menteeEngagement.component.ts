import { AssignedMentor } from './../../models/Mentor/assignedmentor';
import { MenteeEngagementDto } from './../../models/Engagement/menteeengmentdto';
import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, ChangeDetectorRef, HostListener } from '@angular/core';
import { MdbTableDirective, MdbTablePaginationComponent, MdbTableService } from 'ng-uikit-pro-standard';
import { MenteeService } from 'src/app/services/mentee.service';
import { SharedService } from 'src/app/services/shared.service';
import { EngagementDto } from 'src/app/models/Engagement/engagementdto';
import { ReturnEngagementDto } from 'src/app/models/Engagement/returnengagementdto';

@Component({
  selector: 'app-menteeengagement',
  templateUrl: './menteeEngagement.component.html',
  styleUrls: ['./menteeEngagement.component.scss']
})
export class MenteeEngagementComponent implements OnInit, AfterViewInit {
  @ViewChild(MdbTableDirective) mdbTable: MdbTableDirective;
  @ViewChild(MdbTablePaginationComponent) mdbPagination: MdbTablePaginationComponent;
  @ViewChild('row') row: ElementRef;
  optionsSelect: Array<any>;
  optionsEnselect: Array<any>;
  engagements: ReturnEngagementDto[] = [];
  numberOfPendingEngagements: number;
  numberOfCompletedEngagements: number;
  numberOfAssignedMentor = 0;
  assignedMentor: AssignedMentor;

  headElements = ['ID', 'SUBMITTED', 'SCHEDULED', 'MODE', 'TIME', 'STATUS', 'REPORT'];

  selectedValue = 1;
  enSelectedValue = -1;
  searchText: string;
  previous: string;
  firstItemIndex;
  lastItemIndex;
  IsSpinner = true;
  constructor(private menteeService: MenteeService, private tableService: MdbTableService,
    private cdRef: ChangeDetectorRef, private sharedService: SharedService) {
      this.getMenteeEngagements();
    }

  loadOptions() {
    this.optionsSelect = [
      { value: '5', label: 'show 5 entries' },
      { value: '10', label: 'show 10 entries' },
      { value: '20', label: 'show 20 entries' },
      { value: '50', label: 'show 50 entries' },
      ];
  }
  loadEngagementOptions() {
    this.optionsEnselect = [
      { value: '-1', label: 'All' },
      { value: '1', label: 'Pending' },
      { value: '2', label: 'Accepted' },
      { value: '3', label: 'Completed' },
      ];
  }

  loadPagination(entries: any) {
    this.mdbPagination.setMaxVisibleItemsNumberTo(entries);
    this.firstItemIndex = this.mdbPagination.firstItemIndex;
    this.lastItemIndex = this.mdbPagination.lastItemIndex;

    this.mdbPagination.calculateFirstItemIndex();
    this.mdbPagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  loadTableSource(mentees: EngagementDto[]) {
    this.tableService.setDataSource(mentees);
    this.IsSpinner = false;
    this.engagements = this.tableService.getDataSource();
    this.previous = this.tableService.getDataSource();
  }
  getMenteeEngagements() {
    this.menteeService.getMenteeEngagements(this.sharedService.userId).subscribe((menteeEngagment: MenteeEngagementDto) => {
       if (!menteeEngagment.engagements) {
          menteeEngagment.engagements = [];
       }
       this.loadTableSource(menteeEngagment.engagements);
       this.numberOfPendingEngagements = menteeEngagment.numberOfPendingEngagements;
       this.numberOfCompletedEngagements = menteeEngagment.numberOfCompletedEngagements;
    });
  }

  onMentorAssigned(mentor: AssignedMentor) {
    this.assignedMentor = mentor;
    if (this.assignedMentor) {
      this.numberOfAssignedMentor = 1;
    }
  }

  hasEngagementReport(engagement: ReturnEngagementDto) {
    if (engagement.engagementReport && engagement.engagementReport !== '') {
      return true;
    }
    return false;
  }

  onEngagementUploaded(engagement: ReturnEngagementDto) {
    const eng = this.engagements.find((en: ReturnEngagementDto) => en.engagementId === engagement.engagementId);
    eng.engagementReport = engagement.engagementReport;
  }

  IsEngagementAccepted(engagement: ReturnEngagementDto) {
    if (engagement.status === 2 && engagement.hasEngagementTask) {
      return true;
    }
    return false;
  }

  IsEngagementComplete(engagement: ReturnEngagementDto) {
    if (engagement.status === 3) {
      return true;
    }
    return false;
  }

  translateEnagementStatus(statusCode: number) {
    switch (statusCode) {
      case 1:
        return 'Pending';
      case 2:
        return 'Accepted';
      case 3:
        return 'Completed';
    }
  }

  translateCommunicationMode(mode: number) {
    switch (mode) {
      case 1:
        return 'Face to Face';
      case 2:
        return 'WhatsApp Video Call';
      case 3:
        return 'WhatsApp Voice Call';
      case 4:
        return 'Skype Video Call';
      case 5:
        return 'Skype Voice Call';
      case 6:
        return 'Phone Call';
    }
  }

  downloadReport(engagement: ReturnEngagementDto) {
    window.open(engagement.engagementReport);
  }

  engagementCreated(engagement) {
    const engagementData = this.tableService.getDataSource();
    engagementData.unshift(engagement);
    this.numberOfPendingEngagements = this.numberOfPendingEngagements + 1;
    this.loadTableSource(engagementData);
  }

  filterEngagement(engagmentEntry: any) {
    const prev = this.tableService.getDataSource();
    const statusCode = parseInt(engagmentEntry, 10);
    if (statusCode === -1) {
      this.tableService.setDataSource(this.previous);
      this.engagements = this.tableService.getDataSource();
    } else {
      this.engagements = prev.filter((en: ReturnEngagementDto) => en.status === statusCode);
      this.tableService.setDataSource(prev);
    }

  }


  ngOnInit() {
    this.loadOptions();
    this.loadEngagementOptions();
  }

  ngAfterViewInit() {
    this.loadPagination(5);
  }

  onNextPageClick(data: any) {
    this.firstItemIndex = data.first;
    this.lastItemIndex = data.last;
  }

  onPreviousPageClick(data: any) {
    this.firstItemIndex = data.first;
    this.lastItemIndex = data.last;
  }

}

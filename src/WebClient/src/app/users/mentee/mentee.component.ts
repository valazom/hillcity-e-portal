import { MenteeService } from './../../services/mentee.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MenteeDto } from '../../models/mentee/mentee';
import { Component, OnInit, EventEmitter } from '@angular/core';
import { IMyOptions, UploadFile, UploadInput, humanizeBytes, UploadOutput } from 'ng-uikit-pro-standard';
import { SharedService } from 'src/app/services/shared.service';
import { compare } from 'fast-json-patch';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-mentee',
  templateUrl: './mentee.component.html',
  styleUrls: ['./mentee.component.scss']
})
export class MenteeComponent implements OnInit {
  editMode = false;
  mentee: MenteeDto;
  menteeFormGroup: FormGroup;
  menteeDisabled = false;
  toolTipMessage: string;
  public myDatePickerOptions: IMyOptions = {
    // Your options
  };
  constructor(private menteeService: MenteeService, private formbuilder: FormBuilder,
    private toastr: ToastrService,
    private sharedService: SharedService) {
    this.menteeFormGroup = this.createMenteeFormBuilder(formbuilder);
    this.getMentee();
  }
  onSubmit() {
    if (this.menteeFormGroup.valid) {
      if (!this.editMode) {
        const createMenteeRequest = Object.assign({}, this.menteeFormGroup.value);
        this.updateMentee(createMenteeRequest);
        this.menteeService.creatementee(this.mentee).subscribe((menteedto: MenteeDto) => {
          this.mentee = menteedto;
          this.menteeService.setMentee(this.mentee);
          this.toastr.success('Your mentee information successfully created');
        }, error => {
          this. mentee = null;
          this.toastr.error('We could not create your mentee information provided.\n Kindly try again');
        });
      } else {
        const editMenteeRequest = Object.assign({}, this.menteeFormGroup.value);
        const oldMentee = Object.assign({}, this.mentee);
        this.updateMentee(editMenteeRequest);
        const patch = compare(oldMentee, this.mentee);
        this.menteeService.updateMentee(this.sharedService.userId, patch).subscribe(() => {
          this.menteeService.setMentee(this.mentee);
          this.toastr.success('your mentee information was successfully updated');
        }, error => {
           this.updateMentee(oldMentee);
           this.menteeFormGroup.patchValue(oldMentee);
           this.toastr.error('Your mentee profile was not updated');
        });
      }
    }
  }
  updateMentee(model: MenteeDto) {
    if (!this.mentee) {
      this.mentee = new MenteeDto();
    }
    this.mentee.university = model.university;
    this.mentee.course = model.course;
    this.mentee.dateOfAdmission = model.dateOfAdmission;
    this.mentee.dateOfGraduation = model.dateOfGraduation;
    this.mentee.userId = this.sharedService.userId;
  }
  HasMentee() {
    if (this.mentee) {
      this.editMode = true;
      this.toolTipMessage = 'cannot edit graduation date';
      return this.editMode;
    }
  }
  getMentee() {
    this.menteeService.getMentee(this.sharedService.userId).subscribe((menteedto: MenteeDto) => {
      this.mentee = menteedto;
      this.menteeService.setMentee(this.mentee);
      this.menteeFormGroup.patchValue(this.mentee);
    }, error => {
      this.toastr.error(error);
    });
  }
  createMenteeFormBuilder(formbuilder: FormBuilder) {
    return formbuilder.group({
      university: ['', Validators.required],
      course: ['', Validators.required],
      dateOfAdmission: ['', Validators.required],
      dateOfGraduation: ['', Validators.required],
    });
  }
  ngOnInit() {
  }

}

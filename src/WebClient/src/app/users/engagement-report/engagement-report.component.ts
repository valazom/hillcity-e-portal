import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { UploadFile, UploadInput, humanizeBytes, UploadOutput } from 'ng-uikit-pro-standard';
import { SharedService } from 'src/app/services/shared.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { ReturnEngagementDto } from 'src/app/models/Engagement/returnengagementdto';

@Component({
  selector: 'app-engagement-report',
  templateUrl: './engagement-report.component.html',
  styleUrls: ['./engagement-report.component.scss']
})
export class EngagementReportComponent implements OnInit {

  Url: string;
  formData: FormData;
  files: UploadFile[];
  uploadInput: EventEmitter<UploadInput>;
  humanizeBytes: Function;
  dragOver: boolean;
  @Input() engagementInput: ReturnEngagementDto;
  @Output() engagementOutput = new EventEmitter();
  engagementUploaded: ReturnEngagementDto;

  constructor(private toastrService: ToastrService) {
    this.files = [];
    this.uploadInput = new EventEmitter<UploadInput>();
    this.humanizeBytes = humanizeBytes;
  }

  showFiles() {
    let files = '';
    for (let i = 0; i < this.files.length; i ++) {
      files = this.files[i].name;
    }
    return files;
  }

  startUpload(): void {
    if (this.files.length > 0) {
      this.Url = environment.appUrl + 'engagement/' + this.engagementInput.engagementId + '/uploadreport';
      const event: UploadInput = {
        type: 'uploadFile',
        url: this.Url,
        method: 'PUT',
        file: this.files[this.files.length - 1],
        data: { foo: 'bar' },
        headers: {'Authorization' : 'Bearer ' + localStorage.getItem('token') }
      };
      this.files = [];
      this.uploadInput.emit(event);
    } else {
      this.toastrService.error('No file selected');
    }
  }
  EngagementUploaded() {
    this.engagementOutput.emit(this.engagementUploaded);
  }
  cancelUpload(id: string): void {
    this.uploadInput.emit({ type: 'cancel', id: id });
  }

  onUploadOutput(output: UploadOutput | any): void {
    if (output.type === 'allAddedToQueue') {
    } else if (output.type === 'addedToQueue') {
      this.files.push(output.file); // add file to array when added
    } else if (output.type === 'uploading') {
      // update current data in files array for uploading file
      const index = this.files.findIndex(file => file.id === output.file.id);
      this.files[index] = output.file;
    } else if (output.type === 'removed') {
      // remove file from array when removed
      this.files = this.files.filter((file: UploadFile) => file !== output.file);
    } else if (output.type === 'done') {
      const statusCode = output.file.responseStatus;
      if (statusCode === 200) {
         this.engagementUploaded = output.file.response as ReturnEngagementDto;
         if (this.engagementUploaded) {
           this.EngagementUploaded();
           this.toastrService.success('Your engagement report was successfully uploaded');
         }
      } else if (statusCode === 400) {
         this.toastrService.error('Invalid file format');
      } else {
        this.toastrService.error('Your engagement report was not uploaded');
      }
    } else if (output.type === 'dragOver') {
      this.dragOver = true;
    } else if (output.type === 'dragOut') {
    } else if (output.type === 'drop') {
      this.dragOver = false;
    }
    this.showFiles();
  }
  removeFile(id: string): void {
    this.uploadInput.emit({ type: 'remove', id: id });
  }

  removeFirstFileFromQueue() {
    if (this.files.length > 0) {
      this.removeFile(this.files[0].id);
    } else {
      this.toastrService.error('No file selected');
    }
  }
  ngOnInit() {
  }

}

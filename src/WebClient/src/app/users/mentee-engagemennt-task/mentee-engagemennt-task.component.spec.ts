/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { MenteeEngagemenntTaskComponent } from './mentee-engagemennt-task.component';

describe('MenteeEngagemenntTaskComponent', () => {
  let component: MenteeEngagemenntTaskComponent;
  let fixture: ComponentFixture<MenteeEngagemenntTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenteeEngagemenntTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenteeEngagemenntTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

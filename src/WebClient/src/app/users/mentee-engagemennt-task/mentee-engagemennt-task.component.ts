import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { MdbTableService, MdbTableDirective, MdbTablePaginationComponent } from 'ng-uikit-pro-standard';
import { SharedService } from 'src/app/services/shared.service';
import { ReturnEngagementTaskDto } from 'src/app/models/EngagementTask/returnengagementtaskdto';
import { ToastrService } from 'ngx-toastr';
import { MenteeService } from 'src/app/services/mentee.service';

@Component({
  selector: 'app-mentee-engagemennt-task',
  templateUrl: './mentee-engagemennt-task.component.html',
  styleUrls: ['./mentee-engagemennt-task.component.scss']
})
export class MenteeEngagemenntTaskComponent implements OnInit, AfterViewInit {
  @ViewChild(MdbTableDirective) mdbTable: MdbTableDirective;
  @ViewChild(MdbTablePaginationComponent) mdbPagination: MdbTablePaginationComponent;
  @ViewChild('row') row: ElementRef;
  optionsSelect: Array<any>;
  headElements = ['EngagementID', 'Created by', 'Details', 'Category', 'Start Date', 'Expiry Date', 'Status'];
  engagementTasks: ReturnEngagementTaskDto[] = [];
  searchText: string;
  previous: string;
  firstItemIndex;
  lastItemIndex;
  IsSpinner = true;
  selectedValue = 1;
  constructor(private tableService: MdbTableService,
    private cdRef: ChangeDetectorRef, private sharedService: SharedService,
    private toastrService: ToastrService,
    private menteeService: MenteeService) {
      this.getEngagementTask();
    }

  loadTableSource(engagementtasks: ReturnEngagementTaskDto[]) {
    this.tableService.setDataSource(engagementtasks);
    this.IsSpinner = false;
    this.engagementTasks = this.tableService.getDataSource();
    this.previous = this.tableService.getDataSource();
  }

  getEngagementTask() {
    this.menteeService.getMenteeEngagementTasks(this.sharedService.userId).subscribe((engagementtasks: ReturnEngagementTaskDto[]) => {
      this.IsSpinner = false;
      if (!engagementtasks) {
        engagementtasks = [];
      }
      this.loadTableSource(engagementtasks);
    });
  }

  loadPagination(entries: any) {
    this.mdbPagination.setMaxVisibleItemsNumberTo(entries);
    this.firstItemIndex = this.mdbPagination.firstItemIndex;
    this.lastItemIndex = this.mdbPagination.lastItemIndex;

    this.mdbPagination.calculateFirstItemIndex();
    this.mdbPagination.calculateLastItemIndex();
    this.cdRef.detectChanges();
  }

  loadOptions() {
    this.optionsSelect = [
      { value: '5', label: 'show 5 entries' },
      { value: '10', label: 'show 10 entries' },
      { value: '20', label: 'show 20 entries' },
      { value: '50', label: 'show 50 entries' },
      ];
  }

  translateEnagementStatus(statusCode: number) {
    switch (statusCode) {
      case 1:
        return 'Pending';
      case 2:
        return 'OverDue';
      case 3:
        return 'Completed';
    }
  }

  translateTaskCategory(category: number) {
    switch (category) {
      case 1:
        return 'Principles And Values';
      case 2:
        return 'Academic Review';
      case 3:
        return 'General Well Being Of Mentee';
      case 4:
        return 'Mentee Life Goals And Complete';
      case 5:
        return 'Any Other Business';
    }
  }

  ngOnInit() {
    this.loadOptions();
  }

  ngAfterViewInit() {
    this.loadPagination(5);
  }

  onNextPageClick(data: any) {
    this.firstItemIndex = data.first;
    this.lastItemIndex = data.last;
  }

  onPreviousPageClick(data: any) {
    this.firstItemIndex = data.first;
    this.lastItemIndex = data.last;
  }

}

import { AssignedMentor } from './../../models/Mentor/assignedmentor';
import { SharedService } from './../../services/shared.service';
import { MenteeService } from './../../services/mentee.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { IMyOptions, ToastService } from 'ng-uikit-pro-standard';
import { CreateEngagementDto } from 'src/app/models/Engagement/createengagementdto';
import { EngagementBaseDto } from 'src/app/models/Engagement/engagementbasedto';
import { LoctionDto } from 'src/app/models/user/locationdto';
import { Mentor } from 'src/app/models/Mentor/mentor';
import { MentorViewDto } from 'src/app/models/Mentor/mentorviewdto';
import { EngagementService } from 'src/app/services/engagement.service';
import { ReturnEngagementDto } from 'src/app/models/Engagement/returnengagementdto';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-createengagment',
  templateUrl: './createengagment.component.html',
  styleUrls: ['./createengagment.component.scss']
})
export class CreateengagmentComponent implements OnInit {

  public myDatePickerOptions: IMyOptions = {
    dayLabels: { su: 'Sun', mo: 'Mon', tu: 'Tue', we: 'Wed', th: 'Thu', fr: 'Fri', sa: 'Sat' },
    dayLabelsFull: {
      su: 'Sunday', mo: 'Monday', tu: 'Tuesday', we: 'Wednesday', th: 'Thursday', fr: 'Friday', sa:
        'Saturday'
    },
    monthLabels: {
      1: 'Jan', 2: 'Feb', 3: 'Mar', 4: 'Apr', 5: 'May', 6: 'Jun', 7: 'Jul', 8: 'Aug', 9: 'Sep', 10:
        'Oct', 11: 'Nov', 12: 'Dec'
    },
    monthLabelsFull: {
      1: 'January', 2: 'February', 3: 'March', 4: 'April', 5: 'May', 6: 'June', 7: 'July', 8:
        'August', 9: 'September', 10: 'October', 11: 'November', 12: 'December'
    },
    firstDayOfWeek: 'mo',

    // Enable dates (when disabled)
    // Year limits
    minYear: 1000,
    maxYear: 9999,

    // Show Today button
    showTodayBtn: true,

    // Show Clear date button
    showClearDateBtn: true,

    markCurrentDay: true,
    markDates: [{ dates: [{ year: 2018, month: 10, day: 20 }], color: '#303030' }],
    markWeekends: undefined,
    disableHeaderButtons: false,
    showWeekNumbers: false,
  };

  optionSelect: Array<any>;
  optionCommMode: Array<any>;
  engagementFormGroup: FormGroup;
  engagement: CreateEngagementDto;
  assignedMentor: AssignedMentor;
  returnedEngagement: ReturnEngagementDto;
  @Output() createdEngagement = new EventEmitter();
  // tslint:disable-next-line:no-output-on-prefix
  @Output() onGetAssignedMentor = new EventEmitter();

  constructor(private formbuilder: FormBuilder, private menteeservice: MenteeService,
    private sharedservice: SharedService, private engagementService: EngagementService,
    private toastrService: ToastrService,
    private datepipe: DatePipe) {
    this.engagementFormGroup = this.createEngagmentFormGroup(formbuilder);
    this.getAssignedMentor();
  }

  engagementCreated() {
    this.createdEngagement.emit(this.returnedEngagement);
  }

  mentorAssigned() {
    this.onGetAssignedMentor.emit(this.assignedMentor);
  }

  onSubmit() {
    this.getAssignedMentor();
    if (this.engagementFormGroup.valid) {
      const createEngagmentRequest = Object.assign({}, this.engagementFormGroup.value);
      this.updateEngagementModel(createEngagmentRequest);
      if (this.assignedMentor) {
        this.engagement.mentorId = this.assignedMentor.user.userId;
      } else {
        // tslint:disable-next-line:max-line-length
        this.toastrService.error('You cannot create engagement as you have not assigned a mentor. Contact Admin at vision@hillcityfoundation.org');
        return;
      }

      // call service at this stage;
      this.engagementService.createEngagement(this.engagement).subscribe((returnengagment: ReturnEngagementDto) => {
        this.returnedEngagement = returnengagment;
        this.engagementCreated();
        this.engagementFormGroup.reset();
        // this.engagementFormGroup.setValue({"scheduleDateUtc:})
        this.toastrService.success('Engagement successfully created');
      }, error => {
        this.toastrService.error(error);
      });
    }
  }

  updateEngagementModel(model: any) {
    if (!this.engagement) {
      this.engagement = new CreateEngagementDto();
      this.engagement.location = new LoctionDto();
    }
    const date = new Date();
    const datenow = Date.now();
    const mode = this.optionCommMode
    .find((option: any) => model.communicationMode === option.value);
    this.engagement.subject = 'EN.' + mode.label + '.' + datenow;
    this.engagement.duration = model.duration;
    this.engagement.startTime = model.startTime;
    this.engagement.dateCreatedUtc = this.datepipe.transform(date, 'yyyy-MM-dd').toString();
    this.engagement.scheduledDateUtc = model.scheduleDateUtc;
    this.engagement.communicationMode = model.communicationMode;
    this.engagement.menteeId = this.sharedservice.userId;
    this.engagement.status = 1; // status must be pending of new engagement.
    if (model.location) {
      this.engagement.location.address = model.location.address;
    }
  }

  createEngagmentFormGroup(formbuilder: FormBuilder) {
    return formbuilder.group({
      startTime: ['', Validators.required],
      duration: ['', Validators.required],
      scheduleDateUtc: ['', Validators.required],
      communicationMode: ['', Validators.required],
      location: formbuilder.group({
        address: ''
      })
    });
  }

  getAssignedMentor() {
    this.menteeservice.getAssignedMentor(this.sharedservice.userId).subscribe((mentor: MentorViewDto) => {
      this.assignedMentor = mentor.assignedMentor;
      this.mentorAssigned();
    });
  }

  loadOptions() {
    this.optionSelect = [
      { value: '1:00', label: '1hr' },
      { value: '2:00', label: '2hrs' },
      { value: '3:00', label: '3hrs' },
      { value: '4:00', label: '4hrs' },
    ];

    this.optionCommMode = [
      { value: '1', label: 'Face to Face' },
      { value: '2', label: 'WhatsApp Video Call' },
      { value: '3', label: 'WhatsApp Voice Call' },
      { value: '4', label: 'Skype Video Call' },
      { value: '5', label: 'Skype Voice Call' },
      { value: '6', label: 'Phone Call' },
    ];
  }
  ngOnInit() {
    this.loadOptions();
  }

// tslint:disable-next-line:eofline
}
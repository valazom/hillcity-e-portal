import { ConfirmregistrationComponent } from './confirmregistration/confirmregistration.component';
import { ForgetpasswordComponent } from './forgetpassword/forgetpassword.component';
import { Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';

export const accountRoutes: Routes = [
    {path: 'login', component: LoginComponent},
    {path: 'register', component: RegisterComponent},
    {path: 'forgetpassword', component: ForgetpasswordComponent},
    {path: 'confirmreg', component: ConfirmregistrationComponent},
    {path: 'resetpassword', component: ResetpasswordComponent}, // default  route
    {path: '', redirectTo: 'login'}
];

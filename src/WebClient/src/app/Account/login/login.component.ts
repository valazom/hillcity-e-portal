import { UserService } from './../../services/user.service';
import { AuthService } from './../../services/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserLoginDto } from 'src/app/models/user/userlogindto';
import { UserDto } from 'src/app/models/user/userdto';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  model: UserLoginDto;
  IsError = false;
  ErrorMessage: any;
  constructor(private authService: AuthService, private router: Router, private formbuilder: FormBuilder,
    private userService: UserService) {
    this.loginForm = this.createLoginFormBuilder(formbuilder);
  }
  onSumbit() {
    if (this.loginForm.valid) {
      this.model = Object.assign({}, this.loginForm.value);
      this.authService.login(this.model).subscribe(() => {
        this.router.navigate(['/user/users/profile']);
        this.userService.SetCurrentUser(null);
      }, error => {
        this.IsError = true;
        if (error === 'Unauthorized') {
          this.ErrorMessage = 'Invalid username or password specified.';
        } else {
          this.ErrorMessage = error;
        }
      });
    } else {
      this.IsError = true;
      this.ErrorMessage = 'Kindly fill in details correctly';
    }
  }
  createLoginFormBuilder(formbuilder: FormBuilder) {
    return formbuilder.group({
      Username: ['', Validators.required],
      // tslint:disable-next-line:max-line-length
      Password: ['', Validators.required]
    });
  }
  closeAlert() {
    this.IsError = false;
  }
  ngOnInit() {
  }

}

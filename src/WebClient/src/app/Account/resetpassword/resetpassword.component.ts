import { AuthService } from './../../services/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserResetPasswordDto } from 'src/app/models/user/userresetpassworddto';
import { routerNgProbeToken } from '@angular/router/src/router_module';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.scss']
})
export class ResetpasswordComponent implements OnInit {

  encrytedUsername: string;
  paramSub: any;
  resetPasswordForm: FormGroup;
  model: UserResetPasswordDto;
  ChangeText = 'Change';
  IsError = false;
  IsSuccess = false;
  ErrorMessage = '';
  constructor(private route: ActivatedRoute, private formbuilder: FormBuilder,
     private authService: AuthService, private router: Router) {
      this.resetPasswordForm = this.createResetPasswordForm(formbuilder);
    }
  onSubmit() {
    if (this.ChangeText === 'Continue') {
      this.router.navigate(['account/login']);
    }
    if (this.resetPasswordForm.valid) {
      this.model = Object.assign({}, this.resetPasswordForm.value);
      this.model.UsernameHash = this.encrytedUsername;
      // call the service
      this.authService.resetpassword(this.model).subscribe(() => {
        this.IsSuccess = true;
        this.ChangeText = 'Continue';
        this.resetPasswordForm.reset();
      }, error => {
        this.IsError = true;
        this.ErrorMessage = error;
      });
    }
  }
  createResetPasswordForm(formbuilder: FormBuilder) {
    return formbuilder.group({
      // tslint:disable-next-line:max-line-length
      Password: ['', Validators.compose([Validators.required, Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[-_#$^+=!*()@%&]).{8,15}$/)])]
    });
  }
  closeErrorAlert() {
    this.IsError = false;
  }
  closeSuccessAlert() {
    this.IsSuccess = false;
  }
  ngOnInit() {
    this.paramSub = this.route.queryParams.subscribe(param => {
      this.encrytedUsername = param['username'] || '';
    });
  }
  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy() {
    this.paramSub.unsubscribe();
  }

}

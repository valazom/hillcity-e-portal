import { AuthService } from './../../services/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { UserForgetPasswordDto } from 'src/app/models/user/userforgetpassworddto';
import { PlatformLocation } from '@angular/common';

@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.scss']
})
export class ForgetpasswordComponent implements OnInit {
  forgetpasswordForm: FormGroup;
  model: UserForgetPasswordDto;
  IsError = false;
  IsSuccess = false;
  ErrorMessage = '';
  SuccessMessage = '';
  resetpasswordLink = '/account/resetpassword';
  SendText = 'Send';
  @ViewChild('alert') alert: ElementRef;
  constructor(private router: Router, formbuilder: FormBuilder,
    private authService: AuthService, private platformlocation: PlatformLocation) {
    this.forgetpasswordForm = this.createForgetPasswordFormGroup(formbuilder);
   }
  ngOnInit() {
  }

  cancel() {
    this.router.navigate(['/account/login']);
  }
  onSend() {
    if (this.forgetpasswordForm.valid) {
      this.model = Object.assign({}, this.forgetpasswordForm.value);
      this.model.DomainName = (this.platformlocation as any).location.origin + this.resetpasswordLink;
      this.authService.forgetpassword(this.model).subscribe(() => {
          this.IsSuccess = true;
          this.SendText = 'Resend';
          this.SuccessMessage = 'Email has been successfully sent to ' + this.model.Email + ' with informaion to reset your password';
      }, error => {
        this.IsError = true;
        this.IsSuccess = false;
        this.ErrorMessage = error;
      });
    } else {
      this.IsError = true;
      this.IsSuccess = false;
      this.ErrorMessage = 'Enter a valid email address';
    }
  }
  reset() {
    this.forgetpasswordForm.reset();
  }
  createForgetPasswordFormGroup(formbuilder: FormBuilder) {
    return formbuilder.group({
       Email: ['', [Validators.required, Validators.email]]
    });
  }
  closeErrorAlert() {
    this.IsError = false;
  }
  closeSuccessAlert() {
    this.IsSuccess = false;
  }
}

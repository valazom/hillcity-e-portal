import { AuthService } from './../services/auth.service';
import { ConfirmregistrationComponent } from './confirmregistration/confirmregistration.component';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountComponent } from './Account.component';
import { accountRoutes } from './routes';
import { RouterModule } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { MDBBootstrapModulesPro } from 'ng-uikit-pro-standard';
import { MDBSpinningPreloader } from 'ng-uikit-pro-standard';
import { ForgetpasswordComponent } from './forgetpassword/forgetpassword.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ErrorInterceptorProvider } from '../services/error.interceptor';
import { HttpClientModule } from '@angular/common/http';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(accountRoutes),
    MDBBootstrapModulesPro.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  declarations: [
    AccountComponent,
    LoginComponent,
    RegisterComponent,
    ForgetpasswordComponent,
    ConfirmregistrationComponent,
    ResetpasswordComponent
  ],
  providers: [
    AuthService,
    ErrorInterceptorProvider
  ]
})
export class AccountModule { }

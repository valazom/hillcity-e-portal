import { AuthService } from './../../services/auth.service';
import { UserRegisterDto } from './../../models/user/userregisterdto';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NetworkConnection, ConnectionStatusEnum } from 'src/app/models/Network/networkconnection';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  optionsSelect: Array<any>;
  genderSelect: Array<any>;
  registerForm: FormGroup;
  @ViewChild('alert') alert: ElementRef;
  IsError = false;
  ErrorMessage = '';
  constructor(private formbuilder: FormBuilder, private authService: AuthService,
               private router: Router) {
    this.registerForm = this.createFormGroupWithBuilder(formbuilder);
   }

  ngOnInit() {
    this.optionsSelect = [
      {value: 'Mentee', label: 'Mentee'},
      {value: 'Mentor', label: 'Mentor'}
    ];
    this.genderSelect = [
      {value: 1, label: 'Male'},
      {value: 2, label: 'Female'}
    ];
    console.log(navigator.onLine);
  }

  register() {
    if (this.registerForm.valid) {
      const registerRequest: UserRegisterDto = Object.assign({}, this.registerForm.value);
      registerRequest.Location = Object.assign({}, registerRequest.Location);
      this.authService.register(registerRequest).subscribe(() => {
        this.authService.SetEmail(registerRequest.Email);
        this.router.navigate(['account/confirmreg']);
        console.log('registraion successful');
      }, error => {
        this.IsError = true;
        this.ErrorMessage = error;
        console.error(error);
      });
      console.log(this.registerForm.value);
    } else {
      this.IsError = true;
      this.ErrorMessage = 'Kindly fill in the form correctly';
    }
  }
  revert() {
    this.registerForm.reset();
  }
  createFormGroupWithBuilder(formBuilder: FormBuilder) {
    return formBuilder.group({

      Username: ['', Validators.compose([Validators.required, Validators.maxLength(200), Validators.minLength(1)])],
      Gender: ['Select Gender', Validators.required],
      Email: ['', [Validators.required, Validators.email]],
      RoleName: ['Select Role', Validators.required],
      PhoneNumber: ['', Validators.compose([Validators.required, Validators.pattern(/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/)])],
      // tslint:disable-next-line:max-line-length
      Password: ['', Validators.compose([Validators.required, Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[-_#$^+=!*()@%&]).{8,15}$/)])],
      Location: formBuilder.group({
        Address: ''
      })
    });
  }

  closeAlert() {
    this.IsError = false;
  }

}

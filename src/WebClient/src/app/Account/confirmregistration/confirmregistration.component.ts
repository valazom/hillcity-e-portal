import { Router } from '@angular/router';
import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-confirmregistration',
  templateUrl: './confirmregistration.component.html',
  styleUrls: ['./confirmregistration.component.scss']
})
export class ConfirmregistrationComponent implements OnInit {
  email: string;
  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit() {
    this.authService.currentMessage.subscribe(message => this.email = message);
  }
  continue() {
    this.router.navigate(['account/login']);
  }
}

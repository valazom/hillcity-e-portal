import { EngagementTaskDto } from './engagementTaskbasedto';
import { ReturnEngagementDto } from '../Engagement/returnengagementdto';

export class ReturnEngagementTaskDto extends EngagementTaskDto {
    engagementTaskId: number;
    engagement: ReturnEngagementDto;
}

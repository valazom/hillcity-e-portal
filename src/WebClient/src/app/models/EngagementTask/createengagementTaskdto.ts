import { EngagementTaskDto } from './engagementTaskbasedto';

export class CreateEngagementTaskDto extends EngagementTaskDto {
    engagementId: number;
}

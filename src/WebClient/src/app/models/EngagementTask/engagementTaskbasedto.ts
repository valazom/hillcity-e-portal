export class EngagementTaskDto {
    taskCategory: number;
    details: string;
    startDate: string;
    endDate: string;
    status: number;
}

import { LoctionDto } from '../user/locationdto';

export class EngagementBaseDto {
    subject: string;
    duration: string;
    startTime: string;
    dateCreatedUtc: string;
    scheduledDateUtc: string;
    engagementReport: string;
    communicationMode: number;
    status: number;
    hasEngagementTask: boolean;
    location: LoctionDto;
}

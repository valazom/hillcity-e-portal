import { EngagementBaseDto } from './engagementbasedto';

export class EngagementDto extends EngagementBaseDto {
    engagementId: number;
}

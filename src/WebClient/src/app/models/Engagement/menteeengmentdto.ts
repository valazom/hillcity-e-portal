import { EngagementBaseDto } from './engagementbasedto';
import { EngagementDto } from './engagementdto';
import { ReturnEngagementDto } from './returnengagementdto';

export class MenteeEngagementDto {
    numberOfPendingEngagements: number;
    numberOfCompletedEngagements: number;
    engagements: ReturnEngagementDto[];
}

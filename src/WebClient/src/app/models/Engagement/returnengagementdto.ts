import { AssignedMentor } from './../Mentor/assignedmentor';
import { AssignedMentee } from './../mentee/assignedMentee';
import { EngagementBaseDto } from './engagementbasedto';

export class ReturnEngagementDto extends EngagementBaseDto {
    engagementId: number;
    mentee: AssignedMentee;
    mentor: AssignedMentor;
}

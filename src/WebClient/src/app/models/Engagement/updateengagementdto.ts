import { EngagementBaseDto } from './engagementbasedto';

export class UpdateEngagementDto extends EngagementBaseDto {
    engagementId: number;
    mentorId: number;
    menteeId: number;
}

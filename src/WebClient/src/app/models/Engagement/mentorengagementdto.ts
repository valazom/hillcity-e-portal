import { EngagementBaseDto } from './engagementbasedto';
import { ReturnEngagementDto } from './returnengagementdto';

export class MentorEngagementDto {
    numberOfPendingEngagements: number;
    numberOfCompletedEngagements: number;
    numberOfAssignedMentees: number;
    engagements: ReturnEngagementDto[];
}

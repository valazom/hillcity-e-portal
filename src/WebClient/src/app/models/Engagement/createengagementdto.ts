import { EngagementBaseDto } from './engagementbasedto';

export class CreateEngagementDto extends EngagementBaseDto {
    mentorId: number;
    menteeId: number;
}

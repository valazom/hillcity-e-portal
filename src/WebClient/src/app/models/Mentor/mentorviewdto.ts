import { AssignedMentor } from './assignedmentor';
export class MentorViewDto {
    numberOfPendingEngagements: number;
    numberOfCompletedEngagements: number;
    assignedMentor: AssignedMentor;
}

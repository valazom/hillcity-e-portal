import { MentorBaseDto } from './mentorbasedto';
import { UserDto } from '../user/userdto';

export class AssignedMentor extends MentorBaseDto {
    user: UserDto;
}

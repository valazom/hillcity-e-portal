import { UserBaseDto } from './../user/userbasedto';
import { MentorBaseDto } from './mentorbasedto';
export class Mentor extends MentorBaseDto {
    userId: number;
}

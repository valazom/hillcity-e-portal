import { LoctionDto } from './locationdto';
export class UserBaseDto {
    firstName: string;
    lastName: string;
    username: string;
    gender: number;
    email: string;
    photo: string;
    phoneNumber: string;
    publicId: string;
    location: LoctionDto;
}

export class UserPatchDto {
    op: string;
    path: string;
    value: string;
}

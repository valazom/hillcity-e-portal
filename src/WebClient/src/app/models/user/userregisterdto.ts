import { LoctionDto } from './locationdto';
import { UserBaseDto } from './userbasedto';

export class UserRegisterDto {
   Username: string;
   Gender: string;
   Email: string;
   RoleName: string;
   PhoneNumber: string;
   Password: string;
   Location: LoctionDto;
}

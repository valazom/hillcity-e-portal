import { UserBaseDto } from './userbasedto';

export class UserDto extends UserBaseDto {
   userId: number;
}

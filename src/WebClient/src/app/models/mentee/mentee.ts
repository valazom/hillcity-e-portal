import { UserBaseDto } from '../user/userbasedto';
import { MenteeBaseDto } from './menteebasedto';
export class MenteeDto extends MenteeBaseDto {
   userId: number;
}

import { UserDto } from './../user/userdto';
import { MenteeBaseDto } from './menteebasedto';
export class AssignedMentee extends MenteeBaseDto {
    user: UserDto;
}

import { AssignedMentee } from './assignedMentee';
export class MenteeViewDto {
    numberOfPendingEnagagaments: number;
    numberOfCompletedEngagements: number;
    assignedMentees: AssignedMentee[];
}

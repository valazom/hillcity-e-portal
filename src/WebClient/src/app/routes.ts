import { HomeComponent } from './Home/Home.component';
import { Routes } from '@angular/router';

export const appRoutes: Routes = [
   {path: '', component: HomeComponent},
   {path: 'account', loadChildren: './Account/Account.module#AccountModule'},
   {path: 'user', loadChildren: './users/users.module#UsersModule'},
   {path: 'admin', loadChildren: './admin/admin.module#AdminModule'},
   {path: '**', redirectTo: '', pathMatch: 'full'}
];

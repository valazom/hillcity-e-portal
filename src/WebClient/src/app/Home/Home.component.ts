import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './Home.component.html',
  styleUrls: ['./Home.component.scss']
})
export class HomeComponent implements OnInit {
    modalFormLoginEmail = new FormControl('', Validators.email);
    modalFormLoginPassword = new FormControl('', Validators.required);
    modalFormRegisterEmail = new FormControl('', Validators.email);
    modalFormRegisterPassword = new FormControl('', Validators.required);
    modalFormRegisterRepeatPassword = new FormControl('', Validators.required);
  constructor(private router: Router) { }

  ngOnInit() {
  }

  SignIn() {
    this.router.navigate(['/user/users/profile']);
  }
}

import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './services/auth.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { BrowserModule } from '@angular/platform-browser';
import { MDBBootstrapModulesPro} from 'ng-uikit-pro-standard';
import { MDBSpinningPreloader } from 'ng-uikit-pro-standard';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { NavsComponent } from './navs/navs.component';
import { HomeComponent } from './Home/Home.component';
import { AxisComponent } from './axis/axis.component';
import { FooterComponent } from './footer/footer.component';
import { appRoutes } from './routes';
import {RouterModule} from '@angular/router';
import { ErrorInterceptorProvider } from './services/error.interceptor';
import { JwtModule } from '@auth0/angular-jwt';
import {ToastrModule} from 'ngx-toastr';
import { environment } from 'src/environments/environment.prod';

export function tokenGetter() {
 return localStorage.getItem('token');
}

@NgModule({
   declarations: [
      AppComponent,
      NavsComponent,
      NavsComponent,
      HomeComponent,
      AxisComponent,
      FooterComponent
   ],
   imports: [
      BrowserModule,
      BrowserAnimationsModule,
      MDBBootstrapModulesPro.forRoot(),
      FormsModule,
      ReactiveFormsModule,
      RouterModule.forRoot(appRoutes),
      HttpClientModule,
      JwtModule.forRoot({
         config: {
           tokenGetter: tokenGetter,
           whitelistedDomains: ['localhost:5000', 'hepapp.azurewebsites.net'],
           blacklistedRoutes: ['localhost:5000/api/auth', 'hepapp.azurewebsites.net/api/auth']
         }
       }),
       ToastrModule.forRoot()
   ],
   providers: [
      MDBSpinningPreloader,
      ErrorInterceptorProvider,
      AuthService
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }

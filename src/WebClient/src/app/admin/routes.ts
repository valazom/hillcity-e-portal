import { AssignMentorComponent } from './assign-mentor/assign-mentor.component';
import { Component } from '@angular/core';
import { AuthGuard } from './../routeGuards/auth.guard';
import { AdminComponent } from './admin.component';
import { Routes } from '@angular/router';

export const AdminRoutes: Routes = [
    {path: '', redirectTo: 'admin', pathMatch: 'full'},
    {path: 'admin', component: AdminComponent,
      runGuardsAndResolvers: 'always',
      canActivate: [AuthGuard],
      children: [
        {path: 'assignmentor', component: AssignMentorComponent },
      ]
    }
];

import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { SharedService } from '../services/shared.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { UserDto } from '../models/user/userdto';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  jwtHelper = new JwtHelperService();
  Role: any;
  userid: number;
  username: string;
  user: UserDto;
  constructor(public authService: AuthService, private router: Router,
    private userService: UserService, public sharedService: SharedService) { }

  loggedOut() {
    localStorage.removeItem('token');
    this.userService.SetCurrentUser(null);
    this.router.navigate(['/home']);
  }

  get User(): UserDto {
    this.userService.currentMessage.subscribe(cuser => this.user = cuser);
    return this.user;
  }

  getPhoto() {
    this.userService.currentMessage.subscribe(cuser => this.user = cuser);
    if (this.user) {
      if (this.user.photo && this.user.photo !== '') {
        return this.user.photo;
      }
    }
    return 'assets/images/userimage.png';
  }

  ngOnInit() {
    const token = localStorage.getItem('token');
    this.sharedService.LoadDecodedToken(token);
    this.Role = this.sharedService.role;
    this.username = this.sharedService.username;
    this.userid = this.sharedService.userId;
    this.userService.getUser(this.username).subscribe((userdto: UserDto) => {
      this.userService.SetCurrentUser(userdto);
    }, error => {
      this.user = new UserDto();
    });
  }

}

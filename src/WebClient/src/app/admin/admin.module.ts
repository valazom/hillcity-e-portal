import { AssignMentorComponent } from './assign-mentor/assign-mentor.component';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { AdminComponent } from './admin.component';
import { RouterModule } from '@angular/router';
import { AdminRoutes } from './routes';
import { AuthService } from '../services/auth.service';
import { MDBBootstrapModulesPro } from 'ng-uikit-pro-standard';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminRoutes),
    MDBBootstrapModulesPro.forRoot(),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    AdminComponent,
    AssignMentorComponent
  ],
  providers: [
    AuthService,
    DatePipe
  ]
})
export class AdminModule { }
